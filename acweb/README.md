# Environmental variables

## API_ENDPOINT

backend url example `API_ENDPOINT=http://localhost:3000`

## ID_CLIENTE

id_client `ID_CLIENTE=vidasecurity7`
  
## CACHE_TTL_SECONDS

time to live  in seconds for the cache, default 120s   `CACHE_TTL_SECONDS=120`