import {types, flow} from "mobx-state-tree"
import {CXApi} from "./api";
import {rootStore} from "./stores";
import * as moment from 'moment';
import {Axios, Response, ResponseNew} from "./endpoints/base";
import {Campaign, SmsCampaign, PostMarkEventApi, Project, ProjectDeletion, Segment, SegmentDeletion} from "./endpoints/campaigns";
import {ListModelResponse, Model} from "./endpoints/models";
import {ClientModel, ListMarketplaceModelResponse} from "./endpoints/marketplace";
import {Training, TrainingCreationInput, TrainingDetail} from "./endpoints/trainings";
import {Prediction, PredictionResults, PredictionState, UploadFile} from "./endpoints/predictions";
import {TemplateModel, SmsTemplateModel, TemplateDeletion, SmsTemplateDeletion} from "./endpoints/templates";
import {NotificationChangeApi, ReadNotificationApi} from "./endpoints/notifications";
import {boolean, string} from "mobx-state-tree/dist/types/primitives";

const get = require('lodash.get');


export enum ISortDirection {
    ascending = "ascending",
    descending = "descending"

}

export interface ISort<F> {
    field: F,
    direction: ISortDirection

}

export enum IModelSortableFields {
    category = "category",
    version = "version",
    industry = "industry",
    name = "name",
    creation_time = "creation_time"
}

export enum IClientModelSortableFields {
    category = "model.category",
    version = "model.version",
    industry = "model.industry",
    name = "model.name",
    addition_date = "addition_date"
}

export enum ITrainingSortableFields {
    model = "model.name",
    state = "state",
    type = "type",
    subtype = "subtype",
    addition_date = "addition_date",
    name = "name",
    creation_time = "creation_time"
}

export enum IPostmarkEventSortableFields {
    kind = "kind",
    recipient = "recipient",
    subject = "campaign.mailtemplate.title",
    campaign = "campaign.name",
    received_at = "received_at"
}

export enum IPredictionSortableFields {
    name = "name",
    job_state = "job_state",
    creation_time = "creation_time"
}

export enum IMailTemplateSortableFields {
    name = "name",
    creation_date = "creation_date",
}

export enum ICampaignSortableFields {
    creation_time = "creation_time",
    name = "name",
}

export enum IProjectSortableFields {
    creation_date = "creation_date",
    from_address = "from_address",
    name = "name"
}

export enum ISegmentSortableFields {
    creation_time = "creation_time",
    name = "name"
}


const Usuario = types.model({
    id_usuario: types.identifier(types.string),
});

const Cliente = types.model({
    id_cliente: types.identifier(types.string),
});


const CxMailTemplate = types.model({
    id_mailtemplate: types.identifier(types.string),
    name: types.optional(types.string, ""),
    desc: types.optional(types.string, ""),
    creation_date: types.Date,
    title: types.optional(types.string, ""),
    body: types.optional(types.string, ""),
    images: types.optional(types.frozen, {}),
    cliente: types.reference(Cliente),
    usuario: types.reference(Usuario),
    campaign_count: types.optional(types.number, 0),
}).actions(self => {
    function set_name(value: string) {
        self.name = value

    }

    function set_desc(value: string) {
        self.desc = value

    }

    function set_title(value: string) {
        self.title = value

    }

    function set_body(value: string) {
        self.body = value

    }

    return {set_name, set_desc, set_title, set_body}

});

type ICxMailTemplateType = typeof CxMailTemplate.Type;

export interface ICxMailTemplate extends ICxMailTemplateType {
}

const CxSmsTemplate = types.model({
    id_smstemplate: types.identifier(types.string),
    name: types.optional(types.string, ""),
    desc: types.optional(types.string, ""),
    creation_date: types.Date,
    body: types.optional(types.string, ""),
    cliente: types.reference(Cliente),
    usuario: types.reference(Usuario),
    campaign_count: types.optional(types.number, 0),
}).actions(self => {
    function set_name(value: string) {
        self.name = value

    }

    function set_desc(value: string) {
        self.desc = value

    }

    function set_body(value: string) {
        self.body = value

    }

    return {set_name, set_desc, set_body}

});

type ICxSmsTemplateType = typeof CxSmsTemplate.Type;

export interface ICxSmsTemplate extends ICxSmsTemplateType {
}

function sortFNAsc(field: string) {
    return function (a: any, b: any): number {
        const va: string | Date | number = get(a, field);
        const vb: string | Date | number = get(b, field);
        if ((typeof va == 'string') && (typeof vb == 'string')) {
            return va.localeCompare(vb, 'es', {numeric: true})
        }
        else if ((va instanceof Date) && (vb instanceof Date)) {
            return va.getTime() - vb.getTime()
        }
        else if ((typeof va == 'number') && (typeof vb == 'number')) {
            return va - vb
        }
        return 0
    }
}

function sortFNDesc(field: string) {
    return function (a: any, b: any): number {
        const va: string | Date | number = get(a, field);
        const vb: string | Date | number = get(b, field);
        if ((typeof va == 'string') && (typeof vb == 'string')) {
            return vb.localeCompare(va, 'es', {numeric: true})
        }
        else if ((va instanceof Date) && (vb instanceof Date)) {
            return vb.getTime() - va.getTime()
        }
        else if ((typeof va == 'number') && (typeof vb == 'number')) {
            return vb - va
        }
        return 0
    }
}


const CxMlMetric = types.model({
    id_metric: types.identifier(types.string),
    type: types.optional(types.string, ""),
    accuracy: types.optional(types.string, ""),
    fscore: types.optional(types.string, ""),
    recall: types.optional(types.string, ""),
    precision: types.optional(types.string, ""),
    roc: types.optional(types.string, ""),
    confusion_matrix: types.optional(types.frozen, {}),
    roc_curve: types.optional(types.frozen, {}),
    importances: types.optional(types.frozen, {}),
});


const CxMlStatistic = types.model({
    id_statistic: types.identifier(types.string),
    report_name: types.optional(types.string, ""),
    report_desc: types.optional(types.string, ""),
    total_entries: types.optional(types.string, ""),
    positive_level_name: types.optional(types.string, ""),
    negative_level_name: types.optional(types.string, ""),
    positive_level: types.optional(types.string, ""),
    negative_level: types.optional(types.string, ""),
    positive_level_csv: types.optional(types.string, ""),
    negative_level_csv: types.optional(types.string, ""),
    cliente: types.reference(Cliente),
    usuario: types.reference(Usuario)
});


const CxModel = types.model({
    id_model: types.identifier(types.string),
    category: types.optional(types.string, ""),
    version: types.optional(types.string, ""),
    desc: types.optional(types.frozen, {}),
    industry: types.optional(types.string, ""),
    name: types.optional(types.string, ""),
    creation_time: types.optional(types.Date, new Date()),
    is_added: types.optional(types.boolean, false),
});
type ICxModelType = typeof CxModel.Type;

export interface ICxModel extends ICxModelType {
};

const CxProject = types.model({
    id_project: types.identifier(types.string),
    name: types.optional(types.string, ""),
    desc: types.optional(types.string, ""),
    from_address: types.optional(types.string, ""),
    creation_date: types.Date,
    segment_count: types.number,
    cliente: types.reference(Cliente),
    usuario: types.reference(Usuario)
});

type ICxProjectType = typeof CxProject.Type;

export interface ICxProject extends ICxProjectType {
}

const CxClientModel = types.model({
    id_client_model: types.identifier(types.string),
    addition_date: types.Date,
    addition_user: types.reference(Usuario),
    model: types.reference(CxModel),
    cliente: types.reference(Cliente),
});

type ICxClientModelType = typeof CxClientModel.Type;

export interface ICxClientModel extends ICxClientModelType {
};


const CxMarketplace = types.model({
    id_marketplace: types.identifier(types.string),
    name: types.optional(types.string, ""),
    id_app: types.optional(types.string, ""),
    model: types.reference(CxModel),
    creation_time: types.optional(types.Date, new Date()),
});


const CxSegment = types.model({
    id_segment: types.identifier(types.string),
    project: types.maybe(types.reference(CxProject)),
    name: types.optional(types.string, ""),
    desc: types.optional(types.string, ""),
    campaign_input_url: types.optional(types.string, ""),
    total_entries: types.optional(types.string, ""),
    creation_time: types.optional(types.Date, new Date()),
    cliente: types.reference(Cliente),
    usuario: types.reference(Usuario),
    channel_type: types.optional(types.string, ""),
    campaign_count: types.optional(types.number, 0)

});

type ICxSegmentType = typeof CxSegment.Type;

export interface ICxSegment extends ICxSegmentType {
}

const CxTraining = types.model({
    id_training: types.identifier(types.string),
    model: types.reference(CxModel),
    state: types.optional(types.string, ""),
    type: types.optional(types.string, ""),
    subtype: types.optional(types.string, ""),
    name: types.optional(types.string, ""),
    modeling_data: types.optional(types.frozen, {}),
    is_free: types.optional(types.string, ""),
    cliente: types.reference(Cliente),
    usuario: types.reference(Usuario),
    metric: types.maybe(types.reference(CxMlMetric)),
    creation_time: types.Date,
});

type ICxTrainingType = typeof CxTraining.Type;

export interface ICxTraining extends ICxTrainingType {
};

const PostMarkStats = types.model({
    BounceRate: types.optional(types.number, 0),
    Bounced: types.optional(types.number, 0),
    Opens: types.optional(types.number, 0),
    SMTPApiErrors: types.optional(types.number, 0),
    Sent: types.optional(types.number, 0),
    SpamComplaints: types.optional(types.number, 0),
    SpamComplaintsRate: types.optional(types.number, 0),
    TotalClicks: types.optional(types.number, 0),
    TotalTrackedLinksSent: types.optional(types.number, 0),
    Tracked: types.optional(types.number, 0),
    UniqueLinksClicked: types.optional(types.number, 0),
    UniqueOpens: types.optional(types.number, 0),
    WithClientRecorded: types.optional(types.number, 0),
    WithLinkTracking: types.optional(types.number, 0),
    WithOpenTracking: types.optional(types.number, 0),
    WithPlatformRecorded: types.optional(types.number, 0),
    WithReadTimeRecorded: types.optional(types.number, 0)
});

const CxCampaign = types.model({
    id_campaign: types.identifier(types.string),
    segment: types.reference(CxSegment),
    creation_time: types.optional(types.Date, new Date()),
    name: types.optional(types.string, ""),
    desc: types.optional(types.string, ""),
    email_title: types.optional(types.string, ""),
    email_body: types.optional(types.string, ""),
    email_images: types.optional(types.frozen, {}),
    frequency: types.optional(types.string, ""),
    start_time: types.optional(types.Date, new Date()),
    end_time: types.optional(types.Date, new Date()),
    timezone: types.optional(types.string, ""),
    cliente: types.reference(Cliente),
    usuario: types.reference(Usuario),
    state: types.optional(types.boolean, false),
    mailtemplate: types.reference(CxMailTemplate),
    replacement_data_url: types.optional(types.string, ""),
    schedule_expression: types.optional(types.string, ""),
    stats_data: types.maybe(PostMarkStats)
});

type ICxCampaignType = typeof CxCampaign.Type;

export interface ICxCampaign extends ICxCampaignType {
};

const CxSmsCampaign = types.model({
    batch_ids: types.optional(types.frozen, {}),
    cliente: types.reference(Cliente),
    creation_time: types.optional(types.Date, new Date()),
    desc: types.optional(types.string, ""),
    from_address: types.optional(types.number, 0),
    id_smscampaign: types.identifier(types.string),
    name: types.optional(types.string, ""),
    schedule_expression: types.optional(types.string, ""),
    segment: types.reference(CxSegment),
    send_now: types.optional(types.boolean, false),
    smstemplate: types.reference(CxSmsTemplate),
    state: types.optional(types.boolean, false),
    timezone: types.optional(types.string, ""),
    usuario: types.reference(Usuario),
});

type ICxSmsCampaignType = typeof CxSmsCampaign.Type;

export interface ICxSmsCampaign extends ICxSmsCampaignType {
};

const CxPrediction = types.model({
    id_prediction: types.identifier(types.string),
    training: types.maybe(types.reference(CxTraining)),
    name: types.optional(types.string, ""),
    creation_time: types.optional(types.Date, new Date()),
    on_dashboard: types.optional(types.boolean, false),
    id_job: types.optional(types.string, ""),
    job_type: types.optional(types.string, ""),
    job_start_time: types.optional(types.Date, new Date()),
    job_state: types.optional(types.string, ""),
    input_csv_url: types.optional(types.string, ""),
    output_csv_url: types.optional(types.string, ""),
    job_finish_time: types.optional(types.Date, new Date()),
    cliente: types.reference(Cliente),
    usuario: types.reference(Usuario),
    statistic: types.maybe(types.reference(CxMlStatistic)),
});

type ICxPredictionType = typeof CxPrediction.Type;

export interface ICxPrediction extends ICxPredictionType {
};


const CxCampaignStat = types.model({
    id_c_stat: types.identifier(types.string),
    name: types.optional(types.string, ""),
    campaign: types.reference(CxCampaign),
    open_rate: types.optional(types.string, ""),
    delivered: types.optional(types.string, ""),
    cliente: types.reference(Cliente),
    usuario: types.reference(Usuario),
});

const LoginForm = types.model({
    username: types.optional(types.string, ""),
    password: types.optional(types.string, ""),
    remember: types.optional(types.boolean, false),
    is_loading: types.optional(types.boolean, false),
    error: types.optional(types.boolean, false),
}).actions(self => {
        function setUsername(value: string) {
            self.username = value;
        }

        function setPassword(value: string) {
            self.password = value;
        }

        function setRemember(value: boolean) {
            self.remember = value;
        }

        function clear() {
            self.username = "";
            self.password = "";
            self.remember = false;
            self.is_loading = false;
            self.error = false;
        }

        const login = flow(function* () {
            self.is_loading = true;
            try {
                let response = yield  CXApi.login.login(self.username, self.password);
                if (response.data.resp_code == "1") {
                    //login ok
                    let username = self.username;
                    if (self.remember) {
                        self.error = false;
                        //save in local storage
                        localStorage.setItem("login_form", JSON.stringify({
                            username: self.username,
                            password: self.password,
                            remember: self.remember
                        }));
                    } else {
                        clear()
                    }
                    localStorage.setItem("id_session", response.data.id_sesion);
                    localStorage.setItem("id_usuario", username);

                    //update the session
                    CXApi.options.id_sesion = response.data.id_sesion;
                    CXApi.options.id_usuario = username;

                    //go to default_view
                    rootStore.routerStore.goTo("mis_modelos")
                } else {
                    //login error
                    self.error = true;
                }
            } catch (error) {
                console.error("Failed to login", error)
            }
            self.is_loading = false;
            return;
        });

        return {setUsername, setPassword, setRemember, login}
    }
);

function check_login<T>(data: T): T {
    let innerData: any = data as any;
    if (innerData.resp_code == "2" && innerData.resp_message == "id_sesion not found") {
        delete CXApi.options.id_sesion;
        rootStore.routerStore.goTo("login")
    }
    return data
}

const CxNotificationChange = types.model({
    id_notification_change: types.identifier(types.string),
    entity_type: types.optional(types.string, ""),
    entity_id: types.optional(types.string, ""),
    entity_name: types.optional(types.string, ""),
    action_on_entity: types.optional(types.string, ""),
    actor_id_usuario: types.optional(types.string, ""),
    actor_id_cliente: types.optional(types.string, ""),
    created_at: types.optional(types.Date, new Date()),
});

const CxReadNotification = types.model({
    id_read_notification: types.identifier(types.string),
    id_usuario: types.optional(types.string, ""),
    notification_change: types.reference(CxNotificationChange),
    read_at: types.optional(types.Date, new Date()),
});


export enum PostmarkEventKind {
    none = "none",
    open = "open",
    click = "click",
    delivery = "delivery",
    bounce = "bounce",
}

const PostmarkEvent = types.model({
    id: types.identifier(types.string),
    click_location: types.optional(types.string, ""),
    geo_city: types.optional(types.string, ""),
    geo_coords: types.optional(types.string, ""),
    geo_country: types.optional(types.string, ""),
    geo_region: types.optional(types.string, ""),
    cliente: types.reference(Cliente),
    message_id: types.optional(types.string, ""),
    original_link: types.optional(types.string, ""),
    platform: types.optional(types.string, ""),
    query: types.optional(types.frozen, null),
    query_class: types.optional(types.frozen, null),
    received_at: types.Date,
    recipient: types.string,
    campaign: types.reference(CxCampaign),
    kind: types.optional(types.enumeration([
        PostmarkEventKind.none,
        PostmarkEventKind.open,
        PostmarkEventKind.click,
        PostmarkEventKind.bounce,
        PostmarkEventKind.delivery
    ]), PostmarkEventKind.none)
});

type IPostmarkEventType = typeof PostmarkEvent.Type;

interface IPostmarkEvent extends IPostmarkEventType {
};

const CxRoot = types.model({
    login_form: LoginForm,
    list_interval: types.optional(types.number, parseInt(process.env.CACHE_TTL_SECONDS||"120") || 120),
    list_last_request: types.optional(types.map(types.string), {}),
    cliente: types.maybe(Cliente),
    usuario: types.maybe(Usuario),
    templates: types.optional(types.map(CxMailTemplate), {}),
    sms_templates: types.optional(types.map(CxSmsTemplate), {}),
    ml_metrics: types.optional(types.map(CxMlMetric), {}),
    ml_statistics: types.optional(types.map(CxMlStatistic), {}),
    models: types.optional(types.map(CxModel), {}),
    projects: types.optional(types.map(CxProject), {}),
    marketplaces: types.optional(types.map(CxMarketplace), {}),
    segments: types.optional(types.map(CxSegment), {}),
    trainings: types.optional(types.map(CxTraining), {}),
    campaigns: types.optional(types.map(CxCampaign), {}),
    sms_campaigns: types.optional(types.map(CxSmsCampaign), {}),
    predictions: types.optional(types.map(CxPrediction), {}),
    campaign_stats: types.optional(types.map(CxCampaignStat), {}),
    client_models: types.optional(types.map(CxClientModel), {}),
    api_loading: types.optional(types.map(types.number), {}),
    postmark_events: types.optional(types.map(PostmarkEvent), {}),
    notification_change: types.optional(types.map(CxNotificationChange), {}),
    read_notification: types.optional(types.map(CxReadNotification), {}),
}).views((self) => ({
    get notification_map(): Map<string, boolean> {
        let nm = new Map<string, boolean>();
        for (const rn of Array.from(self.read_notification.values())) {
            nm.set(rn.notification_change.id_notification_change, true)
        }
        return nm
    },
    get unread_notification_count(): number {
        let unread = 0;
        for (const nc of Array.from(self.notification_change.values())) {
            if (!self.notification_map.has(nc.id_notification_change)) {
                unread = unread + 1;
            }
        }
        return unread
    },
})).views(self => {
    const get_projects_option = function () {
        let response = [];
        for (const project of Array.from(self.projects.values())) {
            if (project.segment_count == 0) {
                continue
            }
            response.push({
                key: project.id_project,
                text: project.name,
                value: project.id_project
            })
        }
        return response
    };
    const get_campaign_option = function () {
        let response: { key: string, value: string | undefined, text: string }[] = [{
            key: "---",
            text: "---",
            value: undefined
        }];
        for (const campaign of Array.from(self.campaigns.values())) {
            response.push({
                key: campaign.id_campaign,
                text: campaign.name,
                value: campaign.id_campaign
            })
        }
        return response
    };
    const get_segments_option = function (id_project: string) {
        let response = [];
        for (const segment of Array.from(self.segments.values())) {
            if ((!segment.project) || (segment.project.id_project != id_project)) {
                continue
            }
            response.push({
                key: segment.id_segment,
                text: segment.name,
                value: segment.id_segment
            })
        }
        return response
    };
    const get_template_option = function () {
        let response = [];
        for (const template of Array.from(self.templates.values())) {
            response.push({
                key: template.id_mailtemplate,
                text: template.name,
                value: template.id_mailtemplate
            })
        }
        return response
    };
    const get_sms_template_option = function () {
        let response = [];
        for (const template of Array.from(self.sms_templates.values())) {
            response.push({
                key: template.id_smstemplate,
                text: template.name,
                value: template.id_smstemplate
            })
        }
        return response
    };
    const get_my_model_option = function () {
        let response = [];
        for (const model of Array.from(self.client_models.values())) {
            response.push({
                key: model.model.id_model,
                text: model.model.name,
                value: model.model.id_model
            })
        }
        return response
    };
    const get_postmark_event = function (id_campaign: string | null = null,
                                         term = "",
                                         from: Date | null = null,
                                         to: Date | null = null,
                                         kind: PostmarkEventKind[] = [],
                                         sort: ISort<IPostmarkEventSortableFields> = {
                                             direction: ISortDirection.descending,
                                             field: IPostmarkEventSortableFields.received_at
                                         }): IPostmarkEvent[] {
        let ret = [];
        for (const pe of Array.from(self.postmark_events.values())) {
            if (id_campaign) {
                if (pe.campaign.id_campaign != id_campaign) {
                    continue
                }
            }
            if (kind.length > 0) {
                if (kind.indexOf(pe.kind) < 0) {
                    continue
                }
            }

            if (term.trim() != "") {
                let searchHit = pe.campaign.name.toLowerCase().includes(term.toLowerCase())
                    || pe.campaign.mailtemplate.title.toLowerCase().includes(term.toLowerCase())
                    || pe.recipient.toLowerCase().includes(term.toLowerCase())
                    || pe.campaign.id_campaign.toLowerCase().includes(term.toLowerCase());

                if (!searchHit) {
                    continue
                }
            }
            if (from && to) {
                const pe_date = moment(pe.received_at);
                if (!pe_date.isBetween(from, to)) {
                    continue
                }
            }
            ret.push(pe);
        }

        if (sort.direction == ISortDirection.descending) {
            return ret.sort(sortFNDesc(sort.field))
        } else {
            return ret.sort(sortFNAsc(sort.field))

        }
    };
    const get_models = function (category = null,
                                 term = "",
                                 from: Date | null = null,
                                 to: Date | null = null,
                                 sort: ISort<IModelSortableFields> = {
                                     direction: ISortDirection.descending,
                                     field: IModelSortableFields.creation_time
                                 }): ICxModel[] {
        let ret = [];
        for (const model of Array.from(self.models.values())) {
            if (category) {
                if (model.category != category) {
                    continue
                }
            }
            if (term.trim() != "") {
                let searchHit = model.name.toLowerCase().includes(term.toLowerCase())
                    || model.category.toLowerCase().includes(term.toLowerCase())
                    || model.industry.toLowerCase().includes(term.toLowerCase());
                if (!searchHit) {
                    continue
                }
            }

            if (from && to) {
                const pe_date = moment(model.creation_time);
                if (!pe_date.isBetween(from, to)) {
                    continue
                }
            }
            ret.push(model);
        }

        if (sort.direction == ISortDirection.descending) {
            return ret.sort(sortFNDesc(sort.field))
        } else {
            return ret.sort(sortFNAsc(sort.field))

        }
    };

    const get_client_models = function (sort: ISort<IClientModelSortableFields> = {
        direction: ISortDirection.descending,
        field: IClientModelSortableFields.addition_date
    }): ICxClientModel[] {
        let ret = Array.from(self.client_models.values());

        if (sort.direction == ISortDirection.descending) {
            return ret.sort(sortFNDesc(sort.field))
        } else {
            return ret.sort(sortFNAsc(sort.field))
        }
    };

    const get_trainings = function (sort: ISort<ITrainingSortableFields> = {
        direction: ISortDirection.descending,
        field: ITrainingSortableFields.creation_time
    }): ICxTraining[] {
        let ret = Array.from(self.trainings.values());

        if (sort.direction == ISortDirection.descending) {
            return ret.sort(sortFNDesc(sort.field))
        } else {
            return ret.sort(sortFNAsc(sort.field))
        }
    };

    const get_templates = function (sort: ISort<IMailTemplateSortableFields> = {
        direction: ISortDirection.descending,
        field: IMailTemplateSortableFields.creation_date
    }): ICxMailTemplate[] {
        let ret = Array.from(self.templates.values());

        if (sort.direction == ISortDirection.descending) {
            return ret.sort(sortFNDesc(sort.field))
        } else {
            return ret.sort(sortFNAsc(sort.field))
        }
    };

    const get_smstemplates = function (sort: ISort<IMailTemplateSortableFields> = {
        direction: ISortDirection.descending,
        field: IMailTemplateSortableFields.creation_date
    }): ICxSmsTemplate[] {
        let ret = Array.from(self.sms_templates.values());

        if (sort.direction == ISortDirection.descending) {
            return ret.sort(sortFNDesc(sort.field))
        } else {
            return ret.sort(sortFNAsc(sort.field))
        }
    };

    // Get a combined sorted list of both mail and sms templates
    const get_alltemplates  = function (sort: ISort<IMailTemplateSortableFields> = {
        direction: ISortDirection.descending,
        field: IMailTemplateSortableFields.creation_date
    }): any[] {
        let ret: any[] = Array.from(self.sms_templates.values());
        ret = ret.concat(Array.from(self.templates.values()));

        if (sort.direction == ISortDirection.descending) {
            return ret.sort(sortFNDesc(sort.field))
        } else {
            return ret.sort(sortFNAsc(sort.field))
        }
    };

    const get_campaigns = function (sort: ISort<ICampaignSortableFields> = {
        direction: ISortDirection.descending,
        field: ICampaignSortableFields.creation_time
    }): ICxCampaign[] {
        let ret = Array.from(self.campaigns.values());

        if (sort.direction == ISortDirection.descending) {
            return ret.sort(sortFNDesc(sort.field))
        } else {
            return ret.sort(sortFNAsc(sort.field))
        }
    };

    const get_smscampaigns = function (sort: ISort<ICampaignSortableFields> = {
        direction: ISortDirection.descending,
        field: ICampaignSortableFields.creation_time
    }): ICxSmsCampaign[] {
        let ret = Array.from(self.sms_campaigns.values());

        if (sort.direction == ISortDirection.descending) {
            return ret.sort(sortFNDesc(sort.field))
        } else {
            return ret.sort(sortFNAsc(sort.field))
        }
    };

    const get_projects = function (sort: ISort<IProjectSortableFields> = {
        direction: ISortDirection.descending,
        field: IProjectSortableFields.creation_date
    }): ICxProject[] {
        let ret = Array.from(self.projects.values());

        if (sort.direction == ISortDirection.descending) {
            return ret.sort(sortFNDesc(sort.field))
        } else {
            return ret.sort(sortFNAsc(sort.field))
        }
    };

    const get_segments = function (id_project: string | null = null, sort: ISort<ISegmentSortableFields> = {
        direction: ISortDirection.descending,
        field: ISegmentSortableFields.creation_time
    }): ICxSegment[] {
        let ret: ICxSegment[] = [];

        if (id_project) {
            for (const segment of Array.from(self.segments.values())) {
                if (!segment.project) {
                    continue
                }
                if (segment.project.id_project != id_project) {
                    continue
                }
                ret.push(segment)
            }
            if (sort.direction == ISortDirection.descending) {
                return ret.sort(sortFNDesc(sort.field))
            } else {
                return ret.sort(sortFNAsc(sort.field))
            }
        }
        return ret
    };


    const get_predictions = function (id_training: string | null = null, sort: ISort<IPredictionSortableFields> = {
        direction: ISortDirection.descending,
        field: IPredictionSortableFields.creation_time
    }): ICxPrediction[] {
        let ret: ICxPrediction[] = [];

        if (id_training) {
            for (const prediction of Array.from(self.predictions.values())) {
                if (prediction.training!.id_training != id_training) {
                    continue
                }
                ret.push(prediction)
            }
            if (sort.direction == ISortDirection.descending) {
                return ret.sort(sortFNDesc(sort.field))
            } else {
                return ret.sort(sortFNAsc(sort.field))
            }
        }
        return ret
    };


    const get_notifications = function () {
        return Array.from(CxStore.notification_change.values()).sort(sortFNDesc("created_at"));
    };
    return {
        get_projects_option,
        get_campaign_option,
        get_segments_option,
        get_template_option,
        get_sms_template_option,
        get_my_model_option,
        get_postmark_event,
        get_models,
        get_trainings,
        get_predictions,
        get_templates,
        get_smstemplates,
        get_alltemplates,
        get_campaigns,
        get_smscampaigns,
        get_projects,
        get_segments,
        get_client_models,
        get_notifications,
    }
}).actions(self => {
        const addRequest = function (api_id: string) {
            let count = self.api_loading.get(api_id);
            if (!count) {
                count = 0;
            }
            self.api_loading.set(api_id, count + 1);
        };
        const removeRequest = function (api_id: string) {
            let count = self.api_loading.get(api_id);
            if (!count) {
                count = 0;
            }
            if (count == 0) {
                return
            }
            self.api_loading.set(api_id, count - 1);

        };
        const reportError = function (api_id: string, error_id: string, status: number) {
            console.error(`Si has visto este error, por favor reportalo a los administradores, api_id:${api_id} , error_id:${error_id}, status:${status}`)
        };

        const cx_create_training = flow(function* (input: TrainingCreationInput) {
            addRequest('cx_create_training');

            try {
                let http_response: Axios<Response<Training>> = yield  CXApi.trainings.cx_create_training(input);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    for (let item of labmda_output.response) {
                        if (!self.models.has(item.id_model)) {
                            self.models.set(item.id_model, CxModel.create({id_model: item.id_model}));
                        }

                        if (!self.ml_metrics.has(item.id_metric)) {
                            self.ml_metrics.set(item.id_metric, CxMlMetric.create({id_metric: item.id_metric}));
                        }

                        self.trainings.set(item.id_training, CxTraining.create({
                            id_training: item.id_training,
                            model: item.id_model,
                            state: item.state,
                            type: item.type,
                            subtype: item.subtype,
                            name: item.name,
                            modeling_data: item.modeling_data,
                            is_free: item.is_free,
                            cliente: CXApi.options.id_cliente,
                            usuario: CXApi.options.id_usuario,
                            metric: item.id_metric,
                            creation_time: moment.utc(item.creation_time, 'YYYY-M-D h:mm:ss').local().toDate()
                        }));
                    }

                } else {
                    reportError(`cx_create_training`, labmda_output.error_id, labmda_output.status)
                }
            } catch (error) {
                console.error(error)
            }
            removeRequest('cx_create_training');
            return;
        });

        const cx_list_trainings = flow(function* (force: boolean) {
            if (!force) {
                if (self.list_last_request.get('cx_list_trainings')) {
                    if (moment(new Date()).diff(moment(self.list_last_request.get('cx_list_trainings')), "seconds") < self.list_interval) {
                        return null;
                    }
                }
            }


            addRequest('cx_list_trainings');

            try {
                let http_response: Axios<Response<Training>> = yield  CXApi.trainings.cx_list_trainings();
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    self.list_last_request.set('cx_list_trainings', moment().format());
                    for (let item of labmda_output.response) {
                        if (!self.models.has(item.id_model)) {
                            self.models.set(item.id_model, CxModel.create({id_model: item.id_model}));
                        }
                        if (item.id_metric) {
                            if (!self.ml_metrics.has(item.id_metric)) {
                                self.ml_metrics.set(item.id_metric, CxMlMetric.create({id_metric: item.id_metric}));
                            }
                        }


                        self.trainings.set(item.id_training, CxTraining.create({
                            id_training: item.id_training,
                            model: item.id_model,
                            state: item.state,
                            type: item.type,
                            subtype: item.subtype,
                            name: item.name,
                            modeling_data: item.modeling_data,
                            is_free: item.is_free,
                            cliente: CXApi.options.id_cliente,
                            usuario: CXApi.options.id_usuario,
                            metric: item.id_metric,
                            creation_time: moment.utc(item.creation_time, 'YYYY-M-D h:mm:ss').local().toDate()

                        }));
                    }

                } else {
                    reportError(`cx_list_trainings`, labmda_output.error_id, labmda_output.status)
                }
            } catch (error) {
                console.error(error)
            }
            removeRequest('cx_list_trainings');
            return;
        });


        const cx_create_prediction = flow(function* (id_training: string, name: string, input_csv_url: string) {
            const training = self.trainings.get(id_training);
            if (!training) {
                return
            }
            addRequest(`cx_create_prediction`);

            try {
                let http_response: Axios<Response<Prediction>> = yield  CXApi.predictions.cx_create_prediction(id_training, name, input_csv_url);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    for (let prediction of labmda_output.response) {
                        if (prediction.id_statistic) {
                            if (!self.ml_statistics.has(prediction.id_statistic)) {
                                self.ml_statistics.set(prediction.id_statistic, CxMlStatistic.create({
                                    id_statistic: prediction.id_statistic,
                                    cliente: CXApi.options.id_cliente,
                                    usuario: CXApi.options.id_usuario
                                }));
                            }
                        }

                        self.predictions.set(prediction.id_prediction, CxPrediction.create({
                            id_prediction: prediction.id_prediction,
                            training: prediction.id_training,
                            name: prediction.name,
                            creation_time: moment.utc(prediction.creation_time, 'YYYY-M-D h:mm:ss').local().toDate(),
                            on_dashboard: prediction.on_dashboard,
                            id_job: prediction.id_job,
                            job_type: prediction.job_type,
                            job_start_time: moment.utc(prediction.job_start_time, 'YYYY-M-D h:mm:ss').local().toDate(),
                            job_state: prediction.job_state,
                            input_csv_url: prediction.input_csv_url,
                            output_csv_url: prediction.output_csv_url,
                            job_finish_time: moment.utc(prediction.job_finish_time, 'YYYY-M-D h:mm:ss').local().toDate(),
                            cliente: CXApi.options.id_cliente,
                            usuario: CXApi.options.id_usuario,
                            statistic: prediction.id_statistic,
                        }));
                    }

                } else {
                    reportError(`cx_create_prediction`, labmda_output.error_id, labmda_output.status)
                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_create_prediction`);
            return;
        });

        const cx_list_predictions = flow(function* (id_training: string, force: boolean) {
            if (!force) {
                if (self.list_last_request.get(`cx_list_predictions.${id_training}`)) {
                    if (moment(new Date()).diff(moment(self.list_last_request.get(`cx_list_predictions.${id_training}`)), "seconds") < self.list_interval) {
                        return null;
                    }
                }
            }


            const training = self.trainings.get(id_training);
            if (!training) {
                return
            }
            addRequest(`cx_list_predictions.${id_training}`);

            try {
                let http_response: Axios<Response<Prediction>> = yield  CXApi.predictions.cx_list_predictions(id_training);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    self.list_last_request.set(`cx_list_predictions.${id_training}`, moment().format());
                    for (let prediction of labmda_output.response) {
                        if (prediction.id_statistic) {
                            if (!self.ml_statistics.has(prediction.id_statistic)) {
                                self.ml_statistics.set(prediction.id_statistic, CxMlStatistic.create({
                                    id_statistic: prediction.id_statistic,
                                    cliente: CXApi.options.id_cliente,
                                    usuario: CXApi.options.id_usuario
                                }));
                            }
                        }

                        self.predictions.set(prediction.id_prediction, CxPrediction.create({
                            id_prediction: prediction.id_prediction,
                            training: prediction.id_training,
                            name: prediction.name,
                            creation_time: moment.utc(prediction.creation_time, 'YYYY-M-D h:mm:ss').local().toDate(),
                            on_dashboard: prediction.on_dashboard,
                            id_job: prediction.id_job,
                            job_type: prediction.job_type,
                            job_start_time: moment.utc(prediction.job_start_time, 'YYYY-M-D h:mm:ss').local().toDate(),
                            job_state: prediction.job_state,
                            input_csv_url: prediction.input_csv_url || "",
                            output_csv_url: prediction.output_csv_url || "",
                            job_finish_time: moment.utc(prediction.job_finish_time, 'YYYY-M-D h:mm:ss').local().toDate(),
                            cliente: CXApi.options.id_cliente,
                            usuario: CXApi.options.id_usuario,
                            statistic: prediction.id_statistic,
                        }));
                    }

                } else {
                    reportError(`cx_list_predictions`, labmda_output.error_id, labmda_output.status)
                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_list_predictions.${id_training}`);
            return;
        });

        const cx_prediction_results = flow(function* (id_prediction: string) {
            const prediction = self.predictions.get(id_prediction);
            if (!prediction) {
                return
            }
            addRequest(`cx_prediction_results.${id_prediction}`);

            try {
                let http_response: Axios<Response<PredictionResults>> = yield  CXApi.predictions.cx_prediction_results(id_prediction);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    for (let prediction_result of labmda_output.response) {

                        self.ml_statistics.set(prediction_result.id_statistic, CxMlStatistic.create({
                            id_statistic: prediction_result.id_statistic,
                            report_name: prediction_result.report_name,
                            report_desc: prediction_result.report_desc,
                            total_entries: prediction_result.total_entries + "",
                            positive_level_name: prediction_result.positive_level_name,
                            negative_level_name: prediction_result.negative_level_name,
                            positive_level: prediction_result.positive_level + "",
                            negative_level: prediction_result.negative_level + "",
                            positive_level_csv: prediction_result.positive_level_csv,
                            negative_level_csv: prediction_result.negative_level_csv,
                            cliente: prediction_result.id_cliente,
                            usuario: prediction_result.id_usuario,

                        }));
                    }

                } else {
                    reportError(`cx_prediction_results`, labmda_output.error_id, labmda_output.status)
                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_prediction_results.${id_prediction}`);
            return;
        });

        const cx_prediction_state = flow(function* (id_prediction: string) {
            const prediction = self.predictions.get(id_prediction);
            if (!prediction) {
                return
            }
            addRequest(`cx_prediction_state.${id_prediction}`);

            try {
                let http_response: Axios<Response<PredictionState>> = yield  CXApi.predictions.cx_prediction_state(id_prediction);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    for (let prediction_state of labmda_output.response) {
                        prediction.on_dashboard = prediction_state.on_dashboard;
                    }

                } else {
                    reportError(`cx_prediction_state`, labmda_output.error_id, labmda_output.status)
                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_prediction_state.${id_prediction}`);
            return;
        });

        const cx_uploads3_predictinput = flow(function* (name: string, extension: string, content: string, callback: { (url: string): void }) {
            addRequest(`cx_uploads3_predictinput`);

            try {
                let http_response: Axios<Response<UploadFile>> = yield  CXApi.predictions.cx_uploads3_predictinput(name, extension, content);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    callback(labmda_output.response[0].url)
                } else {
                    reportError(`cx_uploads3_predictinput`, labmda_output.error_id, labmda_output.status)
                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_uploads3_predictinput`);
            return;
        });

        const cx_training_details = flow(function* (id_training: string) {
            const training = self.trainings.get(id_training);
            if (!training) {
                return
            }
            if (!training.metric) {
                return
            }

            addRequest(`cx_training_details.${id_training}`);

            try {
                let http_response: Axios<Response<TrainingDetail>> = yield  CXApi.trainings.cx_training_details(training.metric.id_metric);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    for (let item of labmda_output.response) {

                        self.ml_metrics.set(item.id_metric, CxMlMetric.create({
                            id_metric: item.id_metric,
                            type: item.type,
                            accuracy: item.accuracy + "",
                            fscore: item.fscore + "",
                            recall: item.recall + "",
                            precision: item.precision + "",
                            roc: item.roc + "",
                            confusion_matrix: item.confusion_matrix,
                            roc_curve: item.roc_curve,
                            importances: item.importances
                        }));
                    }

                } else {
                    reportError(`cx_training_details`, labmda_output.error_id, labmda_output.status)
                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_training_details.${id_training}`);
            return;
        });

        const cx_list_models = flow(function* (force: boolean) {
            if (!force) {
                if (self.list_last_request.get('cx_list_models')) {
                    if (moment(new Date()).diff(moment(self.list_last_request.get('cx_list_models')), "seconds") < self.list_interval) {
                        return null;
                    }
                }
            }


            addRequest('cx_list_models');
            try {
                let http_response: Axios<Response<ListMarketplaceModelResponse>> = yield  CXApi.marketplace.cx_list_models();
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    self.list_last_request.set('cx_list_models', moment().format());

                    for (let item of labmda_output.response) {
                        self.models.set(item.id_model, CxModel.create({
                            id_model: item.id_model,
                            category: item.category,
                            version: "" + item.version,
                            desc: item.desc,
                            industry: item.industry,
                            name: item.name,
                            is_added: item.is_added,
                            creation_time: moment.utc(item.creation_time, 'YYYY-M-D h:mm:ss').local().toDate(),

                        }));
                    }

                } else {
                    reportError(`cx_list_models`, labmda_output.error_id, labmda_output.status)
                }
            } catch (error) {
                console.error(error)
            }
            removeRequest('cx_list_models');
            return;
        });

        const cx_add_model = flow(function* (id_model: string) {
            addRequest(`cx_add_model.${id_model}`);
            try {
                let http_response: Axios<Response<ClientModel>> = yield  CXApi.marketplace.cx_add_model(id_model);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    self.models.get(id_model)!.is_added = true;
                } else {
                    reportError(`cx_add_model`, labmda_output.error_id, labmda_output.status)
                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_add_model.${id_model}`);
            return;
        });

        const cx_list_mymodels = flow(function* (force: boolean) {
            if (!force) {
                if (self.list_last_request.get('cx_list_mymodels')) {
                    if (moment(new Date()).diff(moment(self.list_last_request.get('cx_list_mymodels')), "seconds") < self.list_interval) {
                        return null;
                    }
                }
            }

            addRequest('cx_list_mymodels');
            try {
                let http_response: Axios<Response<ListModelResponse>> = yield  CXApi.models.cx_list_mymodels();
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    self.list_last_request.set('cx_list_mymodels', moment().format());
                    for (let item of labmda_output.response) {
                        self.models.set(item.id_model, CxModel.create({
                            id_model: item.id_model,
                            category: item.category,
                            version: "" + item.version,
                            desc: item.desc,
                            industry: item.industry,
                            name: item.name,
                            is_added: true,
                            creation_time: moment.utc(item.creation_time, 'YYYY-M-D h:mm:ss').local().toDate(),

                        }));

                        let client_model_id = `${item.id_model}@@@${CXApi.options.id_cliente}`;
                        self.client_models.set(client_model_id, CxClientModel.create({
                            id_client_model: client_model_id,
                            addition_date: moment(item.addition_date, 'YYYY-M-D h:mm:ss').toDate(),
                            addition_user: CXApi.options.id_usuario!,
                            model: item.id_model,
                            cliente: CXApi.options.id_cliente,
                        }));

                    }

                } else {
                    reportError(`cx_list_mymodels`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest('cx_list_mymodels');
            return;
        });
        const cx_send_mailcampaign = flow(function* (input: {
            id_segment: string,
            name: string,
            desc: string,
            frequency: string,
            start_time: string,
            end_time: string,
            schedule_expression: string,
            id_mailtemplate: string,
            email_images: any
        }) {
            addRequest('cx_send_mailcampaign');
            try {
                let http_response: Axios<Response<Campaign>> = yield  CXApi.campaigns.cx_send_mailcampaign(
                    input.id_segment,
                    input.name,
                    input.desc,
                    input.frequency,
                    input.start_time,
                    input.end_time,
                    input.schedule_expression,
                    input.id_mailtemplate,
                    input.email_images);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    let campaign = labmda_output.response[0];
                    self.campaigns.set(campaign.id_campaign, CxCampaign.create({
                        id_campaign: campaign.id_campaign,
                        segment: campaign.id_segment,
                        creation_time: moment.utc(campaign.creation_time, 'YYYY-M-D h:mm:ss').local().toDate(),
                        name: campaign.campaign_name,
                        desc: campaign.campaign_desc,
                        email_images: campaign.email_images,
                        frequency: campaign.frequency,
                        start_time: moment.utc(campaign.start_time || moment().utc().format("YYYY-M-D h:mm:ss"), 'YYYY-M-D h:mm:ss').local().toDate(),
                        end_time: moment.utc(campaign.end_time || moment().utc().format("YYYY-M-D h:mm:ss"), 'YYYY-M-D h:mm:ss').local().toDate(),
                        schedule_expression: campaign.schedule_expression,
                        cliente: campaign.id_cliente,
                        usuario: campaign.id_usuario,
                        mailtemplate: campaign.id_mailtemplate,
                        replacement_data_url: campaign.replacement_data_url,
                        state: campaign.state,
                    }));

                } else {
                    reportError(`cx_send_mailcampaign`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest('cx_send_mailcampaign');
            return;
        });
        const cx_create_campaign = flow(function* (input: {
            id_segment: string,
            name: string,
            desc: string,
            frequency: string,
            start_time: string,
            end_time: string,
            schedule_expression: string,
            id_mailtemplate: string,
            email_images: any
        }) {
            addRequest('cx_create_campaign');
            try {
                let http_response: Axios<Response<Campaign>> = yield  CXApi.campaigns.cx_create_campaign(
                    input.id_segment,
                    input.name,
                    input.desc,
                    input.frequency,
                    input.start_time,
                    input.end_time,
                    input.schedule_expression,
                    input.id_mailtemplate,
                    input.email_images);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    let campaign = labmda_output.response[0];
                    self.campaigns.set(campaign.id_campaign, CxCampaign.create({
                        id_campaign: campaign.id_campaign,
                        segment: campaign.id_segment,
                        creation_time: moment.utc(campaign.creation_time, 'YYYY-M-D h:mm:ss').local().toDate(),
                        name: campaign.campaign_name,
                        desc: campaign.campaign_desc,
                        email_images: campaign.email_images,
                        frequency: campaign.frequency,
                        start_time: moment.utc(campaign.start_time || moment().utc().format("YYYY-M-D h:mm:ss"), 'YYYY-M-D h:mm:ss').local().toDate(),
                        end_time: moment.utc(campaign.end_time || moment().utc().format("YYYY-M-D h:mm:ss"), 'YYYY-M-D h:mm:ss').local().toDate(),
                        schedule_expression: campaign.schedule_expression,
                        cliente: campaign.id_cliente,
                        usuario: campaign.id_usuario,
                        mailtemplate: campaign.id_mailtemplate,
                        replacement_data_url: campaign.replacement_data_url,
                        state: campaign.state,
                    }));

                } else {
                    reportError(`cx_create_campaign`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest('cx_create_campaign');
            return;
        });

        const cx_create_smart_campaign = flow(function* (id_prediction: string, callback: { (id_project: string, id_segment: string): void }) {
            addRequest(`cx_create_smart_campaign.${id_prediction}`);
            try {
                let http_response: Axios<Response<{ segment: Segment, project: Project }>> = yield  CXApi.indicators.cx_create_smart_campaign(id_prediction);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    let project = labmda_output.response[0].project;
                    let segment = labmda_output.response[0].segment;

                    self.projects.set(project.id_project, CxProject.create({
                        id_project: project.id_project,
                        from_address: project.from_address,
                        name: project.name,
                        desc: project.desc,
                        creation_date: moment.utc(project.creation_date, 'YYYY-M-D h:mm:ss').local().toDate(),
                        cliente: project.id_project,
                        usuario: project.id_usuario,
                        segment_count: project.segment_count,
                    }));

                    self.segments.set(segment.id_segment, CxSegment.create({
                        id_segment: segment.id_segment,
                        project: segment.id_project,
                        name: segment.name,
                        desc: segment.desc,
                        total_entries: segment.total_entries + "",
                        campaign_input_url: segment.campaign_input_url,
                        creation_time: moment.utc(segment.creation_time, 'YYYY-M-D h:mm:ss').local().toDate(),
                        channel_type: segment.channel_type,
                        cliente: segment.id_cliente,
                        usuario: segment.id_usuario,
                        campaign_count: segment.campaign_count
                    }));
                    if (callback) {
                        callback(project.id_project, segment.id_segment)
                    }

                } else {
                    reportError(`cx_create_smart_campaign`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_create_smart_campaign.${id_prediction}`);
            return;
        });
        const cx_create_project = flow(function* (name: string, desc: string, from_address: string) {
            addRequest('cx_create_project');
            try {
                let http_response: Axios<Response<Project>> = yield  CXApi.campaigns.cx_create_project(name, desc, from_address);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    let project = labmda_output.response[0];
                    self.projects.set(project.id_project, CxProject.create({
                        id_project: project.id_project,
                        from_address: project.from_address,
                        name: project.name,
                        desc: project.desc,
                        creation_date: moment.utc(project.creation_date, 'YYYY-M-D h:mm:ss').local().toDate(),
                        cliente: project.id_project,
                        usuario: project.id_usuario,
                        segment_count: project.segment_count,
                    }));

                } else {
                    reportError(`cx_create_project`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest('cx_create_project');
            return;
        });
        const cx_list_campaigns = flow(function* (force: boolean) {
            if (!force) {
                if (self.list_last_request.get('cx_list_campaigns')) {
                    if (moment(new Date()).diff(moment(self.list_last_request.get('cx_list_campaigns')), "seconds") < self.list_interval) {
                        return null;
                    }
                }
            }


            addRequest('cx_list_campaigns');

            try {
                let http_response: Axios<Response<Campaign>> = yield  CXApi.campaigns.cx_list_campaigns();
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    self.list_last_request.set('cx_list_campaigns', moment().format());

                    for (let campaign of labmda_output.response) {
                        if (!self.segments.has(campaign.id_segment)) {
                            self.segments.set(campaign.id_segment, CxSegment.create({
                                id_segment: campaign.id_segment,
                                cliente: campaign.id_cliente,
                                usuario: campaign.id_usuario,
                            }))
                        }
                        if (!self.templates.has(campaign.id_mailtemplate)) {
                            self.templates.set(campaign.id_mailtemplate, CxMailTemplate.create({
                                id_mailtemplate: campaign.id_mailtemplate,
                                creation_date: new Date(),
                                cliente: campaign.id_cliente,
                                usuario: campaign.id_usuario,
                            }))
                        }
                        if (!self.campaigns.has(campaign.id_campaign)) {
                            self.campaigns.set(campaign.id_campaign, CxCampaign.create({
                                id_campaign: campaign.id_campaign,
                                segment: campaign.id_segment,
                                creation_time: moment.utc(campaign.creation_time, 'YYYY-M-D h:mm:ss').local().toDate(),
                                name: campaign.campaign_name,
                                desc: campaign.campaign_desc,
                                email_images: campaign.email_images,
                                frequency: campaign.frequency,
                                start_time: moment.utc(campaign.start_time, 'YYYY-M-D h:mm:ss').local().toDate(),
                                end_time: moment.utc(campaign.end_time, 'YYYY-M-D h:mm:ss').local().toDate(),
                                schedule_expression: campaign.schedule_expression,
                                cliente: campaign.id_cliente,
                                usuario: campaign.id_usuario,
                                mailtemplate: campaign.id_mailtemplate,
                                replacement_data_url: campaign.replacement_data_url,
                                state: campaign.state,
                            }));
                        } else {
                            let cp: any = self.campaigns.get(campaign.id_campaign);
                            cp.segment = campaign.id_segment;
                            cp.creation_time = moment.utc(campaign.creation_time, 'YYYY-M-D h:mm:ss').local().toDate();
                            cp.desc = campaign.campaign_desc;
                            cp.email_images = campaign.email_images;
                            cp.frequency = campaign.frequency;
                            cp.start_time = moment.utc(campaign.start_time, 'YYYY-M-D h:mm:ss').local().toDate();
                            cp.end_time = moment.utc(campaign.end_time, 'YYYY-M-D h:mm:ss').local().toDate();
                            cp.schedule_expression = campaign.schedule_expression;
                            cp.mailtemplate = campaign.id_mailtemplate;
                            cp.replacement_data_url = campaign.replacement_data_url;
                            cp.state = campaign.state;
                        }

                    }
                } else {
                    reportError(`cx_list_campaigns`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest('cx_list_campaigns');
            return;
        });

        const cx_list_smscampaigns = flow(function* (force: boolean) {
            if (!force) {
                if (self.list_last_request.get('cx_list_smscampaigns')) {
                    if (moment(new Date()).diff(moment(self.list_last_request.get('cx_list_smscampaigns')), "seconds") < self.list_interval) {
                        return null;
                    }
                }
            }


            addRequest('cx_list_smscampaigns');

            try {
                let http_response: Axios<ResponseNew<SmsCampaign>> = yield  CXApi.campaigns.cx_list_smscampaigns();
                let labmda_output = check_login(http_response.data);
                if (labmda_output.statusCode == 200) {
                    self.list_last_request.set('cx_list_smscampaigns', moment().format());

                    for (let campaign of labmda_output.body.smscampaigns) {
                        if (!self.segments.has(campaign.id_segment)) {
                            self.segments.set(campaign.id_segment, CxSegment.create({
                                id_segment: campaign.id_segment,
                                cliente: campaign.id_cliente,
                                usuario: campaign.id_usuario,
                            }))
                        }
                        if (!self.sms_templates.has(campaign.id_smstemplate)) {
                            self.sms_templates.set(campaign.id_smstemplate, CxSmsTemplate.create({
                                id_smstemplate: campaign.id_smstemplate,
                                creation_date: new Date(),
                                cliente: campaign.id_cliente,
                                usuario: campaign.id_usuario,
                            }))
                        }
                        if (!self.sms_campaigns.has(campaign.id_smscampaign)) {
                            self.sms_campaigns.set(campaign.id_smscampaign, CxSmsCampaign.create({
                                id_smscampaign: campaign.id_smscampaign,
                                segment: campaign.id_segment,
                                creation_time: moment.utc(campaign.creation_time, 'YYYY-M-D h:mm:ss').local().toDate(),
                                name: campaign.name,
                                desc: campaign.desc,
                                schedule_expression: campaign.schedule_expression,
                                cliente: campaign.id_cliente,
                                usuario: campaign.id_usuario,
                                smstemplate: campaign.id_smstemplate,
                                state: campaign.state,
                            }));
                        } else {
                            let cp: any = self.sms_campaigns.get(campaign.id_smscampaign);
                            cp.segment = campaign.id_segment;
                            cp.creation_time = moment.utc(campaign.creation_time, 'YYYY-M-D h:mm:ss').local().toDate();
                            cp.desc = campaign.desc;
                            cp.schedule_expression = campaign.schedule_expression;
                            cp.smstemplate = campaign.id_smstemplate;
                            cp.state = campaign.state;
                        }

                    }
                } else {
                    reportError(`cx_list_smscampaigns`, labmda_output.error_id, labmda_output.statusCode)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest('cx_list_smscampaigns');
            return;
        });

        const cx_campaign_statistics = flow(function* (id_campaign: string) {
            addRequest(`cx_campaign_statistics.${id_campaign}`);
            try {
                let http_response: Axios<Response<Campaign>> = yield  CXApi.campaigns.cx_campaign_statistics(id_campaign);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    for (let campaign of labmda_output.response) {
                        //let cp = self.campaigns.get(campaign.id_campaign)!;
                        //cp.stats_data = campaign.stats_data;
                    }
                } else {
                    reportError(`cx_campaign_statistics`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_campaign_statistics.${id_campaign}`);
            return;
        });

        const cx_campaign_state = flow(function* (id_campaign: string) {
            addRequest(`cx_campaign_state.${id_campaign}`);
            try {
                let http_response: Axios<Response<Campaign>> = yield  CXApi.campaigns.cx_campaign_state(id_campaign);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    for (let campaign of labmda_output.response) {
                        let cp = self.campaigns.get(campaign.id_campaign)!;
                        cp.state = campaign.state;
                    }
                } else {
                    reportError(`cx_campaign_state`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_campaign_state.${id_campaign}`);
            return;
        });

        const cx_smscampaign_state = flow(function* (id_smscampaign: string) {
            addRequest(`cx_smscampaign_state.${id_smscampaign}`);
            try {
                let http_response: Axios<Response<SmsCampaign>> = yield  CXApi.campaigns.cx_smscampaign_state(id_smscampaign);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    for (let campaign of labmda_output.response) {
                        let cp = self.sms_campaigns.get(campaign.id_campaign)!;
                        cp.state = campaign.state;
                    }
                } else {
                    reportError(`cx_smscampaign_state`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_smscampaign_state.${id_smscampaign}`);
            return;
        });

        const cx_list_projects = flow(function* (force: boolean) {
            if (!force) {
                if (self.list_last_request.get('cx_list_projects')) {
                    if (moment(new Date()).diff(moment(self.list_last_request.get('cx_list_projects')), "seconds") < self.list_interval) {
                        return null;
                    }
                }
            }

            addRequest('cx_list_projects');
            try {
                let http_response: Axios<Response<Project>> = yield  CXApi.campaigns.cx_list_projects();
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    self.list_last_request.set('cx_list_projects', moment().format());
                    for (let project of labmda_output.response) {
                        self.projects.set(project.id_project, CxProject.create({
                            id_project: project.id_project,
                            from_address: project.from_address,
                            name: project.name,
                            desc: project.desc,
                            creation_date: moment.utc(project.creation_date, 'YYYY-M-D h:mm:ss').local().toDate(),
                            cliente: project.id_project,
                            usuario: CXApi.options.id_usuario,
                            segment_count: project.segment_count,

                        }));
                    }
                } else {
                    reportError(`cx_list_projects`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest('cx_list_projects');
            return;
        });

        const cx_delete_campaign = flow(function* (id_campaign: string) {
            addRequest(`cx_delete_campaign.${id_campaign}`);
            try {
                let http_response: Axios<Response<SmsCampaign>> = yield  CXApi.campaigns.cx_delete_campaign(id_campaign);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    for (let campaign of labmda_output.response) {
                        for (const event of Array.from(self.postmark_events.values())) {
                            if (event.campaign.id_campaign == campaign.id_campaign) {
                                self.postmark_events.delete(event.id)
                            }
                        }
                        self.sms_campaigns.delete(campaign.id_campaign)

                    }
                } else {
                    reportError(`cx_delete_campaign`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_delete_campaign.${id_campaign}`);
            return;
        });

        const cx_delete_smscampaign = flow(function* (id_smscampaign: string) {
            addRequest(`cx_delete_smscampaign.${id_smscampaign}`);
            try {
                let http_response: Axios<ResponseNew<SmsCampaign>> = yield  CXApi.campaigns.cx_delete_smscampaign(id_smscampaign);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.statusCode == 200) {
                    for (let campaign of labmda_output.body.smscampaigns) {
                        for (const event of Array.from(self.postmark_events.values())) {
                            if (event.campaign.id_campaign == campaign.id_smscampaign) {
                                self.postmark_events.delete(event.id)
                            }
                        }
                        self.campaigns.delete(campaign.id_smscampaign)

                    }
                } else {
                    reportError(`cx_delete_smscampaign`, labmda_output.error_id, labmda_output.statusCode)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_delete_smscampaign.${id_smscampaign}`);
            return;
        });

        const cx_delete_project = flow(function* (id_project: string) {
            addRequest(`cx_delete_project.${id_project}`);
            try {
                let http_response: Axios<Response<ProjectDeletion>> = yield  CXApi.campaigns.cx_delete_project(id_project);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    for (let project of labmda_output.response) {
                        self.projects.delete(project.id_project)

                    }
                } else {
                    reportError(`cx_delete_project`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_delete_project.${id_project}`);
            return;
        });

        const cx_create_segment = flow(function* (id_project: string, campaign_input_url: string, channel_type: string, name: string, desc: string) {
            addRequest('cx_create_segment');
            try {
                let http_response: Axios<Response<Segment>> = yield  CXApi.campaigns.cx_create_segment(id_project, campaign_input_url, channel_type, name, desc);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    for (let segment of labmda_output.response) {
                        self.segments.set(segment.id_segment, CxSegment.create({
                            id_segment: segment.id_segment,
                            project: segment.id_project,
                            name: segment.name,
                            desc: segment.desc,
                            total_entries: segment.total_entries + "",
                            campaign_input_url: segment.campaign_input_url,
                            creation_time: moment.utc(segment.creation_time, 'YYYY-M-D h:mm:ss').local().toDate(),
                            channel_type: segment.channel_type,
                            cliente: segment.id_cliente,
                            usuario: segment.id_usuario,
                            campaign_count: segment.campaign_count
                        }));
                    }
                } else {
                    reportError(`cx_create_segment`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest('cx_create_segment');
            return;
        });
        const cx_upload_segment = flow(function* (name: string, extension: string, content: string, callback: { (url: string): void }) {
            addRequest(`cx_upload_segment`);

            try {
                let http_response: Axios<Response<UploadFile>> = yield  CXApi.campaigns.cx_upload_segment(name, extension, content);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    callback(labmda_output.response[0].url)
                } else {
                    reportError(`cx_upload_segment`, labmda_output.error_id, labmda_output.status)
                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_upload_segment`);
            return;
        });

        const cx_uploads3_traininput = flow(function* (name: string, extension: string, content: string, callback: { (url: string): void }) {
            addRequest(`cx_uploads3_traininput`);

            try {
                let http_response: Axios<Response<UploadFile>> = yield  CXApi.trainings.cx_uploads3_traininput(name, extension, content);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    callback(labmda_output.response[0].url)
                } else {
                    reportError(`cx_upload_segment`, labmda_output.error_id, labmda_output.status)
                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_uploads3_traininput`);
            return;
        });

        const cx_uploadimages_campaign = flow(function* (name: string, extension: string, content: string, callback: { (url: string): void }) {
            addRequest(`cx_uploadimages_campaign`);

            try {
                let http_response: Axios<Response<UploadFile>> = yield  CXApi.campaigns.cx_uploadimages_campaign(name, extension, content);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    callback(labmda_output.response[0].url)
                } else {
                    reportError(`cx_uploadimages_campaign`, labmda_output.error_id, labmda_output.status)
                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_uploadimages_campaign`);
            return;
        });
        const cx_list_segments = flow(function* (id_project: string, force: boolean) {
            if (!force) {
                if (self.list_last_request.get(`cx_list_segments.${id_project}`)) {
                    if (moment(new Date()).diff(moment(self.list_last_request.get(`cx_list_segments.${id_project}`)), "seconds") < self.list_interval) {
                        return null;
                    }
                }

            }

            addRequest('cx_list_segments');
            addRequest(`cx_list_segments.${id_project}`);

            try {
                let http_response: Axios<Response<Segment>> = yield  CXApi.campaigns.cx_list_segments(id_project);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    self.list_last_request.set(`cx_list_segments.${id_project}`, moment().format());
                    for (let segment of labmda_output.response) {
                        self.segments.set(segment.id_segment, CxSegment.create({
                            id_segment: segment.id_segment,
                            project: segment.id_project,
                            name: segment.name,
                            desc: segment.desc,
                            total_entries: segment.total_entries + "",
                            campaign_input_url: segment.campaign_input_url,
                            creation_time: moment.utc(segment.creation_time, 'YYYY-M-D h:mm:ss').local().toDate(),
                            channel_type: segment.channel_type,
                            campaign_count: segment.campaign_count,
                            cliente: segment.id_cliente,
                            usuario: segment.id_usuario,
                        }));
                    }
                } else {
                    reportError(`cx_list_segments`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest('cx_list_segments');
            removeRequest(`cx_list_segments.${id_project}`);

            return;
        });

        const cx_delete_segment = flow(function* (id_segment: string) {
            addRequest(`cx_delete_segment.${id_segment}`);
            try {
                let http_response: Axios<Response<SegmentDeletion>> = yield  CXApi.campaigns.cx_delete_segment(id_segment);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    for (let segment of labmda_output.response) {
                        self.segments.delete(segment.id_segment);
                    }
                } else {
                    reportError(`cx_delete_segment`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_delete_segment.${id_segment}`);
            return;
        });

        const cx_create_template = flow(function* (name: string, desc: string, title: string, body: string) {
            addRequest('cx_create_template');
            try {
                let http_response: Axios<Response<TemplateModel>> = yield  CXApi.templates.cx_create_template(name, desc, title, body);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    let template = labmda_output.response[0];
                    self.templates.set(template.id_mailtemplate, CxMailTemplate.create({
                        id_mailtemplate: template.id_mailtemplate,
                        name: template.name,
                        desc: template.desc,
                        creation_date: moment.utc(template.creation_date, 'YYYY-M-D h:mm:ss').local().toDate(),
                        title: template.title,
                        body: template.body,
                        images: template.images,
                        cliente: template.id_cliente,
                        usuario: template.id_usuario,
                        campaign_count: template.campaign_count,
                    }));

                } else {
                    reportError(`cx_create_template`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest('cx_create_template');
            return;
        });

        const cx_create_smstemplate = flow(function* (name: string, desc: string, body: string) {
            addRequest('cx_create_smstemplate');
            try {
                let http_response: Axios<ResponseNew<SmsTemplateModel>> = yield  CXApi.templates.cx_create_smstemplate(name, desc, body);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.statusCode == 200) {
                    let template = labmda_output.body;
                    self.sms_templates.set(template.id_smstemplate, CxSmsTemplate.create({
                        id_smstemplate: template.id_smstemplate,
                        name: template.name,
                        // desc: template.desc,
                        creation_date: moment.utc(template.creation_date, 'YYYY-M-D h:mm:ss').local().toDate(),
                        // body: template.body,
                        cliente: template.id_cliente,
                        usuario: template.id_usuario,
                    }));

                } else {
                    reportError(`cx_create_smstemplate`, labmda_output.error_id, labmda_output.statusCode)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest('cx_create_smstemplate');
            return;
        });

        const cx_list_templates = flow(function* (force: boolean) {
            if (!force) {
                if (self.list_last_request.get('cx_list_templates')) {
                    if (moment(new Date()).diff(moment(self.list_last_request.get('cx_list_templates')), "seconds") < self.list_interval) {
                        return null;
                    }
                }
            }

            addRequest('cx_list_templates');
            try {
                let http_response: Axios<Response<TemplateModel>> = yield  CXApi.templates.cx_list_templates();
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    self.list_last_request.set('cx_list_templates', moment().format());
                    for (const template of labmda_output.response) {
                        self.templates.set(template.id_mailtemplate, CxMailTemplate.create({
                            id_mailtemplate: template.id_mailtemplate,
                            name: template.name,
                            desc: template.desc,
                            creation_date: moment.utc(template.creation_date, 'YYYY-M-D h:mm:ss').local().toDate(),
                            title: template.title,
                            body: template.body,
                            images: template.images,
                            cliente: template.id_cliente,
                            usuario: template.id_usuario,
                            campaign_count: template.campaign_count,
                        }));
                    }

                } else {
                    reportError(`cx_list_templates`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest('cx_list_templates');
            return;
        });

        const cx_list_smstemplates = flow(function* (force: boolean) {
            if (!force) {
                if (self.list_last_request.get('cx_list_smstemplates')) {
                    if (moment(new Date()).diff(moment(self.list_last_request.get('cx_list_smstemplates')), "seconds") < self.list_interval) {
                        return null;
                    }
                }
            }

            addRequest('cx_list_smstemplates');
            try {
                let http_response: Axios<ResponseNew<SmsTemplateModel>> = yield  CXApi.templates.cx_list_smstemplates();
                let labmda_output = check_login(http_response.data);
                if (labmda_output.statusCode == 200) {
                    self.list_last_request.set('cx_list_smstemplates', moment().format());
                    for (const template of labmda_output.body.smstemplates) {
                        self.sms_templates.set(template.id_smstemplate, CxSmsTemplate.create({
                            id_smstemplate: template.id_smstemplate,
                            name: template.name,
                            desc: template.desc,
                            creation_date: moment.utc(template.creation_date, 'YYYY-M-D h:mm:ss').local().toDate(),
                            body: template.body,
                            cliente: template.id_cliente,
                            usuario: template.id_usuario,
                            // campaign_count: template.campaign_count,
                        }));
                    }

                } else {
                    reportError(`cx_list_smstemplates`, labmda_output.error_id, labmda_output.statusCode)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest('cx_list_smstemplates');
            return;
        });

        // TODO: Add Edit and delete for sms_template

        const cx_edit_template = flow(function* (id_mailtemplate: string, input: { name?: string, desc?: string, title?: string, body?: string }) {
            addRequest(`cx_edit_template.${id_mailtemplate}`);
            try {
                let http_response: Axios<Response<TemplateModel>> = yield  CXApi.templates.cx_edit_template(id_mailtemplate, input);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    let template = labmda_output.response[0];
                    self.templates.set(template.id_mailtemplate, CxMailTemplate.create({
                        id_mailtemplate: template.id_mailtemplate,
                        name: template.name,
                        desc: template.desc,
                        creation_date: moment.utc(template.creation_date, 'YYYY-M-D h:mm:ss').local().toDate(),
                        title: template.title,
                        body: template.body,
                        images: template.images,
                        cliente: template.id_cliente,
                        usuario: template.id_usuario,
                        campaign_count: template.campaign_count,
                    }));

                } else {
                    reportError(`cx_edit_template`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_edit_template.${id_mailtemplate}`);
            return;
        });

        const cx_edit_smstemplate = flow(function* (id_smstemplate: string, input: { name?: string, desc?: string, title?: string, body?: string }) {
            addRequest(`cx_edit_smstemplate.${id_smstemplate}`);
            try {
                let http_response: Axios<ResponseNew<SmsTemplateModel>> = yield  CXApi.templates.cx_edit_smstemplate(id_smstemplate, input);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.statusCode == 200) {
                    let template = labmda_output.body;
                    self.sms_templates.set(template.id_smstemplate, CxSmsTemplate.create({
                        id_smstemplate: template.id_smstemplate,
                        // name: template.name,
                        // desc: template.desc,
                        // creation_date: moment.utc(template.creation_date, 'YYYY-M-D h:mm:ss').local().toDate(),
                        // body: template.body,
                        cliente: template.id_cliente,
                        usuario: template.id_usuario,
                        // campaign_count: template.campaign_count,
                    }));

                } else {
                    reportError(`cx_edit_smstemplate`, labmda_output.error_id, labmda_output.statusCode)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_edit_smstemplate.${id_smstemplate}`);
            return;
        });

        const cx_delete_template = flow(function* (id_mailtemplate: string) {
            addRequest(`cx_delete_template.${id_mailtemplate}`);
            try {
                let http_response: Axios<Response<TemplateDeletion>> = yield  CXApi.templates.cx_delete_template(id_mailtemplate);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    let template = labmda_output.response[0];
                    self.templates.delete(template.id_mailtemplate);

                } else {
                    reportError(`cx_delete_template`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_delete_template.${id_mailtemplate}`);
            return;
        });

        const cx_delete_smstemplate = flow(function* (id_smstemplate: string) {
            addRequest(`cx_delete_smstemplate.${id_smstemplate}`);
            try {
                let http_response: Axios<ResponseNew<SmsTemplateDeletion>> = yield  CXApi.templates.cx_delete_smstemplate(id_smstemplate);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.statusCode == 200) {
                    let template = labmda_output.body;
                    self.sms_templates.delete(template.id_smstemplate);

                } else {
                    reportError(`cx_delete_smstemplate`, labmda_output.error_id, labmda_output.statusCode)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_delete_smstemplate.${id_smstemplate}`);
            return;
        });

        const add_postmark_event = (pe: PostMarkEventApi) => {
            let id = "";
            let kind = PostmarkEventKind.none;
            if (pe.id_delivery) {
                id = `delivery_${pe.id_delivery}`;
                kind = PostmarkEventKind.delivery
            }
            if (pe.id_open) {
                id = `open_${pe.id_open}`;
                kind = PostmarkEventKind.open
            }
            if (pe.id_click) {
                id = `click_${pe.id_click}`;
                kind = PostmarkEventKind.click
            }
            if (pe.id_bounce) {
                id = `bounce_${pe.id_bounce}`;
                kind = PostmarkEventKind.bounce
            }

            self.postmark_events.set(id, PostmarkEvent.create({
                id: id,
                kind: kind,
                click_location: pe.click_location || "",
                geo_city: pe.geo_city || "",
                geo_coords: pe.geo_coords || "",
                geo_country: pe.geo_country || "",
                geo_region: pe.geo_region || "",
                cliente: pe.id_cliente,
                message_id: pe.message_id || "",
                original_link: pe.original_link || "",
                platform: pe.platform || "",
                query: pe.query || null,
                query_class: pe.query_class || null,
                received_at: moment.utc(pe.received_at || pe.delivered_at || pe.bounced_at || moment.utc().format('YYYY-MM-DDThh:mm:ss'), 'YYYY-MM-DDThh:mm:ss').local().toDate(),
                recipient: pe.recipient || "",
                campaign: pe.tag
            }))

        };

        const cx_campaign_opens = flow(function* (id_campaign: string, force: boolean) {
            if (!self.campaigns.has(id_campaign)) {
                return null;
            }
            if (!force) {
                if (self.list_last_request.get(`cx_campaign_opens.${id_campaign}`)) {
                    if (moment(new Date()).diff(moment(self.list_last_request.get(`cx_campaign_opens.${id_campaign}`)), "seconds") < self.list_interval) {
                        return null;
                    }
                }
            }


            addRequest(`cx_campaign_opens.${id_campaign}`);
            try {
                let http_response: Axios<Response<PostMarkEventApi>> = yield  CXApi.campaigns.cx_campaign_opens(id_campaign);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    self.list_last_request.set(`cx_campaign_opens.${id_campaign}`, moment().format());
                    for (const pe of labmda_output.response) {
                        add_postmark_event(pe);
                    }

                } else {
                    reportError(`cx_campaign_opens`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_campaign_opens.${id_campaign}`);
            return;
        });

        const cx_campaign_clicks = flow(function* (id_campaign: string, force: boolean) {
            if (!self.campaigns.has(id_campaign)) {
                return null;
            }
            if (!force) {
                if (self.list_last_request.get(`cx_campaign_clicks.${id_campaign}`)) {
                    if (moment(new Date()).diff(moment(self.list_last_request.get(`cx_campaign_clicks.${id_campaign}`)), "seconds") < self.list_interval) {
                        return null;
                    }
                }
            }


            addRequest(`cx_campaign_clicks.${id_campaign}`);
            try {
                let http_response: Axios<Response<PostMarkEventApi>> = yield  CXApi.campaigns.cx_campaign_clicks(id_campaign);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    self.list_last_request.set(`cx_campaign_clicks.${id_campaign}`, moment().format());
                    for (const pe of labmda_output.response) {
                        add_postmark_event(pe);
                    }

                } else {
                    reportError(`cx_campaign_clicks`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_campaign_clicks.${id_campaign}`);
            return;
        });

        const cx_campaign_bounce = flow(function* (id_campaign: string, force: boolean) {


            if (!self.campaigns.has(id_campaign)) {
                return null;
            }

            if (!force) {
                if (self.list_last_request.get(`cx_campaign_bounce.${id_campaign}`)) {
                    if (moment(new Date()).diff(moment(self.list_last_request.get(`cx_campaign_bounce.${id_campaign}`)), "seconds") < self.list_interval) {
                        return null;
                    }
                }
            }


            addRequest(`cx_campaign_bounce.${id_campaign}`);
            try {
                let http_response: Axios<Response<PostMarkEventApi>> = yield  CXApi.campaigns.cx_campaign_bounce(id_campaign);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    self.list_last_request.set(`cx_campaign_bounce.${id_campaign}`, moment().format());

                    for (const pe of labmda_output.response) {
                        add_postmark_event(pe);
                    }

                } else {
                    reportError(`cx_campaign_bounce`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_campaign_bounce.${id_campaign}`);
            return;
        });

        const cx_campaign_delivery = flow(function* (id_campaign: string, force: boolean) {
            if (!self.campaigns.has(id_campaign)) {
                return null;
            }
            if (!force) {
                if (self.list_last_request.get(`cx_campaign_delivery.${id_campaign}`)) {
                    if (moment(new Date()).diff(moment(self.list_last_request.get(`cx_campaign_delivery.${id_campaign}`)), "seconds") < self.list_interval) {
                        return null;
                    }
                }
            }


            addRequest(`cx_campaign_delivery.${id_campaign}`);
            try {
                let http_response: Axios<Response<PostMarkEventApi>> = yield  CXApi.campaigns.cx_campaign_delivery(id_campaign);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    self.list_last_request.set(`cx_campaign_delivery.${id_campaign}`, moment().format());

                    for (const pe of labmda_output.response) {
                        add_postmark_event(pe);
                    }

                } else {
                    reportError(`cx_campaign_delivery`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_campaign_delivery.${id_campaign}`);
            return;
        });

        const cx_list_postmark_events = flow(function* (force: boolean) {
            yield cx_list_campaigns(force);
            for (const campaign of Array.from(self.campaigns.values())) {
                cx_campaign_opens(campaign.id_campaign, force);
                cx_campaign_clicks(campaign.id_campaign, force);
                cx_campaign_delivery(campaign.id_campaign, force);
                cx_campaign_bounce(campaign.id_campaign, force);
            }
            return null;
        });

        const cx_list_notification_reads = flow(function* (force: boolean = false) {
            if (!force) {
                if (self.list_last_request.get(`cx_list_notification_reads`)) {
                    if (moment(new Date()).diff(moment(self.list_last_request.get(`cx_list_notification_reads`)), "seconds") < self.list_interval) {
                        return null;
                    }
                }
            }

            addRequest(`cx_list_notification_reads`);

            try {
                let http_response: Axios<Response<ReadNotificationApi>> = yield  CXApi.notifications.cx_list_notification_reads();
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    self.list_last_request.set(`cx_list_notification_reads`, moment().format());
                    for (const notification_read of labmda_output.response) {
                        if (!self.notification_change.has(notification_read.id_notification_change)) {
                            self.notification_change.set(notification_read.id_notification_change, {
                                id_notification_change: notification_read.id_notification_change,
                                entity_type: "",
                                entity_id: "",
                                entity_name: "",
                                action_on_entity: "",
                                actor_id_usuario: "",
                                actor_id_cliente: "",
                                created_at: new Date(),
                            })
                        }
                        self.read_notification.set(notification_read.id_read_notification, {
                            id_read_notification: notification_read.id_read_notification,
                            id_usuario: notification_read.id_usuario,
                            notification_change: notification_read.id_notification_change,
                            read_at: moment.utc(notification_read.read_at, 'YYYY-M-D h:mm:ss').local().toDate(),
                        })
                    }

                } else {
                    reportError(`cx_list_notification_reads`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_list_notification_reads`);
            return;
        });

        const cx_list_notifications = flow(function* (force: boolean = false) {
            if (!force) {
                if (self.list_last_request.get(`cx_list_notifications`)) {
                    if (moment(new Date()).diff(moment(self.list_last_request.get(`cx_list_notifications`)), "seconds") < self.list_interval) {
                        return null;
                    }
                }
            }

            addRequest(`cx_list_notifications`);

            try {
                let http_response: Axios<Response<NotificationChangeApi>> = yield  CXApi.notifications.cx_list_notifications();
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    self.list_last_request.set(`cx_list_notifications`, moment().format());
                    for (const nc of labmda_output.response) {
                        self.notification_change.set(nc.id_notification_change, {
                            id_notification_change: nc.id_notification_change,
                            entity_type: nc.entity_type,
                            entity_id: nc.entity_id,
                            entity_name: nc.entity_name,
                            action_on_entity: nc.action_on_entity,
                            actor_id_usuario: nc.actor_id_usuario,
                            actor_id_cliente: nc.actor_id_cliente,
                            created_at: moment.utc(nc.created_at, 'YYYY-M-D h:mm:ss').local().toDate(),
                        })
                    }

                } else {
                    reportError(`cx_list_notifications`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_list_notifications`);
            return;
        });

        const cx_create_notification_read = flow(function* (id_notification_change: string) {

            addRequest(`cx_create_notification_read.${id_notification_change}`);

            try {
                let http_response: Axios<Response<NotificationChangeApi>> = yield  CXApi.notifications.cx_create_notification_read(id_notification_change);
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    for (const notification_read of labmda_output.response) {
                        self.read_notification.set(notification_read.id_read_notification, {
                            id_read_notification: notification_read.id_read_notification,
                            id_usuario: notification_read.id_usuario,
                            notification_change: notification_read.id_notification_change,
                            read_at: moment.utc(notification_read.read_at, 'YYYY-M-D h:mm:ss').local().toDate(),
                        })
                    }

                } else {
                    reportError(`cx_create_notification_read`, labmda_output.error_id, labmda_output.status)

                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_create_notification_read.${id_notification_change}`);
            return;
        });
        const cx_list_active_indicators = flow(function* () {
            addRequest(`cx_list_active_indicators`);

            try {
                let http_response: Axios<Response<PredictionResults>> = yield  CXApi.indicators.cx_list_active_indicators();
                let labmda_output = check_login(http_response.data);
                if (labmda_output.status == 200) {
                    for (let prediction_result of labmda_output.response) {

                        self.predictions.set(prediction_result.id_prediction, {
                            id_prediction: prediction_result.id_prediction,
                            on_dashboard: true,
                            cliente: prediction_result.id_cliente,
                            usuario: prediction_result.id_usuario,
                            statistic: prediction_result.id_statistic,
                        });

                        self.ml_statistics.set(prediction_result.id_statistic, CxMlStatistic.create({
                            id_statistic: prediction_result.id_statistic,
                            report_name: prediction_result.report_name,
                            report_desc: prediction_result.report_desc,
                            total_entries: prediction_result.total_entries + "",
                            positive_level_name: prediction_result.positive_level_name,
                            negative_level_name: prediction_result.negative_level_name,
                            positive_level: prediction_result.positive_level + "",
                            negative_level: prediction_result.negative_level + "",
                            positive_level_csv: prediction_result.positive_level_csv,
                            negative_level_csv: prediction_result.negative_level_csv,
                            cliente: prediction_result.id_cliente,
                            usuario: prediction_result.id_usuario,
                        }));
                    }

                } else {
                    reportError(`cx_list_active_indicators`, labmda_output.error_id, labmda_output.status)
                }
            } catch (error) {
                console.error(error)
            }
            removeRequest(`cx_list_active_indicators`);
            return;
        });

        return {
            cx_list_projects,
            cx_list_mymodels,
            cx_list_segments,
            cx_list_models,
            cx_list_trainings,
            cx_list_predictions,
            cx_list_campaigns,
            cx_list_smscampaigns,
            cx_list_templates,
            cx_list_smstemplates,
            cx_list_postmark_events,
            cx_list_notification_reads,
            cx_list_notifications,
            cx_list_active_indicators,
            cx_create_project,
            cx_create_campaign,
            cx_send_mailcampaign,
            cx_create_segment,
            cx_add_model,
            cx_training_details,
            cx_prediction_results,
            cx_prediction_state,
            cx_uploads3_predictinput,
            cx_delete_project,
            cx_create_template,
            cx_create_smstemplate,
            cx_edit_template,
            cx_edit_smstemplate,
            cx_delete_template,
            cx_delete_smstemplate,
            cx_delete_segment,
            cx_upload_segment,
            cx_campaign_state,
            cx_smscampaign_state,
            cx_campaign_statistics,
            cx_delete_campaign,
            cx_delete_smscampaign,
            cx_uploadimages_campaign,
            cx_create_prediction,
            cx_uploads3_traininput,
            cx_create_training,
            cx_campaign_opens,
            cx_campaign_clicks,
            cx_campaign_delivery,
            cx_campaign_bounce,
            cx_create_notification_read,
            cx_create_smart_campaign

        }
    }
).views(self => {
    const isLoading = function (api_id: string) {
        let count = self.api_loading.get(api_id);
        if (!count) {
            count = 0;
        }
        return count != 0;
    };
    return {isLoading}

});

let login_form: any = {};

//check if credentials are stored in localStorage
if (window.localStorage) {
    // localStorage supported
    let value = localStorage.getItem("login_form");
    if (value) {
        login_form = JSON.parse(value)
    }
}

export const CxStore = CxRoot.create({
    login_form: login_form,
});

CxStore.cx_list_notification_reads().then(() => {
    CxStore.cx_list_notifications()

});
setInterval(function () {
    CxStore.cx_list_notification_reads().then(() => {
        CxStore.cx_list_notifications()
    })
}, 10000);