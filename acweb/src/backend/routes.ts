import {RouterStore, RouterState} from "mobx-state-router";
import {CXApi} from "./api";
import {CxStore} from "./models";
import {rootStore} from "./stores";

export const routes = [
    {
        name: 'login',
        pattern: '/',
        onEnter: (fromState: any, toState: any, routerStore: RouterStore) => {
            if (CXApi.options.id_sesion) {
                rootStore.routerStore.goTo("mis_modelos", {}, {page: 1});
                return Promise.reject();
            }
            return Promise.resolve();
        }
    },
    {
        name: 'mis_modelos',
        pattern: '/mis_modelos',
        onEnter: (fromState: any, toState: any, routerStore: RouterStore) => {
            //redirect to login
            if (!CXApi.options.id_sesion) {
                rootStore.routerStore.goTo("login");
                return Promise.reject();
            }
            CxStore.cx_list_mymodels(false);
            return Promise.resolve();

        }
    },
    {
        name: 'tienda',
        pattern: '/tienda',
        onEnter: (fromState: any, toState: any, routerStore: RouterStore) => {
            //redirect to login
            if (!CXApi.options.id_sesion) {
                rootStore.routerStore.goTo("login");
                return Promise.reject();
            }
            CxStore.cx_list_models(false);
            return Promise.resolve();

        }
    },
    {
        name: 'entrenamientos',
        pattern: '/entrenamientos',
        onEnter: (fromState: any, toState: any, routerStore: RouterStore) => {
            //redirect to login
            if (!CXApi.options.id_sesion) {
                rootStore.routerStore.goTo("login");
                return Promise.reject();
            }
            CxStore.cx_list_models(false);
            let lt = CxStore.cx_list_trainings(false);
            if (toState.queryParams.id_training) {
                if (CxStore.trainings.has(toState.queryParams.id_training)) {
                    CxStore.cx_training_details(toState.queryParams.id_training);
                    CxStore.cx_list_predictions(toState.queryParams.id_training, false);

                }
                else {
                    lt.then(() => {
                        CxStore.cx_training_details(toState.queryParams.id_training);
                        CxStore.cx_list_predictions(toState.queryParams.id_training, false);

                    })
                }
            }
            return Promise.resolve();
        }
    },
    {
        name: 'analitica_por_segmento',
        pattern: '/analitica_por_segmento',
        onEnter: (fromState: any, toState: any, routerStore: RouterStore) => {
            //redirect to login
            if (!CXApi.options.id_sesion) {
                rootStore.routerStore.goTo("login");
                return Promise.reject();
            }
            return Promise.resolve();
        }
    },
    {
        name: 'analitica_por_campagna',
        pattern: '/analitica_por_campagna',
        onEnter: (fromState: any, toState: any, routerStore: RouterStore) => {
            //redirect to login
            if (!CXApi.options.id_sesion) {
                rootStore.routerStore.goTo("login");
                return Promise.reject();
            }

            let lt = CxStore.cx_list_campaigns(false);
            if (toState.queryParams.id_campaign) {
                if (CxStore.campaigns.has(toState.queryParams.id_campaign)) {
                    CxStore.cx_campaign_opens(toState.queryParams.id_campaign, false);
                    CxStore.cx_campaign_clicks(toState.queryParams.id_campaign, false);
                    CxStore.cx_campaign_delivery(toState.queryParams.id_campaign, false);
                    CxStore.cx_campaign_bounce(toState.queryParams.id_campaign, false);
                }
                else {
                    lt.then(() => {
                        CxStore.cx_campaign_opens(toState.queryParams.id_campaign, false);
                        CxStore.cx_campaign_clicks(toState.queryParams.id_campaign, false);
                        CxStore.cx_campaign_delivery(toState.queryParams.id_campaign, false);
                        CxStore.cx_campaign_bounce(toState.queryParams.id_campaign, false);
                    })
                }
            }
            return Promise.resolve();
        }


    },
    {
        name: 'proyectos',
        pattern: '/proyectos',
        onEnter: (fromState: any, toState: any, routerStore: RouterStore) => {
            //redirect to login
            if (!CXApi.options.id_sesion) {
                rootStore.routerStore.goTo("login");
                return Promise.reject();
            }

            let lt = CxStore.cx_list_projects(false);
            if (toState.queryParams.id_project) {
                if (CxStore.projects.has(toState.queryParams.id_project)) {
                    CxStore.cx_list_segments(toState.queryParams.id_project, false);

                }
                else {
                    lt.then(() => {
                        CxStore.cx_list_segments(toState.queryParams.id_project, false);

                    })
                }
            }

            return Promise.resolve();
        }
    },
    {
        name: 'templates',
        pattern: '/templates',
        onEnter: (fromState: any, toState: any, routerStore: RouterStore) => {
            //redirect to login
            if (!CXApi.options.id_sesion) {
                rootStore.routerStore.goTo("login");
                return Promise.reject();
            }
            CxStore.cx_list_templates(false);
            CxStore.cx_list_smstemplates(false);
            return Promise.resolve();
        }
    },
    {
        name: 'campaigns',
        pattern: '/campaigns',
        onEnter: (fromState: any, toState: any, routerStore: RouterStore) => {
            //redirect to login
            if (!CXApi.options.id_sesion) {
                rootStore.routerStore.goTo("login");
                return Promise.reject();
            }
            let lt1 = CxStore.cx_list_campaigns(false);
            let lt2 = CxStore.cx_list_smscampaigns(false);
            if (toState.queryParams.id_campaign) {
                if (CxStore.campaigns.has(toState.queryParams.id_campaign)) {
                    CxStore.cx_campaign_opens(toState.queryParams.id_campaign, false);
                    CxStore.cx_campaign_clicks(toState.queryParams.id_campaign, false);
                    CxStore.cx_campaign_delivery(toState.queryParams.id_campaign, false);
                    CxStore.cx_campaign_bounce(toState.queryParams.id_campaign, false);
                }
                else {
                    lt1.then(() => {
                        CxStore.cx_campaign_opens(toState.queryParams.id_campaign, false);
                        CxStore.cx_campaign_clicks(toState.queryParams.id_campaign, false);
                        CxStore.cx_campaign_delivery(toState.queryParams.id_campaign, false);
                        CxStore.cx_campaign_bounce(toState.queryParams.id_campaign, false);
                    });
                    lt2.then(() => {
                        CxStore.cx_campaign_opens(toState.queryParams.id_campaign, false);
                        CxStore.cx_campaign_clicks(toState.queryParams.id_campaign, false);
                        CxStore.cx_campaign_delivery(toState.queryParams.id_campaign, false);
                        CxStore.cx_campaign_bounce(toState.queryParams.id_campaign, false);
                    })
                }
            }

            return Promise.resolve();
        }
    },
    {
        name: 'events',
        pattern: '/events',
        onEnter: (fromState: any, toState: any, routerStore: RouterStore) => {
            //redirect to login
            if (!CXApi.options.id_sesion) {
                rootStore.routerStore.goTo("login");
                return Promise.reject();
            }

            CxStore.cx_list_templates(false);
            CxStore.cx_list_smstemplates(false);

            if (toState.queryParams.id_campaign) {
                CxStore.cx_list_campaigns(false).then(() => {
                    CxStore.cx_campaign_clicks(toState.queryParams.id_campaign, false);
                    CxStore.cx_campaign_opens(toState.queryParams.id_campaign, false);
                    CxStore.cx_campaign_delivery(toState.queryParams.id_campaign, false);
                    CxStore.cx_campaign_bounce(toState.queryParams.id_campaign, false);
                })

            } else {
                CxStore.cx_list_postmark_events(false);
            }

            return Promise.resolve();
        }
    },
    {
        name: 'indicators',
        pattern: '/indicators',
        onEnter: (fromState: any, toState: any, routerStore: RouterStore) => {
            //redirect to login
            if (!CXApi.options.id_sesion) {
                rootStore.routerStore.goTo("login");
                return Promise.reject();
            }

            CxStore.cx_list_active_indicators();
            return Promise.resolve();
        }
    },
    {
        name: 'notFound',
        pattern: '/not-found'
    }
];