import { RouterState, RouterStore } from 'mobx-state-router';
import { routes } from './routes';

const notFound = new RouterState('notFound');

class RootStore  {
    routerStore = new RouterStore(this, routes, notFound)
}

// Create the rootStore
export const rootStore = new RootStore();
