import {Axios, Endpoint, Response} from "./base"
import {PredictionResults} from "./predictions"
import {Segment, Project} from "./campaigns";


export class Indicators extends Endpoint {

    async cx_list_active_indicators(): Promise<Response<PredictionResults>> {
        return this.call_api('cx_list_active_indicators');
    }

    async cx_create_smart_campaign(id_prediction: string): Promise<Axios<Response<{ segment: Segment, project: Project }>>> {
        return this.call_api('cx_create_smart_campaign', {id_prediction: id_prediction});
    }
}
