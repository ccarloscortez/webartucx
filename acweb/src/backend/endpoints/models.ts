import {Endpoint, Response, Axios} from "./base"

export interface Model {
    id_model: string
    name: string
    desc: any
    category: string
    industry: string
    version: string
    creation_time: string
}

export interface ListModelResponse extends Model{
    addition_date: string

}

export class Models extends Endpoint {
    async cx_list_mymodels(): Promise<Axios<Response<ListModelResponse>>> {
        return this.call_api('cx_list_mymodels');
    }
}