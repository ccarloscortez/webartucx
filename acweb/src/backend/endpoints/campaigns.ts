import {Response, ResponseNew, Endpoint, Axios} from "./base";
import {UploadFile} from "./predictions";


export interface Project {
    id_project: string,
    name: string,
    desc: string,
    creation_date: string,
    id_cliente: string,
    id_usuario: string,
    from_address: string,
    segment_count: number,
}


export interface Segment {
    id_segment: string,
    id_project: string,
    name: string,
    desc: string,
    campaign_input_url: string,
    total_entries: string,
    creation_time: string,
    id_cliente: string,
    id_usuario: string,
    channel_type: string,
    campaign_count: number
}

export interface Campaign {
    id_campaign: string,
    id_segment: string,
    creation_time: string,
    campaign_name: string,
    campaign_desc: string,
    email_title: string,
    email_body: string,
    email_images: any,
    frequency: string,
    start_time: string,
    end_time: string,
    timezone: string,
    id_cliente: string,
    id_usuario: string,
    state: boolean,
    id_mailtemplate: string,
    replacement_data_url: string,
    schedule_expression: string,
    stats_data?: PostMarkStats
}

export interface SmsCampaign {
    batch_ids: any,
    id_cliente: string,
    creation_time: string,
    desc: string,
    from_address: number,
    id_campaign: string,
    name: string,
    schedule_expression: string,
    id_segment: string,
    send_now: boolean,
    id_smstemplate: string,
    state: boolean,
    timezone: string,
    id_usuario: string,
}


interface PostMarkStats {
    BounceRate: number,
    Bounced: number,
    Opens: number,
    SMTPApiErrors: number,
    Sent: number,
    SpamComplaints: number,
    SpamComplaintsRate: number,
    TotalClicks: number,
    TotalTrackedLinksSent: number,
    Tracked: number,
    UniqueLinksClicked: number,
    UniqueOpens: number,
    WithClientRecorded: number,
    WithLinkTracking: number,
    WithOpenTracking: number,
    WithPlatformRecorded: number,
    WithReadTimeRecorded: number
}

export interface PostMarkEventApi {
    click_location: string,
    geo_city: string,
    geo_coords: string,
    geo_country: string,
    geo_region: string,
    id_cliente: string,
    message_id: string,
    original_link: string,
    platform: string,
    query: any,
    query_class: any,
    received_at?: string,
    delivered_at?: string,
    bounced_at?: string,
    recipient: string,
    tag: string,
    id_delivery?: number,
    id_open?: number,
    id_click?: number,
    id_bounce?: number,
}

export interface SegmentDeletion {
    id_segment: string
}

export interface ProjectDeletion {
    id_project: string
}

export class Campaigns extends Endpoint {
    async cx_create_project(name: string, desc: string, from_address: string): Promise<Axios<Response<Project>>> {
        return this.call_api('cx_create_project', {
            name: name,
            desc: desc,
            from_address: from_address
        });
    }

    async cx_list_projects(): Promise<Axios<Response<Project>>> {
        return this.call_api('cx_list_projects');
    }

    async cx_delete_project(id_project: string): Promise<Axios<Response<ProjectDeletion>>> {
        return this.call_api('cx_delete_project', {id_project: id_project});
    }


    async cx_create_campaign(id_segment: string,
                             name: string,
                             desc: string,
                             frequency: string,
                             start_time: string,
                             end_time: string,
                             schedule_expression: string,
                             id_mailtemplate: string,
                             email_images: any): Promise<Axios<Response<Campaign>>> {
        return this.call_api('cx_create_campaign', {
            id_segment: id_segment,
            campaign_name: name,
            campaign_desc: desc,
            frequency: frequency,
            start_time: start_time,
            end_time: end_time,
            schedule_expression: schedule_expression,
            id_mailtemplate: id_mailtemplate,
            email_images: email_images,
        });
    }

    async cx_send_mailcampaign(id_segment: string,
                               name: string,
                               desc: string,
                               frequency: string,
                               start_time: string,
                               end_time: string,
                               schedule_expression: string,
                               id_mailtemplate: string,
                               email_images: any): Promise<Axios<Response<Campaign>>> {
        return this.call_api('cx_send_mailcampaign', {
            id_segment: id_segment,
            campaign_name: name,
            campaign_desc: desc,
            frequency: frequency,
            start_time: start_time,
            end_time: end_time,
            schedule_expression: schedule_expression,
            id_mailtemplate: id_mailtemplate,
            email_images: email_images,
        });
    }

    async cx_list_campaigns(): Promise<Axios<Response<Campaign>>> {
        return this.call_api('cx_list_campaigns');
    }

    async cx_list_smscampaigns(): Promise<Axios<ResponseNew<SmsCampaign>>> {
        return this.call_api('cx_list_smscampaigns');
    }

    async cx_campaign_state(id_campaign: string): Promise<Axios<Response<Campaign>>> {
        return this.call_api('cx_campaign_state', {id_campaign: id_campaign, type: 'mail'});
    }

    async cx_smscampaign_state(id_smscampaign: string): Promise<Axios<ResponseNew<SmsCampaign>>> {
        return this.call_api('cx_campaign_state', {id_campaign: id_smscampaign, type: 'sms'});
    }

    async cx_campaign_statistics(id_campaign: string): Promise<Axios<Response<Campaign>>> {
        return this.call_api('cx_campaign_statistics', {id_campaign: id_campaign});
    }

    async cx_delete_campaign(id_campaign: string): Promise<Axios<Response<Campaign>>> {
        return this.call_api('cx_delete_campaign', {id_campaign: id_campaign});
    }

    async cx_delete_smscampaign(id_campaign: string): Promise<Axios<ResponseNew<SmsCampaign>>> {
        return this.call_api('cx_delete_smscampaign', {id_smscampaign: id_campaign});
    }

    async cx_uploadimages_campaign(name: string, extension: string, content: string): Promise<Response<UploadFile>> {
        return this.call_api('cx_uploadimages_campaign', {name: name, extension: extension, content: content});
    }

    async cx_create_segment(id_project: string, campaign_input_url: string, channel_type: string, name: string, desc: string): Promise<Axios<Response<Segment>>> {
        return this.call_api('cx_create_segment', {
            id_project: id_project,
            campaign_input_url: campaign_input_url,
            channel_type: channel_type,
            name: name,
            desc: desc
        });
    }

    async cx_list_segments(id_project: string): Promise<Axios<Response<Segment>>> {
        return this.call_api('cx_list_segments', {id_project: id_project});
    }

    async cx_delete_segment(id_segment: string): Promise<Axios<Response<SegmentDeletion>>> {
        return this.call_api('cx_delete_segment', {id_segment: id_segment});
    }

    async cx_upload_segment(name: string, extension: string, content: string): Promise<Response<UploadFile>> {
        return this.call_api('cx_upload_segment', {name: name, extension: extension, content: content});
    }

    async cx_campaign_opens(tag: string): Promise<Response<PostMarkEventApi>> {
        return this.call_api('cx_campaign_opens', {tag: tag});
    }

    async cx_campaign_clicks(tag: string): Promise<Response<PostMarkEventApi>> {
        return this.call_api('cx_campaign_clicks', {tag: tag});
    }

    async cx_campaign_delivery(tag: string): Promise<Response<PostMarkEventApi>> {
        return this.call_api('cx_campaign_delivery', {tag: tag});
    }

    async cx_campaign_bounce(tag: string): Promise<Response<PostMarkEventApi>> {
        return this.call_api('cx_campaign_bounce', {tag: tag});
    }

    async cx_schedule_smscampaign(id_segment: string, name: string, desc: string, scheduleExpression: string, id_smstemplate: string): Promise<Response<any>> {
        return this.call_api('cx_schedule_smscampaign', {
            name: name,
            desc: desc,
            send_now: false,
            state: true,
            scheduleExpression: scheduleExpression,
            id_smstemplate: id_smstemplate,
            id_segment: id_segment
        });

    }

    async cx_send_sms_now(id_segment: string, name: string, desc: string, id_smstemplate: string): Promise<Response<any>> {
        return this.call_api('cx_send_sms_now', {
            name: name,
            desc: desc,
            send_now: true,
            state: true,
            id_smstemplate: id_smstemplate,
            id_segment: id_segment
        });

    }

}

