import {Endpoint, Response} from "./base"
import {UploadFile} from "./predictions";

export interface Training {
    creation_time: string,
    id_training: string,
    id_model: string,
    state: string,
    type: string,
    subtype: string,
    modeling_data: any,
    is_free: string,
    id_metric: string,
    name: string,
}


export interface TrainingDetail {
    id_training: string,
    id_metric: string,
    type: string,
    accuracy: string,
    fscore: string,
    recall: string,
    precision: string,
    roc: string,
    confusion_matrix: any,
    roc_curve: any,
    importances: any

}

export interface TrainingCreationInput {
    id_model: string,
    name: string,
    type: string,
    dataset_url_training: string,
    training_label: string,
    subtype: string,
    is_free?: boolean,

}

export class Trainings extends Endpoint {
    async cx_list_trainings(): Promise<Response<Training>> {
        return this.call_api('cx_list_trainings');
    }

    async cx_training_details(id_metric: string): Promise<Response<TrainingDetail>> {
        return this.call_api('cx_training_details', {id_metric: id_metric});
    }

    async cx_uploads3_traininput(name: string, extension: string, content: string): Promise<Response<UploadFile>> {
        return this.call_api('cx_uploads3_traininput', {name: name, extension: extension, content: content});
    }

    async cx_create_training(input: TrainingCreationInput): Promise<Response<Training>> {
        input.is_free = false;
        return this.call_api('cx_create_training', input);

    }

}
