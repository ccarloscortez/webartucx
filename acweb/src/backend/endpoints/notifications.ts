import {Response, Endpoint, Axios} from "./base";

export interface NotificationChangeApi {
    id_notification_change: string,
    entity_type: string,
    entity_id: string,
    entity_name: string,
    action_on_entity: string,
    actor_id_usuario: string,
    actor_id_cliente: string,
    created_at: string,
}


export interface ReadNotificationApi {
    id_read_notification: string,
    id_usuario: string,
    id_notification_change: string,
    read_at: string,
}

export class Notifications extends Endpoint {
    async cx_create_notification_read(id_notification_change: string): Promise<Axios<Response<ReadNotificationApi>>> {
        return this.call_api('cx_create_notification_read', {
            id_notification_change: id_notification_change
        });
    }

    async cx_list_notification_reads(): Promise<Axios<Response<ReadNotificationApi>>> {
        return this.call_api('cx_list_notification_reads');
    }

    async cx_list_notifications(): Promise<Axios<Response<NotificationChangeApi>>> {
        return this.call_api('cx_list_notifications');
    }
}

