import {Endpoint, Response} from "./base"
import {Model} from "./models"


export interface ClientModel{
    id_cliente: string
    id_model: string
    addition_date: string
    addition_user: string
}

export interface ListMarketplaceModelResponse extends Model{
    is_added:boolean,
}

export class Marketplace extends Endpoint {
    async cx_add_model(id_model: string): Promise<Response<ClientModel>> {
        return this.call_api('cx_add_model', {id_model: id_model});
    }

    async cx_list_models(): Promise<Response<ListMarketplaceModelResponse>> {
        return this.call_api('cx_list_models');
    }
}
