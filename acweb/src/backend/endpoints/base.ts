import axios from 'axios';

export interface Response<T> {
    status: number,
    response: Array<T>,
    error_id: string,
}

export interface ResponseNew<T> {
    statusCode: number,
    body: any,
    error_id: string,
}

export interface Axios<T> {
    data: T;
}

export interface Options {
    url: string,
    id_app?: string,
    id_cliente?: string,
    id_usuario?: string,
    id_sesion?: string,
}

export class Endpoint {
    protected options: Options;

    constructor(options: Options) {
        this.options = options

    }

    protected async call_api(id_api: string, DATA_IN?: any): Promise<any> {
        let p = axios.post(this.options.url, JSON.stringify({
            id_app: this.options.id_app,
            id_cliente: this.options.id_cliente,
            id_usuario: this.options.id_usuario || "default",
            id_sesion: this.options.id_sesion || "0",
            id_api: id_api,
            DATA_IN: DATA_IN || {},
        }), {
            headers: {
                'Content-Type': 'application/json',
            }
        });

        return p
    }
}