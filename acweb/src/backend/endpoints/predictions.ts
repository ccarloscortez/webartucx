import {Endpoint, Response} from "./base"

export interface Prediction {
    id_prediction: string,
    id_training: string,
    name: string,
    creation_time: string,
    on_dashboard: string,
    id_job: string,
    job_type: string,
    job_start_time: string,
    job_state: string,
    input_csv_url: string,
    output_csv_url: string,
    job_finish_time: string,
    id_cliente: string,
    id_usuario: string,
    id_statistic: string,
}

export interface PredictionResults {
    id_prediction: string,
    id_statistic: string,
    report_name: string,
    report_desc: string,
    total_entries: string,
    positive_level_name: string,
    negative_level_name: string,
    positive_level: string,
    negative_level: string,
    positive_level_csv: string,
    negative_level_csv: string,
    id_cliente: string,
    id_usuario: string,
}


export interface PredictionState {
    fecha: string,
    id_prediction: string,
    on_dashboard: boolean,
    id_cliente: string,
    id_usuario: string
}
export interface UploadFile {
    url: string
}

export class Predictions extends Endpoint {
    async cx_list_predictions(id_training: string): Promise<Response<Prediction>> {
        return this.call_api('cx_list_predictions', {id_training: id_training});
    }

    async cx_prediction_results(id_prediction: string): Promise<Response<PredictionResults>> {
        return this.call_api('cx_prediction_results', {id_prediction: id_prediction});
    }

    async cx_prediction_state(id_prediction: string): Promise<Response<PredictionState>> {
        return this.call_api('cx_prediction_state', {id_prediction: id_prediction});
    }

    async cx_uploads3_predictinput(name: string, extension: string, content: string): Promise<Response<UploadFile>> {
        return this.call_api('cx_uploads3_predictinput', {name: name, extension: extension, content: content});
    }

    async cx_create_prediction(id_training: string, name: string, input_csv_url: string): Promise<Response<Prediction>> {
        return this.call_api('cx_create_prediction', {
            id_training: id_training,
            name: name,
            input_csv_url: input_csv_url
        });
    }
}
