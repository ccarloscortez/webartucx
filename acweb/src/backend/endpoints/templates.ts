import {Endpoint, Response, ResponseNew} from "./base"

export interface TemplateModel {
    creation_date: string,
    id_cliente: string,
    id_usuario: string,
    id_mailtemplate: string,
    name: string,
    desc: string,
    title: string,
    body: string,
    images: any,
    campaign_count: number
}

export interface SmsTemplateModel {
    creation_date: string,
    id_cliente: string,
    id_usuario: string,
    id_smstemplate: string,
    name: string,
    desc: string,
    body: string,
}

export interface TemplateDeletion {
    id_mailtemplate: string,
}

export interface SmsTemplateDeletion {
    id_smstemplate: string,
}

export class Templates extends Endpoint {
    async cx_create_template(name: string, desc: string, title: string, body: string): Promise<Response<TemplateModel>> {
        return this.call_api('cx_create_template', {
            name: name,
            desc: desc,
            body: body,
            title: title,
        });
    }

    async cx_create_smstemplate(name: string, desc: string, body: string): Promise<ResponseNew<SmsTemplateModel>> {
        return this.call_api('cx_create_smstemplate', {
            name: name,
            desc: desc,
            body: body,
        });
    }

    async cx_delete_template(id_mailtemplate: string): Promise<Response<TemplateDeletion>> {
        return this.call_api('cx_delete_template', {id_mailtemplate: id_mailtemplate});
    }

    async cx_delete_smstemplate(id_smstemplate: string): Promise<ResponseNew<SmsTemplateDeletion>> {
        return this.call_api('cx_delete_smstemplate', {id_smstemplate: id_smstemplate});
    }

    async cx_list_templates(): Promise<Response<TemplateModel>> {
        return this.call_api('cx_list_templates');
    }

    async cx_list_smstemplates(): Promise<ResponseNew<SmsTemplateModel>> {
        return this.call_api('cx_list_smstemplates');
    }

    async cx_edit_template(id_mailtemplate: string, input?: { name?: string, desc?: string, body?: string, title?: string }): Promise<Response<TemplateModel>> {
        return this.call_api('cx_edit_template', {
            id_mailtemplate: id_mailtemplate,
            desc: input && input.desc,
            title: input && input.title,
            body: input && input.body,
            name: input && input.name,

        });
    }

    async cx_edit_smstemplate(id_smstemplate: string, input?: { name?: string, desc?: string, body?: string }): Promise<ResponseNew<SmsTemplateModel>> {
        return this.call_api('cx_edit_smstemplate', {
            id_smstemplate: id_smstemplate,
            desc: input && input.desc,
            body: input && input.body,
            name: input && input.name,
        });
    }
}
