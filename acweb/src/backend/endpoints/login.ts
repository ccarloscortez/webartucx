import {Endpoint, Axios} from "./base"

export interface LoginResponse {
    elements?:null,
    id_tx: string,
    resp_code: string,
    resp_message: string,
    first_name?: string,
    id_sesion?: string,
    profile_pic?: string,

}

export class Login extends Endpoint {
    async login(username:string, password:string): Promise<Axios<LoginResponse>> {
        return this.call_api('login',{username:username, password:password});
    }
}