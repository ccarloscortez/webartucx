import {Models} from "./endpoints/models";
import {Marketplace} from "./endpoints/marketplace";
import {Predictions} from "./endpoints/predictions";
import {Templates} from "./endpoints/templates";
import {Trainings} from "./endpoints/trainings";
import {Indicators} from "./endpoints/indicators";
import {Campaigns} from "./endpoints/campaigns";
import {Login} from "./endpoints/login";
import {Options} from "./endpoints/base";
import {Notifications} from "./endpoints/notifications";

class Api {
    models: Models;
    marketplace: Marketplace;
    predictions: Predictions;
    templates: Templates;
    trainings: Trainings;
    indicators: Indicators;
    campaigns: Campaigns;
    login: Login;
    options: Options;
    notifications: Notifications;

    constructor(url: string, id_cliente: string, id_app: string) {
        this.options = {url: url, id_cliente: id_cliente, id_app: id_app};
        this.models = new Models(this.options);
        this.marketplace = new Marketplace(this.options);
        this.predictions = new Predictions(this.options);
        this.templates = new Templates(this.options);
        this.trainings = new Trainings(this.options);
        this.indicators = new Indicators(this.options);
        this.campaigns = new Campaigns(this.options);
        this.login = new Login(this.options);
        this.notifications = new Notifications(this.options);

    }

    clear_session() {
        localStorage.clear();
        delete  this.options.id_sesion;
    }
}


export const CXApi = new Api(process.env.API_ENDPOINT || "http://localhost:3000/", process.env.ID_CLIENTE || "vidasecurity", "artucx");

//check if credentials are stored in localStorage
if (!window.localStorage) {
} else {
    // localStorage supported
    let id_session = localStorage.getItem("id_session");
    if (id_session) {
        CXApi.options.id_sesion = id_session;
    }

    let id_usuario = localStorage.getItem("id_usuario");
    if (id_usuario) {
        CXApi.options.id_usuario = id_usuario;
    }
}