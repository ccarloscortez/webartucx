require('setimmediate');
require('moment/locale/es.js');

import * as React from "react";
import * as ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import { HistoryAdapter } from 'mobx-state-router';
import { createBrowserHistory } from 'history';
import { rootStore } from './backend/stores';
import {Shell} from "./shell";
import * as moment from 'moment';
import 'react-times/css/material/default.css';
import 'react-day-picker/lib/style.css';
import 'react-toastify/dist/ReactToastify.css';

// or you can use classic theme
moment.locale('es');


// Observe history changes
const historyAdapter = new HistoryAdapter(rootStore.routerStore, createBrowserHistory());
historyAdapter.observeRouterStateChanges();

class App extends React.Component {
    render() {
        return (
            <Provider rootStore={rootStore}>
                <Shell/>
            </Provider>
        );
    }
}

ReactDOM.render(<App/>, document.getElementById('root'));

