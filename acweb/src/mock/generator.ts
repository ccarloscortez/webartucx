import mocker from 'mocker-data-generator'


const Usuario = {
    id_usuario: {
        faker: 'random.uuid'
    },
    first_name: {
        static: "cristobal"
    },
    profile_pic: {
        static: "https://s3.amazonaws.com/amjr/imgavatar.png"
    },

};

const Cliente = {
    id_cliente: {
        faker: 'random.uuid'
    }
};

const CxMailTemplate = {
    id_mailtemplate: {
        faker: 'random.uuid',
    },
    name: {
        faker: 'company.companyName',
    },
    desc: {
        faker: 'lorem.paragraph',
    },
    title: {
        faker: 'company.companyName',
    },
    body: {
        faker: 'lorem.paragraph',
    },
    creation_date: {
        faker: 'date.past',
    },
    id_cliente: {
        hasOne: 'cliente',
        get: 'id_cliente'
    },
    id_usuario: {
        hasOne: 'usuario',
        get: 'id_usuario'
    },
    campaign_count: {
        chance: 'integer({"min": 0, "max": 5})',
    },
    images: {
        static: {
            img1: {static: "http://s3.envato.com/files/164595342/01_preview.__large_preview.jpg"},
            img2: {static: "https://static.intercomcdn.com/docs/docs.intercom.io/images/1-dccbe2a391c3d87781c1d04e93da88c4b34af11d500a878221c354a56c1979b1.png"}
        }
    }
};

const ConfusionMatrix = {
    fn: {
        chance: 'integer({"min": 1, "max": 10000})',
    },
    fp: {
        chance: 'integer({"min": 1, "max": 10000})',
    },
    tn: {
        chance: 'integer({"min": 1, "max": 10000})',
    },
    tp: {
        chance: 'integer({"min": 1, "max": 10000})',
    },
};

const CxMlMetric = {
    id_metric: {
        faker: 'random.uuid',
    },
    type: {
        static: 'training',
    },
    accuracy: {
        chance: 'floating({"min": 0, "max": 1})',
    },
    fscore: {
        chance: 'floating({"min": 0, "max": 1})',
    },
    recall: {
        chance: 'floating({"min": 0, "max": 1})',
    },
    precision: {
        chance: 'floating({"min": 0, "max": 1})',
    },
    roc: {
        chance: 'floating({"min": 0, "max": 1})',
    },
    confusion_matrix: {
        hasOne: 'confusion_matrix',
    },
    roc_curve: {
        fpr: {static: [0.0, 0.6140167364016736, 1.0]},
        tpr: {static: [0.0, 0.9776725304465493, 1.0]},
        aoc: {static: 0.6818278970224378}

    },
    importances: {
        TIPOOPERACION: {static: 0.05743269808737461},
        FECFINVIG: {static: 0.20440281030615345},
        FECHA_INICIO_INTERMEDIARIO: {static: 0.18270534766317806},
        PROD: {static: 0.09147445267471604},
        NOMBREPRODUCTO: {static: 0.09013863883592325},
        PERIDICIDADPAGO: {static: 0.10080792131037347},
        OPERACION: {static: 0.03456907960056085},
        ACTIVIDADLABORAL: {static: 0.11111107349694681},
        FECHAMOV: {static: 0.06593969684230107},
        MEDIOPAGO: {static: 0.06141828118247239}
    }

};


const CxMlStatistic = {
    id_statistic: {
        faker: 'random.uuid',
    },
    report_name: {
        faker: 'company.companyName',
    },
    report_desc: {
        faker: 'lorem.paragraph',
    },
    total_entries: {
        chance: 'integer({"min": 1, "max": 1000})',
    },
    positive_level_name: {
        faker: 'company.companyName',
    },
    negative_level_name: {
        faker: 'company.companyName',
    },
    positive_level: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    negative_level: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    positive_level_csv: {
        faker: 'system.filePath',
    },
    negative_level_csv: {
        faker: 'system.filePath',
    },
    id_cliente: {
        hasOne: 'cliente',
        get: 'id_cliente'
    },
    id_usuario: {
        hasOne: 'usuario',
        get: 'id_usuario'
    },
};


const CxModel = {
    id_model: {
        faker: 'random.uuid',
    },
    category: {
        chance: 'pickone(["default_rate", "churn", "upselling", "cross_selling", "custom_model"])',
        eval: true
    },
    version: {
        chance: 'integer({"min": 1, "max": 10})',
    },
    industry: {
        chance: 'pickone(["insurers", "banks", "ecomerce"])',
        eval: true
    },
    name: {
        faker: 'company.companyName',
    },
    creation_time: {
        faker: 'date.past',
    },
    desc: {
        title: {
            faker: 'lorem.paragraph',
        },
        type: {static: "csv"},
        Column1: {static: "varchar"},
        Column2: {static: "varchar"},
        Column3: {static: "int"},
        Column4: {static: "varchar"},
        Column5: {static: "int"},
        Column6: {static: "int"},
        Column7: {static: "int"},
        Column8: {static: "int"}
    },
    id_added: {
        faker: 'random.boolean',
    },
};


const CxProject = {
    id_project: {
        faker: 'random.uuid',
    },
    name: {
        faker: 'company.companyName',
    },
    desc: {
        faker: 'lorem.paragraph',
    },
    creation_date: {
        faker: 'date.past',
    },
    from_address: {static: 'vida@security.cl'},

    id_cliente: {
        hasOne: 'cliente',
        get: 'id_cliente'
    },
    id_usuario: {
        hasOne: 'usuario',
        get: 'id_usuario'
    },
    segment_count: {
        chance: 'integer({"min": 0, "max": 5})',
    },
};

const CxClientModel = {
    model: {
        hasOne: 'cx_model',
    },
    addition_user: {
        hasOne: 'usuario',
        get: 'id_usuario'
    },
    addition_date: {
        faker: 'date.past',
    },
    id_cliente: {
        hasOne: 'cliente',
        get: 'id_cliente'
    },
};

const CxMarketplace = {
    id_marketplace: {
        faker: 'random.uuid',
    },
    name: {
        faker: 'company.companyName',
    },
    id_app: {
        faker: 'company.companyName',
    },
    id_model: {
        hasOne: 'cx_model',
        get: 'id_model'
    },
    creation_time: {
        faker: 'date.past',
    },
};


const CxSegment = {
    id_segment: {
        faker: 'random.uuid',
    },
    id_project: {
        hasOne: 'cx_project',
        get: 'id_project'
    },
    name: {
        faker: 'company.companyName',
    },
    desc: {
        faker: 'lorem.paragraph',
    },
    total_entries: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    campaign_input_url: {
        faker: 'system.filePath',
    },
    creation_time: {
        faker: 'date.past',
    },
    channel_type: {
        static: 'email',
    },
    id_cliente: {
        hasOne: 'cliente',
        get: 'id_cliente'
    },
    id_usuario: {
        hasOne: 'usuario',
        get: 'id_usuario'
    },
    campaign_count: {
        chance: 'integer({"min": 0, "max": 5})',
    },
};


const CxTraining = {
    name: {
        faker: 'company.companyName',
    },
    id_training: {
        faker: 'random.uuid',
    },
    id_model: {
        hasOne: 'cx_model',
        get: 'id_model',
        eval: true

    },
    state: {
        chance: "pickone(['ready', 'processing'])",
        eval: true
    },
    id_metric: {
        hasOne: 'cx_ml_metric',
        get: 'id_metric'
    },
    id_cliente: {
        hasOne: 'cliente',
        get: 'id_cliente'
    },
    id_usuario: {
        hasOne: 'usuario',
        get: 'id_usuario'
    },
    creation_time: {
        faker: 'date.past',
    },
    is_free: {
        chance: "pickone(['true', 'false'])",
        eval: true
    },
    type: {
        chance: "pickone(['supervised', 'unsupervised'])",
        eval: true
    },
    subtype: {
        chance: "pickone(['clustering', 'regression', 'classification'])",
        eval: true
    },
    modeling_data: {
        static: {},
    }
};


const PostMarkStats = {
    BounceRate: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    Bounced: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    Opens: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    SMTPApiErrors: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    Sent: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    SpamComplaints: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    SpamComplaintsRate: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    TotalClicks: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    TotalTrackedLinksSent: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    Tracked: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    UniqueLinksClicked: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    UniqueOpens: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    WithClientRecorded: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    WithLinkTracking: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    WithOpenTracking: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    WithPlatformRecorded: {
        chance: 'integer({"min": 1, "max": 100})',
    },
    WithReadTimeRecorded: {
        chance: 'integer({"min": 1, "max": 100})',
    },
};

const CxCampaign = {
    id_campaign: {
        faker: 'random.uuid',
    },
    id_segment: {
        hasOne: 'cx_segment',
        get: 'id_segment'
    },
    id_mailtemplate: {
        hasOne: 'cx_mail_template',
        get: 'id_mailtemplate'
    },
    creation_time: {
        faker: 'date.past',
    },
    name: {
        faker: 'company.companyName',
    },
    desc: {
        faker: 'lorem.paragraph',
    },
    id_cliente: {
        hasOne: 'cliente',
        get: 'id_cliente'
    },
    id_usuario: {
        hasOne: 'usuario',
        get: 'id_usuario'
    },
    stats_data: {
        hasOne: 'post_mark_stats',
    },
    start_time: {
        faker: 'date.past',
    },
    end_time: {
        faker: 'date.future',
    },
    timezone: {
        static: 'ACDT',
    },
    state: {
        faker: 'random.boolean',
    },
    frequency: {
        static: "daily",
    },
    schedule_expression: {
        static: "cron(20 13 * * 3,4,5 *)",
    },
    replacement_data_url: {
        faker: 'system.filePath',
    },
    email_images: {
        static: {
            img1: {static: "http://s3.envato.com/files/164595342/01_preview.__large_preview.jpg"},
            img2: {static: "https://static.intercomcdn.com/docs/docs.intercom.io/images/1-dccbe2a391c3d87781c1d04e93da88c4b34af11d500a878221c354a56c1979b1.png"}
        }
    }
};

const CxPrediction = {
    id_prediction: {
        faker: 'random.uuid',
    },
    id_training: {
        hasOne: 'cx_training',
        get: 'id_training'
    },
    name: {
        faker: 'company.companyName',
    },
    creation_time: {
        faker: 'date.past',
    },
    on_dashboard: {
        faker: 'random.boolean',
    },
    id_job: {
        faker: 'random.uuid',
    },
    job_type: {
        static: 'batch',
    },
    job_start_time: {
        faker: 'date.past',
    },
    job_state: {
        chance: "pickone(['in progress', 'finished'])",
        eval: true,
    },
    input_csv_url: {
        faker: 'system.filePath',
    },
    output_csv_url: {
        faker: 'system.filePath',
    },
    job_finish_time: {
        faker: 'date.future',

    },
    id_cliente: {
        hasOne: 'cliente',
        get: 'id_cliente'
    },
    id_usuario: {
        hasOne: 'usuario',
        get: 'id_usuario'
    },
    id_statistic: {
        hasOne: 'cx_ml_statistic',
        get: 'id_statistic'
    },
    cx_ml_statistic: {
        hasOne: 'cx_ml_statistic',
    },

};

const PostmarkEvent = {
    click_location: {
        static: "HTML"
    },
    geo_city: {
        static: "Santiago"
    },
    geo_coords: {
        static: "-33.45,-70.6667"
    },
    geo_country: {
        static: "Chile"
    },
    geo_region: {
        static: "Santiago Metropolitan"
    },
    id_cliente: {
        hasOne: 'cliente',
        get: 'id_cliente'
    },
    message_id: {
        static: "fa1647d4-1894-4c51-a96f-babe6d15bef4"
    },
    original_link: {
        static: "http://artucx-eng.s3-website-us-east-1.amazonaws.com"
    },
    platform: {
        static: "Mobile"
    },
    query: {
        static: null
    },
    query_class: {
        static: null
    },
    received_at: {
        faker: 'date.past',
    },
    recipient: {
        faker: 'internet.email',
    },
    tag: {
        hasOne: 'cx_campaign',
        get: 'id_campaign'
    },
};

const CxNotificationChange = {
    id_notification_change: {
        faker: 'random.uuid',
    },
    action_on_entity: {
        chance: "pickone(['created', 'deleted'])",
        eval: true,
    },
    actor_id_usuario: {
        static: 'cristobal',
    },
    created_at: {
        faker: 'date.past',
    },
};

const CxReadNotification = {
    id_read_notification: {
        faker: 'random.uuid',
    },
    id_usuario: {
        hasOne: 'usuario',
        get: 'id_usuario'
    },
    id_notification_change: {
        hasOne: 'notification_change',
        get: 'id_notification_change'
    },
    read_at: {
        faker: 'date.past',
    },
};

// Using traditional callback Style
export let db: any = {};
mocker()
    .schema('usuario', Usuario, 1)
    .schema('cliente', Cliente, 1)
    .schema('cx_mail_template', CxMailTemplate, 50)
    .schema('confusion_matrix', ConfusionMatrix, 50)
    .schema('cx_ml_metric', CxMlMetric, 50)
    .schema('cx_ml_statistic', CxMlStatistic, 50)
    .schema('cx_model', CxModel, 20)
    .schema('cx_project', CxProject, 50)
    .schema('cx_client_model', CxClientModel, 20)
    .schema('cx_market_place', CxMarketplace, 50)
    .schema('cx_segment', CxSegment, 150)
    .schema('cx_training', CxTraining, 20)
    .schema('post_mark_stats', PostMarkStats, 50)
    .schema('cx_campaign', CxCampaign, 50)
    .schema('cx_prediction', CxPrediction, 200)
    .schema('postmark_event', PostmarkEvent, 600)
    .schema('notification_change', CxNotificationChange, 50)
    .schema('read_notification', CxReadNotification, 30)
    .build(function (error, data) {
        db = data;
        let index = 0;
        for (const pe  of db.postmark_event) {
            index += 1;
            const myArray = [{id_delivery: 2}, {id_open: 11}, {id_click: 60}, {id_bounce: 60}];
            const rand = myArray[Math.floor(Math.random() * myArray.length)];
            if (rand.id_delivery) pe.id_delivery = index;
            if (rand.id_open) pe.id_open = index;
            if (rand.id_click) pe.id_click = index;
            if (rand.id_bounce) pe.id_bounce = index;
        }

        let rtypes = [
            {key: "cx_mail_template", name: "CxMailTemplate"},
            {key: "cx_project", name: "CxProject"},
            {key: "cx_client_model", name: "CxClientModel"},
            {key: "cx_segment", name: "CxSegment"},
            {key: "cx_training", name: "CxTraining"},
            {key: "cx_campaign", name: "CxCampaign"},
            {key: "cx_prediction", name: "CxPrediction"}
        ];

        for (const n_change  of db.notification_change) {
            const picked_rtype = rtypes[Math.floor(Math.random() * rtypes.length)];
            n_change.entity_type = picked_rtype.name;
            const target = db[picked_rtype.key][Math.floor(Math.random() * db[picked_rtype.key].length)];

            switch (picked_rtype.name) {
                case "CxMailtemplate":
                    n_change.entity_id = target.id_mailtemplate;
                    n_change.entity_name = target.name;
                    continue;
                case  "CxProject":
                    n_change.entity_id = target.id_project;
                    n_change.entity_name = target.name;
                    continue;
                case "CxClientModel":
                    n_change.entity_id = `${target.id_cliente}_${target.id_model}`;
                    n_change.entity_name = target.model.name;
                    continue;
                case  "CxSegment":
                    n_change.entity_id = target.id_segment;
                    n_change.entity_name = target.name;
                    continue;
                case  "CxTraining":
                    n_change.entity_id = target.id_training;
                    n_change.entity_name = target.name;
                    continue;
                case "CxCampaign":
                    n_change.entity_id = target.id_campaign;
                    n_change.entity_name = target.name;
                    continue;
                case "CxPrediction":
                    n_change.entity_id = target.id_prediction;
                    n_change.entity_name = target.name;
            }
        }
    });
