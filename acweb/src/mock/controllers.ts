/* app/controllers/welcome.controllers.ts */

// Import only what we need from express
import {Request, Response, Router} from 'express';
import {db} from "./generator";
import * as moment from 'moment';

const session = "24cdb38d9ee3affc11ef9baf46e28ee6";
const username = "cristobal";
const password = "123";

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

// Assign router to the express.Router() instance
const router: Router = Router();

function error(api_id: string) {
    return {status: 500, error_id: 'ced54805-4fb6-427f-a480-2aea44ab6925', api_id: api_id}
}

const login = {
    login: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            if (body.DATA_IN.username == username && body.DATA_IN.password == password) {
                return {
                    elements: null,
                    first_name: db["usuario"][0].first_name,
                    id_sesion: session,
                    id_tx: "14145",
                    profile_pic: db["usuario"][0].profile_pic,
                    resp_code: "1",
                    resp_message: "login ok"
                }
            } else {
                return {
                    id_tx: "14147",
                    resp_code: "2",
                    resp_message: "User not found",

                }
            }
        }
        return error('login')

    }
};

const marketplace = {
    cx_list_models: function (force_error: any, id_app: string) {
        if (!force_error) {
            const models = db["cx_model"];
            let response = [];
            for (const m of models) {
                response.push({
                        id_app: id_app,
                        id_model: m.id_model,
                        name: m.name,
                        desc: m.desc,
                        category: m.category,
                        industry: m.industry,
                        version: m.version,
                        is_added: m.id_added,
                        creation_time: moment(m.creation_time).format('YYYY-M-D h:mm:ss')
                    }
                )
            }
            return {status: 200, response: response}
        }
        return error('cx_list_models')
    },
    cx_add_model: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            let ccm = {
                id_model: body.DATA_IN.id_model,
                id_cliente: body.id_cliente,
                addition_date: new Date(),
                addition_user: body.id_usuario,
            };


            let response = [{
                id_model: ccm.id_model,
                id_cliente: ccm.id_cliente,
                addition_date: moment(ccm.addition_date).format('YYYY-M-D h:mm:ss'),
                addition_user: ccm.addition_user,
            }];

            return {status: 200, response: response}
        }
        return error('cx_add_model')
    }
};

const trainings = {
    cx_list_trainings: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            const trainings = db["cx_training"];
            let response = [];
            for (const training of trainings) {
                response.push({
                        creation_time: moment(training.creation_time).format('YYYY-M-D h:mm:ss'),
                        id_training: training.id_training,
                        name: training.name,
                        id_model: training.id_model,
                        state: training.state,
                        type: training.type,
                        subtype: training.subtype,
                        modeling_data: training.modeling_data,
                        is_free: training.is_free,
                        id_metric: training.id_metric,
                        id_app: id_app,
                    }
                )
            }
            return {status: 200, response: response}
        }
        return error('cx_list_trainings')
    },

    cx_training_details: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            let cx_ml_metric: any = {};
            for (let m of  db["cx_ml_metric"]) {
                if (m.id_metric == body.DATA_IN.id_metric) {
                    cx_ml_metric = m;
                    break
                }
            }
            let response = [];
            response.push({
                    id_metric: cx_ml_metric.id_metric,
                    type: cx_ml_metric.type,
                    accuracy: cx_ml_metric.accuracy + "",
                    fscore: cx_ml_metric.fscore + "",
                    recall: cx_ml_metric.recall + "",
                    precision: cx_ml_metric.precision + "",
                    roc: cx_ml_metric.roc + "",
                    confusion_matrix: cx_ml_metric.confusion_matrix,
                    roc_curve: cx_ml_metric.roc_curve,
                    importances: cx_ml_metric.importances,
                }
            );

            return {status: 200, response: response}
        }
        return error('cx_training_details')
    },
};

const models = {
    cx_list_mymodels: function (force_error: any) {
        if (!force_error) {
            const client_models = db["cx_client_model"];

            let response = [];
            for (const cm of client_models) {
                response.push({
                        id_model: cm.model.id_model,
                        name: cm.model.name,
                        desc: cm.model.desc,
                        category: cm.model.category,
                        industry: cm.model.industry,
                        version: "" + cm.model.version,
                        addition_date: moment(cm.addition_date).format('YYYY-M-D h:mm:ss'),
                        creation_time: moment(cm.model.creation_time).format('YYYY-M-D h:mm:ss')

                    }
                )
            }
            return {status: 200, response: response}

        }
        return error('cx_list_mymodels')

    }
};


var predictions = {
    cx_list_predictions: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            const predictions = db["cx_prediction"];
            let response = [];
            for (const prediction of predictions) {
                if (prediction.id_training == body.DATA_IN.id_training) {
                    response.push({
                            id_prediction: prediction.id_prediction,
                            id_training: prediction.id_training,
                            name: prediction.name,
                            creation_time: moment(prediction.creation_time).format('YYYY-M-D h:mm:ss'),
                            on_dashboard: prediction.on_dashboard,
                            id_job: prediction.id_job,
                            job_type: prediction.job_type,
                            job_start_time: moment(prediction.job_start_time).format('YYYY-M-D h:mm:ss'),
                            job_state: prediction.job_state,
                            input_csv_url: prediction.input_csv_url,
                            output_csv_url: prediction.output_csv_url,
                            job_finish_time: moment(prediction.job_finish_time).format('YYYY-M-D h:mm:ss'),
                            id_cliente: prediction.id_cliente,
                            id_usuario: prediction.id_usuario,
                            id_statistic: prediction.id_statistic,
                        }
                    )
                }
            }
            return {status: 200, response: response}
        }
        return error('cx_list_predictions')
    },

    cx_prediction_state: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            const predictions = db["cx_prediction"];
            let response = [];
            for (const prediction of predictions) {
                prediction.on_dashboard = !prediction.on_dashboard;
                if (prediction.id_prediction == body.DATA_IN.id_prediction) {
                    response.push({
                            fecha: moment(new Date()).format('YYYY-M-D h:mm:ss'),
                            id_prediction: prediction.id_prediction,
                            on_dashboard: prediction.on_dashboard,
                            id_cliente: prediction.id_cliente,
                            id_usuario: prediction.id_usuario
                        }
                    )
                }
            }
            return {status: 200, response: response}
        }
        return error('cx_prediction_state')
    },

    cx_prediction_results: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            const predictions = db["cx_prediction"];
            let response = [];
            for (const prediction of predictions) {
                if (prediction.id_prediction == body.DATA_IN.id_prediction) {
                    response.push({
                            id_prediction: prediction.id_prediction,
                            id_statistic: prediction.id_statistic,
                            report_name: prediction.cx_ml_statistic.report_name,
                            report_desc: prediction.cx_ml_statistic.report_desc,
                            total_entries: prediction.cx_ml_statistic.total_entries,
                            positive_level_name: prediction.cx_ml_statistic.positive_level_name,
                            negative_level_name: prediction.cx_ml_statistic.negative_level_name,
                            positive_level: prediction.cx_ml_statistic.positive_level,
                            negative_level: prediction.cx_ml_statistic.negative_level,
                            positive_level_csv: prediction.cx_ml_statistic.positive_level_csv,
                            negative_level_csv: prediction.cx_ml_statistic.negative_level_csv,
                            id_cliente: prediction.cx_ml_statistic.id_cliente,
                            id_usuario: prediction.cx_ml_statistic.id_usuario,
                        }
                    )
                }
            }
            return {status: 200, response: response}
        }
        return error('cx_prediction_results')
    },
    cx_create_prediction: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            let prediction = {
                id_prediction: uuidv4(),
                id_training: body.DATA_IN.id_training,
                name: body.DATA_IN.name,
                creation_time: new Date(),
                on_dashboard: false,
                id_job: uuidv4(),
                job_type: "batch",
                job_start_time: new Date(),
                job_state: "in progress",
                input_csv_url: body.DATA_IN.input_csv_url,
                output_csv_url: null,
                job_finish_time: null,
                id_cliente: body.id_cliente,
                id_usuario: body.id_usuario,
                id_statistic: null
            };

            db["cx_prediction"].push(prediction);
            let response = [];
            response.push({
                id_prediction: prediction.id_prediction,
                id_training: prediction.id_training,
                name: prediction.name,
                creation_time: moment(prediction.creation_time).format('YYYY-M-D h:mm:ss'),
                on_dashboard: prediction.on_dashboard,
                id_job: prediction.id_job,
                job_type: prediction.job_type,
                job_start_time: moment(prediction.job_start_time).format('YYYY-M-D h:mm:ss'),
                job_state: prediction.job_state,
                input_csv_url: prediction.input_csv_url,
                output_csv_url: prediction.output_csv_url,
                job_finish_time: null,
                id_cliente: prediction.id_cliente,
                id_usuario: prediction.id_usuario,
                id_statistic: null
            });

            return {status: 200, response: response}
        }
        return error('cx_create_prediction')
    },
    cx_uploads3_predictinput: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {

            return {
                status: 200,
                response: [{"url": "https://s3.amazonaws.com/artucxstorage/vidasecurity/predictions/input/test_01CJ4M5R4MJF5SHZ4YR3RATHGW.csv"}]
            }
        }
        return error('cx_uploads3_predictinput')
    }
};

var templates = {
    cx_create_template: function (force_error: any, id_app: string, body: any) {

        if (!force_error) {
            let template = {
                creation_date: new Date(),
                id_cliente: body.id_cliente,
                id_usuario: body.id_usuario,
                id_mailtemplate: uuidv4(),
                name: body.DATA_IN.name,
                desc: body.DATA_IN.desc,
                title: body.DATA_IN.title,
                body: body.DATA_IN.body,
                campaign_count: 0,
            };

            db["cx_mail_template"].push(template);

            let response = [];
            response.push({
                    creation_date: moment(template.creation_date).format('YYYY-M-D h:mm:ss'),
                    id_cliente: template.id_cliente,
                    id_usuario: template.id_usuario,
                    id_mailtemplate: template.id_mailtemplate,
                    name: template.name,
                    desc: template.desc,
                    title: template.title,
                    body: template.body,
                    campaign_count: template.campaign_count,

                }
            );

            return {status: 200, response: response}
        }
        return error('cx_create_template')

    },

    cx_edit_template: function (force_error: any, id_app: string, body: any) {

        if (!force_error) {
            let response = [];

            for (let template of db["cx_mail_template"]) {
                if (template.id_mailtemplate != body.DATA_IN.id_mailtemplate) {
                    continue
                }
                template.name = body.DATA_IN.name || template.name;
                template.desc = body.DATA_IN.desc || template.desc;
                template.title = body.DATA_IN.title || template.title;
                template.body = body.DATA_IN.body || template.body;

                response.push({
                        creation_date: moment(template.creation_date).format('YYYY-M-D h:mm:ss'),
                        id_cliente: template.id_cliente,
                        id_usuario: template.id_usuario,
                        id_mailtemplate: template.id_mailtemplate,
                        name: template.name,
                        desc: template.desc,
                        title: template.title,
                        body: template.body,
                        campaign_count: template.campaign_count,

                    }
                );
            }


            return {status: 200, response: response}
        }
        return error('cx_edit_template')
    },


    cx_delete_template: function (force_error: any, id_app: string, body: any) {

        if (!force_error) {
            return {status: 200, response: [{id_mailtemplate: body.DATA_IN.id_mailtemplate}]}
        }
        return error('cx_delete_template')

    },

    cx_list_templates: function (force_error: any, id_app: string, body: any) {

        if (!force_error) {

            let response = [];
            for (const template of db["cx_mail_template"]) {
                response.push({
                        creation_date: moment(template.creation_date).format('YYYY-M-D h:mm:ss'),
                        id_cliente: template.id_cliente,
                        id_usuario: template.id_usuario,
                        id_mailtemplate: template.id_mailtemplate,
                        name: template.name,
                        desc: template.desc,
                        title: template.title,
                        body: template.body,
                        campaign_count: template.campaign_count,

                    }
                );
            }

            return {status: 200, response: response}
        }
        return error('cx_list_templates')

    },

};

var campaigns = {
    cx_campaign_opens: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            let response = [];
            for (const pe of db["postmark_event"]) {
                if (pe.tag != body.DATA_IN.tag) {
                    continue
                }
                if (!pe.id_open) {
                    continue
                }
                response.push({
                    id_open: pe.id_open,
                    click_location: pe.click_location,
                    geo_city: pe.geo_city,
                    geo_coords: pe.geo_coords,
                    geo_country: pe.geo_country,
                    geo_region: pe.geo_region,
                    id_cliente: pe.id_cliente,
                    message_id: pe.message_id,
                    original_link: pe.original_link,
                    platform: pe.platform,
                    query: pe.query,
                    query_class: pe.query_class,
                    received_at: moment(pe.received_at).format('YYYY-MM-DDThh:mm:ss'),
                    recipient: pe.recipient,
                    tag: pe.tag,
                });
            }

            return {status: 200, response: response}
        }
        return error('cx_campaign_opens')
    },
    cx_campaign_clicks: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            let response = [];
            for (const pe of db["postmark_event"]) {
                if (pe.tag != body.DATA_IN.tag) {
                    continue
                }
                if (!pe.id_click) {
                    continue
                }
                response.push({
                    id_click: pe.id_click,
                    click_location: pe.click_location,
                    geo_city: pe.geo_city,
                    geo_coords: pe.geo_coords,
                    geo_country: pe.geo_country,
                    geo_region: pe.geo_region,
                    id_cliente: pe.id_cliente,
                    message_id: pe.message_id,
                    original_link: pe.original_link,
                    platform: pe.platform,
                    query: pe.query,
                    query_class: pe.query_class,
                    received_at: moment(pe.received_at).format('YYYY-MM-DDThh:mm:ss'),
                    recipient: pe.recipient,
                    tag: pe.tag,
                });
            }

            return {status: 200, response: response}
        }
        return error('cx_campaign_clicks')
    },

    cx_campaign_delivery: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            let response = [];
            for (const pe of db["postmark_event"]) {
                if (pe.tag != body.DATA_IN.tag) {
                    continue
                }
                if (!pe.id_delivery) {
                    continue
                }
                response.push({
                    id_delivery: pe.id_delivery,
                    click_location: pe.click_location,
                    geo_city: pe.geo_city,
                    geo_coords: pe.geo_coords,
                    geo_country: pe.geo_country,
                    geo_region: pe.geo_region,
                    id_cliente: pe.id_cliente,
                    message_id: pe.message_id,
                    original_link: pe.original_link,
                    platform: pe.platform,
                    query: pe.query,
                    query_class: pe.query_class,
                    received_at: moment(pe.received_at).format('YYYY-MM-DDThh:mm:ss'),
                    recipient: pe.recipient,
                    tag: pe.tag,
                });
            }

            return {status: 200, response: response}
        }
        return error('cx_campaign_delivery')
    },
    cx_campaign_bounce: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            let response = [];
            for (const pe of db["postmark_event"]) {
                if (pe.tag != body.DATA_IN.tag) {
                    continue
                }
                if (!pe.id_bounce) {
                    continue
                }
                response.push({
                    id_bounce: pe.id_bounce,
                    click_location: pe.click_location,
                    geo_city: pe.geo_city,
                    geo_coords: pe.geo_coords,
                    geo_country: pe.geo_country,
                    geo_region: pe.geo_region,
                    id_cliente: pe.id_cliente,
                    message_id: pe.message_id,
                    original_link: pe.original_link,
                    platform: pe.platform,
                    query: pe.query,
                    query_class: pe.query_class,
                    received_at: moment(pe.received_at).format('YYYY-MM-DDThh:mm:ss'),
                    recipient: pe.recipient,
                    tag: pe.tag,
                });
            }

            return {status: 200, response: response}
        }
        return error('cx_campaign_bounce')
    },
    cx_campaign_statistics: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            let response = [];
            for (let campaign of db["cx_campaign"]) {
                if (campaign.id_campaign != body.DATA_IN.id_campaign) {
                    continue
                }
                response.push({
                    id_cliente: campaign.id_cliente,
                    id_usuario: campaign.id_usuario,
                    id_campaign: campaign.id_campaign,
                    id_segment: campaign.id_segment,
                    campaign_name: campaign.name,
                    open_rate: 0,
                    delivered: 0,
                    state: campaign.state,
                    stats_data: campaign.stats_data
                });
            }

            return {status: 200, response: response}
        }
        return error('cx_campaign_statistics')
    },

    cx_uploadimages_campaign: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {

            return {
                status: 200,
                response: [{"url": "https://s3.amazonaws.com/artucxstorage/vidasecurity/predictions/input/test_01CJ4M5R4MJF5SHZ4YR3RATHGW.png"}]
            }
        }
        return error('cx_uploadimages_campaign')
    },

    cx_create_campaign: function (force_error: any, id_app: string, body: any) {

        if (!force_error) {
            let campaign = {
                id_campaign: uuidv4(),
                id_segment: body.DATA_IN.id_segment,
                creation_time: new Date(),
                name: body.DATA_IN.campaign_name,
                desc: body.DATA_IN.campaign_desc,
                email_images: body.DATA_IN.email_images,
                frequency: body.DATA_IN.frequency,
                start_time: body.DATA_IN.start_time,
                end_time: body.DATA_IN.end_time,
                schedule_expression: body.DATA_IN.schedule_expression,
                id_cliente: body.id_cliente,
                id_usuario: body.id_usuario,
                id_mailtemplate: body.DATA_IN.id_mailtemplate,
                replacement_data_url: body.DATA_IN.replacement_data_url,
                state: true,
            };

            db["cx_campaign"].push(campaign);

            let response = [];
            response.push({
                    id_campaign: campaign.id_campaign,
                    id_segment: campaign.id_segment,
                    creation_time: moment(campaign.creation_time).format('YYYY-M-D h:mm:ss'),
                    campaign_name: campaign.name,
                    campaign_desc: campaign.desc,
                    email_images: campaign.email_images,
                    frequency: campaign.frequency,
                    start_time: campaign.start_time,
                    end_time: campaign.end_time,
                    schedule_expression: campaign.schedule_expression,
                    id_cliente: campaign.id_cliente,
                    id_usuario: campaign.id_usuario,
                    id_mailtemplate: campaign.id_mailtemplate,
                    replacement_data_url: campaign.replacement_data_url,
                    state: campaign.state,
                }
            );

            return {status: 200, response: response}
        }
        return error('cx_create_campaign')

    },

    cx_list_campaigns: function (force_error: any, id_app: string, body: any) {

        if (!force_error) {

            let response = [];
            for (const campaign of db["cx_campaign"]) {
                response.push({
                        id_campaign: campaign.id_campaign,
                        id_segment: campaign.id_segment,
                        creation_time: moment(campaign.creation_time).format('YYYY-M-D h:mm:ss'),
                        campaign_name: campaign.name,
                        campaign_desc: campaign.desc,
                        email_images: campaign.email_images,
                        frequency: campaign.frequency,
                        start_time: campaign.start_time,
                        end_time: campaign.end_time,
                        schedule_expression: campaign.schedule_expression,
                        id_cliente: campaign.id_cliente,
                        id_usuario: campaign.id_usuario,
                        id_mailtemplate: campaign.id_mailtemplate,
                        replacement_data_url: campaign.replacement_data_url,
                        state: campaign.state,
                    }
                );
            }


            return {status: 200, response: response}
        }
        return error('cx_list_campaigns')

    },

    cx_campaign_state: function (force_error: any, id_app: string, body: any) {

        if (!force_error) {

            let response = [];
            for (let campaign of db["cx_campaign"]) {
                if (campaign.id_campaign != body.DATA_IN.id_campaign) {
                    continue
                }
                campaign.state = !campaign.state;
                response.push({
                        id_campaign: campaign.id_campaign,
                        state: campaign.state,
                    }
                );
            }
            return {status: 200, response: response}
        }
        return error('cx_campaign_state')
    },


    cx_delete_campaign: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            return {
                status: 200,
                response: [{
                    id_campaign: body.DATA_IN.id_campaign
                }]
            }
        }
        return error('cx_delete_campaign')
    },


    cx_create_project: function (force_error: any, id_app: string, body: any) {

        if (!force_error) {
            let project = {
                id_project: uuidv4(),
                from_address: 'vida@security.cl',
                name: body.DATA_IN.name,
                desc: body.DATA_IN.desc,
                creation_date: new Date(),
                id_cliente: body.id_cliente,
                id_usuario: body.id_usuario,
                segment_count: 0
            };

            db["cx_project"].push(project);

            let response = [];
            response.push({
                    id_project: project.id_project,
                    from_address: project.from_address,
                    name: project.name,
                    desc: project.desc,
                    creation_date: moment(project.creation_date).format('YYYY-M-D h:mm:ss'),
                    id_cliente: project.id_project,
                    id_usuario: project.id_usuario,
                    segment_count: project.segment_count
                }
            );

            return {status: 200, response: response}
        }
        return error('cx_create_project')

    },

    cx_list_projects: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {

            const projects = db["cx_project"];
            let response = [];

            for (const project of projects) {
                response.push({
                        id_project: project.id_project,
                        from_address: project.from_address,
                        name: project.name,
                        desc: project.desc,
                        creation_date: moment(project.creation_date).format('YYYY-M-D h:mm:ss'),
                        id_cliente: project.id_project,
                        id_usuario: project.id_usuario,
                        segment_count: project.segment_count
                    }
                );
            }

            return {status: 200, response: response}
        }
        return error('cx_list_projects')
    },

    cx_delete_project: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            return {
                status: 200,
                response: [{
                    id_project: body.DATA_IN.id_project
                }]
            }
        }
        return error('cx_delete_project')
    },

    cx_create_segment: function (force_error: any, id_app: string, body: any) {

        if (!force_error) {
            let segment = {
                id_segment: uuidv4(),
                id_project: body.DATA_IN.id_project,
                name: body.DATA_IN.name,
                desc: body.DATA_IN.desc,
                total_entries: 0,
                campaign_input_url: body.DATA_IN.campaign_input_url,
                creation_time: new Date(),
                channel_type: body.DATA_IN.channel_type,
                id_cliente: body.id_cliente,
                id_usuario: body.id_usuario,
                campaign_count: 0
            };

            db["cx_segment"].push(segment);

            let response = [];
            response.push({
                    id_segment: segment.id_segment,
                    id_project: segment.id_project,
                    name: segment.name,
                    desc: segment.desc,
                    total_entries: segment.total_entries + "",
                    campaign_input_url: segment.campaign_input_url,
                    creation_time: moment(segment.creation_time).format('YYYY-M-D h:mm:ss'),
                    channel_type: segment.channel_type,
                    id_cliente: segment.id_cliente,
                    id_usuario: segment.id_usuario,
                    id_app: id_app,
                    campaign_count: segment.campaign_count
                }
            );

            return {status: 200, response: response}
        }
        return error('cx_create_segment')
    },

    cx_list_segments: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {

            const segments = db["cx_segment"];
            let response = [];

            for (const segment of segments) {
                if (segment.id_project != body.DATA_IN.id_project) {
                    continue
                }
                response.push({
                        id_segment: segment.id_segment,
                        id_project: segment.id_project,
                        name: segment.name,
                        desc: segment.desc,
                        total_entries: segment.total_entries + "",
                        campaign_input_url: segment.campaign_input_url,
                        creation_time: moment(segment.creation_time).format('YYYY-M-D h:mm:ss'),
                        channel_type: segment.channel_type,
                        id_cliente: segment.id_cliente,
                        id_usuario: segment.id_usuario,
                        id_app: id_app,
                        campaign_count: segment.campaign_count
                    }
                );
            }

            return {status: 200, response: response}
        }
        return error('cx_list_segments')
    },
    cx_delete_segment: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            return {
                status: 200,
                response: [{
                    id_segment: body.DATA_IN.id_segment
                }]
            }
        }
        return error('cx_delete_project')
    },
    cx_upload_segment: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {

            return {
                status: 200,
                response: [{"url": "https://s3.amazonaws.com/artucxstorage/vidasecurity/predictions/input/test_01CJ4M5R4MJF5SHZ4YR3RATHGW.png"}]
            }
        }
        return error('cx_upload_segment')
    },

};


var notifications = {
    cx_create_notification_read: function (force_error: any, id_app: string, body: any) {

        if (!force_error) {
            let rn = {
                read_at: new Date(),
                id_read_notification: uuidv4(),
                id_notification_change: body.DATA_IN.id_notification_change,
                id_usuario: body.DATA_IN.id_usuario,
            };

            db["read_notification"].push(rn);

            let response = [];
            response.push({
                    id_read_notification: rn.id_read_notification,
                    id_usuario: rn.id_usuario,
                    id_notification_change: rn.id_notification_change,
                    read_at: moment(rn.read_at).format('YYYY-M-D h:mm:ss')
                }
            );
            return {status: 200, response: response}
        }
        return error('cx_create_notification_read')
    },

    cx_list_notification_reads: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            let response = [];

            for (const rn of db["read_notification"]) {

                response.push({
                        id_read_notification: rn.id_read_notification,
                        id_usuario: rn.id_usuario,
                        id_notification_change: rn.id_notification_change,
                        read_at: moment(rn.read_at).format('YYYY-M-D h:mm:ss')
                    }
                );
            }
            return {status: 200, response: response}
        }
        return error('cx_list_notification_reads')
    },


    cx_list_notifications: function (force_error: any, id_app: string, body: any) {

        if (!force_error) {
            let response = [];

            for (const notification of db["notification_change"]) {
                response.push({
                        id_notification_change: notification.id_notification_change,
                        entity_type: notification.entity_type,
                        entity_id: notification.entity_id,
                        entity_name: notification.entity_name,
                        action_on_entity: notification.action_on_entity,
                        actor_id_usuario: notification.actor_id_usuario,
                        actor_id_cliente: notification.actor_id_cliente,
                        created_at: moment(notification.created_at).format('YYYY-M-D h:mm:ss'),
                    }
                );
            }
            return {status: 200, response: response}
        }
        return error('cx_list_notifications')

    },

};

var indicators = {
    cx_list_active_indicators: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            if (!force_error) {
                const predictions = db["cx_prediction"];
                let response = [];
                for (const prediction of predictions) {
                    if (prediction.on_dashboard == true) {
                        response.push({
                                id_prediction: prediction.id_prediction,
                                id_statistic: prediction.id_statistic,
                                report_name: prediction.cx_ml_statistic.report_name,
                                report_desc: prediction.cx_ml_statistic.report_desc,
                                total_entries: prediction.cx_ml_statistic.total_entries,
                                positive_level_name: prediction.cx_ml_statistic.positive_level_name,
                                negative_level_name: prediction.cx_ml_statistic.negative_level_name,
                                positive_level: prediction.cx_ml_statistic.positive_level,
                                negative_level: prediction.cx_ml_statistic.negative_level,
                                positive_level_csv: prediction.cx_ml_statistic.positive_level_csv,
                                negative_level_csv: prediction.cx_ml_statistic.negative_level_csv,
                                id_cliente: prediction.cx_ml_statistic.id_cliente,
                                id_usuario: prediction.cx_ml_statistic.id_usuario,
                            }
                        )
                    }
                }
                return {status: 200, response: response}
            }
            return error('cx_prediction_results')
        }
        return error('cx_list_predictions')
    },
    cx_create_smart_campaign: function (force_error: any, id_app: string, body: any) {
        if (!force_error) {
            return {
                status: 200, response: [{
                    segment: db["cx_segment"][0],
                    project: db["cx_project"][0]
                }]
            }
        }

        return error('cx_list_predictions')
    }

};


// The / here corresponds to the route that the WelcomeController
// is mounted on in the controllers.ts file.
// In this case it's /welcome
router.post('/', (req: Request, res: Response) => {
    res.setHeader('Content-Type', 'application/json');

    // Reply with a hello world when no name param is provided
    switch (req.body.id_api) {
        case "login":
            res.send(JSON.stringify(login.login(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_list_models":
            res.send(JSON.stringify(marketplace.cx_list_models(req.params.force_error, req.body.id_app)));
            return;
        case "cx_add_model":
            res.send(JSON.stringify(marketplace.cx_add_model(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_list_mymodels":
            res.send(JSON.stringify(models.cx_list_mymodels(req.params.force_error)));
            return;
        case "cx_create_campaign":
            res.send(JSON.stringify(campaigns.cx_create_campaign(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_send_mailcampaign":
            res.send(JSON.stringify(campaigns.cx_create_campaign(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_list_campaigns":
            res.send(JSON.stringify(campaigns.cx_list_campaigns(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_delete_campaign":
            res.send(JSON.stringify(campaigns.cx_delete_campaign(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_campaign_statistics":
            res.send(JSON.stringify(campaigns.cx_campaign_statistics(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_uploadimages_campaign":
            res.send(JSON.stringify(campaigns.cx_uploadimages_campaign(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_campaign_state":
            res.send(JSON.stringify(campaigns.cx_campaign_state(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_create_project":
            res.send(JSON.stringify(campaigns.cx_create_project(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_list_projects":
            res.send(JSON.stringify(campaigns.cx_list_projects(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_create_segment":
            res.send(JSON.stringify(campaigns.cx_create_segment(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_list_segments":
            res.send(JSON.stringify(campaigns.cx_list_segments(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_delete_project":
            res.send(JSON.stringify(campaigns.cx_delete_project(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_delete_segment":
            res.send(JSON.stringify(campaigns.cx_delete_segment(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_upload_segment":
            res.send(JSON.stringify(campaigns.cx_upload_segment(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_campaign_opens":
            res.send(JSON.stringify(campaigns.cx_campaign_opens(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_campaign_clicks":
            res.send(JSON.stringify(campaigns.cx_campaign_clicks(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_campaign_delivery":
            res.send(JSON.stringify(campaigns.cx_campaign_delivery(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_campaign_bounce":
            res.send(JSON.stringify(campaigns.cx_campaign_bounce(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_list_trainings":
            res.send(JSON.stringify(trainings.cx_list_trainings(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_training_details":
            res.send(JSON.stringify(trainings.cx_training_details(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_list_predictions":
            res.send(JSON.stringify(predictions.cx_list_predictions(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_prediction_state":
            res.send(JSON.stringify(predictions.cx_prediction_state(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_prediction_results":
            res.send(JSON.stringify(predictions.cx_prediction_results(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_uploads3_predictinput":
            res.send(JSON.stringify(predictions.cx_uploads3_predictinput(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_create_template":
            res.send(JSON.stringify(templates.cx_create_template(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_edit_template":
            res.send(JSON.stringify(templates.cx_edit_template(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_list_templates":
            res.send(JSON.stringify(templates.cx_list_templates(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_delete_template":
            res.send(JSON.stringify(templates.cx_delete_template(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_create_notification_read":
            res.send(JSON.stringify(notifications.cx_create_notification_read(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_list_notification_reads":
            res.send(JSON.stringify(notifications.cx_list_notification_reads(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_list_notifications":
            res.send(JSON.stringify(notifications.cx_list_notifications(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_list_active_indicators":
            res.send(JSON.stringify(indicators.cx_list_active_indicators(req.params.force_error, req.body.id_app, req.body)));
            return;
        case "cx_create_smart_campaign":
            res.send(JSON.stringify(indicators.cx_create_smart_campaign(req.params.force_error, req.body.id_app, req.body)));
            return;
    }
    res.send(JSON.stringify({status: 404}));
});

// Export the express.Router() instance to be used by controllers.ts
export const ArtuCX: Router = router;