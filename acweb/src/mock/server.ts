// Import everything from express and assign it to the express variable
import * as express from 'express';
import * as  bodyParser from 'body-parser';
import * as cors from 'cors';

// Import WelcomeController from controllers entry point
import {ArtuCX} from './controllers';

// Create a new express application instance
const app: express.Application = express();
app.use(bodyParser.json({limit:"100mb"}));
app.use(cors());

//add random delay
app.use(function (req, res, next) {
    let time =Math.floor((Math.random() * 2500));
    setTimeout(function () {
        next();
    }, time);

});


// The port the express app will listen on
const port = process.env.PORT || 3000;

app.use('/', ArtuCX);

// Serve the application at the given port
app.listen(port, () => {
    // Success callback
    console.log(`Listening at http://localhost:${port}/`);
});
