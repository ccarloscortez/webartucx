import * as React from "react";
import {observer} from "mobx-react";
import {
    Container,
    Segment,
    Menu,
    Popup,
    Icon,
    Dropdown,
    Label,
    List, Modal, Button
} from 'semantic-ui-react'
import {CxStore} from "../backend/models";
import * as moment from 'moment';
import {rootStore} from "../backend/stores";
import * as logo1 from '../assets/images/logo1.png';
import * as logo2 from '../assets/images/logo2.png';
import {CXApi} from "../backend/api";
import {mapResourceKind} from "../utils";
import {ToastContainer} from 'react-toastify';

const marketplace_categories = [
    ["default_rate", "Default rate"],
    ["churn", "Churn"],
    ["upselling", "Upselling"],
    ["cross_selling", "Cross Selling"],
    ["custom_model", "Custom Model"]];


@observer
export class MainLayout extends React.Component <{ fluid?: boolean }, any> {

    render() {

        return (<div id="main-layout">
                <ToastContainer/>


                <div id="left" className="main-layout-column">
                    <div className="bottom">
                        <Menu
                            color={"blue"}
                            inverted
                            vertical
                            borderless

                            style={{height: "100vh"}}>
                            <Segment basic>
                                <div className={"login-logo"}>
                                    <img src={logo1}/>
                                    <img src={logo2}/>
                                </div>
                            </Segment>
                            <Menu.Item>
                                <Icon name='world'/>
                                Tienda
                                <Menu.Menu>
                                    {marketplace_categories.map((item) => {
                                        const active_router = rootStore.routerStore.getCurrentRoute();
                                        const active_query_params: any = rootStore.routerStore.routerState.queryParams;
                                        return <Menu.Item as='a'
                                                          key={item[0]}
                                                          active={active_router.name == "tienda" && active_query_params.category == item[0]}
                                                          onClick={() => {
                                                              if (!(active_router.name == "tienda" && active_query_params.category == item[0])) {
                                                                  rootStore.routerStore.goTo("tienda", {}, {"category": item[0]})

                                                              }
                                                          }}>
                                            {item[1]}
                                        </Menu.Item>

                                    })}
                                </Menu.Menu>

                            </Menu.Item>
                            <Menu.Item>
                                <Icon name='box'/>
                                Mis Modelos
                                <Menu.Menu>
                                    <Menu.Item as='a'
                                               active={rootStore.routerStore.getCurrentRoute().name == "mis_modelos"}
                                               onClick={() => {
                                                   if (!(rootStore.routerStore.getCurrentRoute().name == "mis_modelos")) {
                                                       rootStore.routerStore.goTo("mis_modelos", {}, {page: 1})

                                                   }
                                               }}>
                                        Modelos
                                    </Menu.Item>
                                    <Menu.Item as='a'
                                               active={rootStore.routerStore.getCurrentRoute().name == "entrenamientos"}
                                               onClick={() => {
                                                   if (!(rootStore.routerStore.getCurrentRoute().name == "entrenamientos")) {
                                                       rootStore.routerStore.goTo("entrenamientos", {}, {page: 1})

                                                   }
                                               }}>
                                        Entrenamientos
                                    </Menu.Item>
                                </Menu.Menu>
                            </Menu.Item>
                            <Menu.Item>
                                <Icon name='chart pie'/>
                                Análisis de Negocio
                                <Menu.Menu>
                                    <Menu.Item as='a'
                                               active={rootStore.routerStore.getCurrentRoute().name == "indicadores"}
                                               onClick={() => {
                                                   if (!(rootStore.routerStore.getCurrentRoute().name == "indicadores")) {
                                                       rootStore.routerStore.goTo("indicators")

                                                   }
                                               }}>
                                        Indicadores
                                    </Menu.Item>
                                    <Menu.Item as='a'
                                               active={rootStore.routerStore.getCurrentRoute().name == "analitica_por_segmento"}
                                               onClick={() => {
                                                   if (!(rootStore.routerStore.getCurrentRoute().name == "analitica_por_segmento")) {
                                                       rootStore.routerStore.goTo("analitica_por_segmento")

                                                   }
                                               }}>
                                        Por Segmento
                                    </Menu.Item>
                                    <Menu.Item as='a'
                                               active={rootStore.routerStore.getCurrentRoute().name == "analitica_por_campagna"}
                                               onClick={() => {
                                                   if (!(rootStore.routerStore.getCurrentRoute().name == "analitica_por_campagna")) {
                                                       rootStore.routerStore.goTo("analitica_por_campagna")

                                                   }
                                               }}>
                                        Por Campañas
                                    </Menu.Item>
                                </Menu.Menu>
                            </Menu.Item>
                            <Menu.Item>
                                <Icon name='phone volume'/>
                                Campañas
                                <Menu.Menu>
                                    <Menu.Item as='a'
                                               active={rootStore.routerStore.getCurrentRoute().name == "proyectos"}
                                               onClick={() => {
                                                   if (!(rootStore.routerStore.getCurrentRoute().name == "proyectos")) {
                                                       rootStore.routerStore.goTo("proyectos", {}, {page: 1})

                                                   }
                                               }}>
                                        Proyectos
                                    </Menu.Item>
                                    <Menu.Item as='a'
                                               active={rootStore.routerStore.getCurrentRoute().name == "campaigns"}
                                               onClick={() => {
                                                   if (!(rootStore.routerStore.getCurrentRoute().name == "campaigns")) {
                                                       rootStore.routerStore.goTo("campaigns", {}, {page: 1})

                                                   }
                                               }}>
                                        Campañas
                                    </Menu.Item>
                                    <Menu.Item as='a'
                                               active={rootStore.routerStore.getCurrentRoute().name == "events"}
                                               onClick={() => {
                                                   if (!(rootStore.routerStore.getCurrentRoute().name == "events")) {
                                                       rootStore.routerStore.goTo("events", {}, {page: 1})

                                                   }
                                               }}>
                                        Eventos
                                    </Menu.Item>
                                    <Menu.Item as='a'
                                               active={rootStore.routerStore.getCurrentRoute().name == "templates"}
                                               onClick={() => {
                                                   if (!(rootStore.routerStore.getCurrentRoute().name == "templates")) {
                                                       rootStore.routerStore.goTo("templates", {}, {page: 1})

                                                   }
                                               }}>
                                        Templates
                                    </Menu.Item>
                                </Menu.Menu>
                            </Menu.Item>
                        </Menu>
                    </div>
                </div>
                <div id="right" className="main-layout-column">
                    <div className="bottom main-content">
                        <Segment
                            inverted
                            basic
                            color={"green"}
                            padded={false}
                            textAlign={"right"}
                            style={{padding: 7}}>
                            <Container fluid textAlign={"right"}>
                                <Menu compact icon inverted color={"green"}>
                                    <Dropdown
                                        simple
                                        item
                                        icon='question circle outline'
                                        color={"green"}
                                        className='icon'>
                                    </Dropdown>
                                    <Popup trigger={
                                        <a className={"item"}>
                                            <Icon name='bell outline'/>
                                            {(CxStore.unread_notification_count) > 0 ?
                                                <Label color='red' floating>
                                                    {CxStore.unread_notification_count}
                                                </Label> : null
                                            }
                                        </a>}
                                           on='click'
                                           position={'bottom right'}
                                           flowing
                                           hideOnScroll
                                           content={CxStore.notification_change.size > 0 ? <div>
                                               <Segment basic style={{padding: 0, display: "flex"}}>
                                                   <div style={{flexGrow: 1}}><b>Notificaciones </b></div>

                                                   <div><a href={"#"} onClick={() => {
                                                       for (const nc of CxStore.get_notifications()) {
                                                           if (!CxStore.notification_map.has(nc.id_notification_change)) {
                                                               CxStore.cx_create_notification_read(nc.id_notification_change)
                                                           }
                                                       }
                                                   }}><b>Macar todo como leidó</b></a></div>
                                               </Segment>
                                               <List celled style={{maxHeight: 560}}>
                                                   {CxStore.get_notifications().map((nc) => {
                                                       let footer = CxStore.isLoading(`cx_create_notification_read.${nc.id_notification_change}`) ?
                                                           <b>Leyendo</b> : <a onClick={() => {
                                                               CxStore.cx_create_notification_read(nc.id_notification_change)
                                                           }} href={"#"}>Marcar como leidó</a>;
                                                       return <List.Item key={nc.id_notification_change}
                                                                         className={CxStore.notification_map.has(nc.id_notification_change) ? "read" : "unread"}>
                                                           <List.Icon name='braille'/>
                                                           <List.Content>
                                                               <List.Header
                                                                   style={{color: "blue"}}>{nc.actor_id_usuario}</List.Header>
                                                               <List.Description>
                                                                   {nc.action_on_entity == "created" ? "Creó" : "Eliminó"}&nbsp;
                                                                   <b>{mapResourceKind(nc.entity_type)}</b>&nbsp;
                                                                   <Label>{nc.entity_name}</Label>
                                                                   <br/>
                                                                   {moment(nc.created_at).fromNow()}&nbsp;
                                                                   {CxStore.notification_map.has(nc.id_notification_change) ? null : footer}
                                                               </List.Description>
                                                           </List.Content>
                                                       </List.Item>
                                                   })}


                                               </List></div> : null}/>

                                    <Dropdown
                                        direction='left'
                                        simple
                                        item
                                        icon='user outline'
                                        color={"green"}
                                        className='icon'>
                                        <Dropdown.Menu>
                                            <Dropdown.Item icon='arrow circle left' text='Salir' onClick={() => {
                                                CXApi.clear_session();
                                                rootStore.routerStore.goTo("login")

                                            }}/>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </Menu>
                            </Container>


                        </Segment>
                        <Container style={{flex: 1}} fluid={this.props.fluid}>
                            <Container style={{marginTop: 32}}/>
                            {this.props.children}
                        </Container>
                        <Segment inverted basic>Copyright © {moment().year()} Artu CX.</Segment>
                    </div>
                </div>
            </div>

        )
    }

}


