import * as React from "react";
import {observer} from "mobx-react";
import {
    Label,
    Form,
    Segment,
    Header,
    Table,
    Container,
    Grid,
    Icon,
    Popup,
    Button,
    Checkbox,
    Pagination,
    Loader, Divider, Dropdown, List, Modal, Message,
} from 'semantic-ui-react'
import {
    CxStore,
    IClientModelSortableFields,
    IPostmarkEventSortableFields,
    ISortDirection,
    PostmarkEventKind
} from "../backend/models";
import * as moment from 'moment';
import {rootStore} from "../backend/stores";
import DayPickerInput from 'react-day-picker/DayPickerInput';
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment';
import 'moment/locale/es';


export function mapKind(kind: PostmarkEventKind) {
    if (kind == PostmarkEventKind.open) {
        return <Label circular color={"teal"}>Abierto</Label>
    }
    if (kind == PostmarkEventKind.delivery) {
        return <Label circular color={"olive"}>Entregado</Label>
    }
    if (kind == PostmarkEventKind.click) {
        return <Label circular color={"purple"}>Click</Label>
    }

    if (kind == PostmarkEventKind.bounce) {
        return <Label circular color={"red"}>Rebotados</Label>
    }
}

function clean_kind(k: string[]): string[] {
    let ret = [];
    for (const e of k) {
        if (e in PostmarkEventKind) {
            ret.push(e)
        }
    }
    return ret;
}

const removeElement = function (nums: string[], val: string) {
    for (let i = nums.length - 1; i >= 0; i--) {
        if (nums[i] == val) {
            nums.splice(i, 1);
        }
    }
    return nums.length;
};

export function mapTimeToString(s: string): string {
    switch (s) {
        case "all_time":
            return "Todo el tiempo";
        case "last_30_days":
            return "Último 30 dias";
        case "last_month":
            return "Último mes";
        case "previous_month":
            return "Mes anterior";
        case "last_7_days":
            return "Último 7 dias";
        case "last_week":
            return "Última semana";
        case "previous_week":
            return "Semana anterior";
        case "custom":
            return "Personalizado"
        default:
            return "Todo el tiempo";
    }
}

function isLoading() {
    let loading = false;
    for (const campaign of Array.from(CxStore.campaigns.values())) {
        loading = loading
            || CxStore.isLoading(`cx_campaign_opens.${campaign.id_campaign}`)
            || CxStore.isLoading(`cx_campaign_delivery.${campaign.id_campaign}`)
            || CxStore.isLoading(`cx_campaign_clicks.${campaign.id_campaign}`)
    }
    return loading
}

@observer
export class TimeModal extends React.Component<any, any> {
    state = {
        custom_form: {from: "", to: "", error: ""}
    };

    render() {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;
        const from = this.props.from;
        const to = this.props.to;
        return <Modal size={"mini"} open={queryParams.modal == "custom_time"}>
            <Modal.Header>Intervalo personalizado</Modal.Header>
            <Modal.Content>
                <Form>
                    <div className={"field"}>
                        <label>Desde</label>
                        <DayPickerInput placeholder={from ? formatDate(from) : "Inicio"}
                                        onDayChange={(day: any) => {
                                            this.setState({custom_form: {...this.state.custom_form, from: day}})
                                        }}
                                        value={this.state.custom_form.from}
                                        dayPickerProps={{localeUtils: MomentLocaleUtils, locale: 'es'}}
                                        formatDate={formatDate}
                                        parseDate={parseDate}/>
                    </div>
                    <div className={"field"}>
                        <label>Hasta</label>
                        <DayPickerInput placeholder={to ? formatDate(to) : "Final"}
                                        onDayChange={(day: any) => {
                                            this.setState({custom_form: {...this.state.custom_form, to: day}})
                                        }}
                                        value={this.state.custom_form.to}
                                        dayPickerProps={{localeUtils: MomentLocaleUtils, locale: 'es'}}
                                        formatDate={formatDate}
                                        parseDate={parseDate}/>
                    </div>
                </Form>
                <Message warning
                         hidden={this.state.custom_form.error == ""}>
                    <Message.Header>Fechas incorrectas</Message.Header>
                    <p>{this.state.custom_form.error}.</p>
                </Message> </Modal.Content>
            <Modal.Actions>
                <Button negative onClick={() => {
                    rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                        rootStore.routerStore.routerState.params,
                        {...rootStore.routerStore.routerState.queryParams, modal: undefined}
                    )
                }}>Cancelar</Button>
                <Button positive
                        onClick={() => {
                            if (this.state.custom_form.to == "" || this.state.custom_form.from == "") {
                                this.setState({
                                    custom_form: {
                                        ...this.state.custom_form,
                                        error: "Define tanto una fecha final como una inicial"
                                    }
                                });
                                return
                            }

                            if (moment(this.state.custom_form.to).isBefore(this.state.custom_form.from, "day")) {
                                this.setState({
                                    custom_form: {
                                        ...this.state.custom_form,
                                        error: "La fecha final debe ser mayor que la fecha inicial"
                                    }
                                });
                                return
                            }
                            this.setState({
                                custom_form: {
                                    ...this.state.custom_form,
                                    error: ""
                                }
                            });
                            rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                                rootStore.routerStore.routerState.params,
                                {
                                    ...rootStore.routerStore.routerState.queryParams,
                                    modal: undefined,
                                    time: "custom",
                                    from: moment(this.state.custom_form.from).format("YYYYMMDD"),
                                    to: moment(this.state.custom_form.to).format("YYYYMMDD")

                                })
                        }}
                        icon='checkmark'
                        labelPosition='right'
                        content='Mostrar'/>
            </Modal.Actions>
        </Modal>
    }

}

@observer
export class Events extends React.Component<any, any> {
    state = {
        term: "",
        popup_open: null,
    };
    handleSort = (field: IPostmarkEventSortableFields) => {
        return () => {
            const queryParams: any = rootStore.routerStore.routerState.queryParams;
            rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                rootStore.routerStore.routerState.params,
                {
                    ...queryParams,
                    postmark_event_sort_direction: queryParams.postmark_event_sort_direction == ISortDirection.ascending ? ISortDirection.descending : ISortDirection.ascending,
                    postmark_event_sort_field: field
                })
        };

    };


    render() {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;
        const params: any = rootStore.routerStore.routerState.params;
        let time = queryParams.time;
        let from = null;
        let to = null;
        if (!time) {
            time = "all_time"
        }
        if (time == "last_30_days") {
            to = new Date();
            from = moment(to).add(-30, "days").toDate();
        }
        if (time == "last_month") {
            to = new Date();
            from = moment(to).startOf("month").toDate();
        }
        if (time == "previous_month") {
            to = new Date();
            from = moment(to).startOf("month").add(-1, "month").toDate();
            to = moment(from).endOf("month").toDate()
        }
        if (time == "last_7_days") {
            to = new Date();
            from = moment(to).add(-7, "days").toDate();
        }
        if (time == "last_week") {
            to = new Date();
            from = moment(to).startOf("week").toDate();
        }
        if (time == "previous_week") {
            to = new Date();
            from = moment(to).startOf("week").add(-1, "week").toDate();
            to = moment(from).endOf("week").toDate()
        }
        if (time == "custom") {
            if (queryParams.from) {
                from = moment(queryParams.from, "YYYYMMDD").toDate()
            }
            if (queryParams.to) {
                to = moment(queryParams.to, "YYYYMMDD").toDate()
            }
        }

        let sort = {direction: ISortDirection.descending, field: IPostmarkEventSortableFields.received_at};
        if (queryParams.postmark_event_sort_direction) {
            sort.direction = queryParams.postmark_event_sort_direction
        }
        if (queryParams.postmark_event_sort_field) {
            sort.field = queryParams.postmark_event_sort_field
        }

        let keys = CxStore.get_postmark_event(queryParams.id_campaign, this.state.term, from, to, queryParams.kind || [], sort);
        const items_per_page = 15;
        const total_pages = Math.ceil(keys.length / items_per_page);
        const page: number = parseInt(queryParams.page || 1);
        let index = 0;

        return (
            <Container fluid>
                <TimeModal from={from} to={to}/>
                <Segment stacked padded={"very"}>
                    <div className={"table-header"}>
                        <div className={"table-header-left"}>
                            <Header as="h3">
                                Eventos
                                <Loader size={"tiny"}
                                        active={isLoading()}
                                        inline/>
                            </Header>
                        </div>
                        <div className={"table-header-right"}>
                            <Popup trigger={<Button icon primary onClick={() => {
                                CxStore.cx_list_templates(true);
                                if (queryParams.id_campaign) {
                                    CxStore.cx_list_campaigns().then(() => {
                                        CxStore.cx_campaign_clicks(queryParams.id_campaign, true);
                                        CxStore.cx_campaign_opens(queryParams.id_campaign, true);
                                        CxStore.cx_campaign_delivery(queryParams.id_campaign, true);
                                        CxStore.cx_campaign_bounce(queryParams.id_campaign, true);
                                    })

                                } else {
                                    CxStore.cx_list_postmark_events(true);
                                }
                            }}>
                                <Icon name='undo'/>
                            </Button>} content='Actualizar'/>
                        </div>
                    </div>
                    <Form>
                        <div style={{display: "flex"}}>
                            <div style={{flexGrow: 1, paddingRight: 8}}>
                                <Form.Input icon={"search"}
                                            value={this.state.term}
                                            onKeyDown={(evt: any) => {
                                                var code = evt.charCode || evt.keyCode;
                                                if (code == 27) {
                                                    this.setState({term: ""})
                                                }
                                            }}
                                            onChange={(evt: any, data: any) => {
                                                this.setState({term: data.value})
                                            }}
                                            placeholder={"Buscar por recipiente, sujeto o campaña"}
                                            iconPosition={"left"}/>
                            </div>
                            <div>
                                <Popup
                                    open={this.state.popup_open == "filters"}
                                    onClose={() => {
                                        if (this.state.popup_open == "filters") this.setState({popup_open: null})
                                    }}
                                    onOpen={() => {
                                        this.setState({popup_open: "filters"})
                                    }}

                                    trigger={<Button primary icon>Filtros<Icon name={"caret down"}/></Button>}
                                    content={<Form>
                                        <h5>Eventos</h5>
                                        <Form.Field style={{alignItems: "center", display: "flex"}}>
                                            <Checkbox
                                                checked={(queryParams.kind || []).indexOf(PostmarkEventKind.open) >= 0}
                                                onClick={() => {
                                                    const queryParams: any = rootStore.routerStore.routerState.queryParams;
                                                    let kind = [];
                                                    if (queryParams.kind) {
                                                        if (typeof queryParams.kind === 'string' || queryParams.kind instanceof String) {
                                                            kind.push(queryParams.kind)
                                                        } else {
                                                            kind = Array.from(queryParams.kind);
                                                        }
                                                    }

                                                    if (kind.indexOf(PostmarkEventKind.open) < 0) {
                                                        kind.push(PostmarkEventKind.open)
                                                    } else {
                                                        removeElement(kind, PostmarkEventKind.open)
                                                    }
                                                    rootStore.routerStore.goTo(
                                                        rootStore.routerStore.routerState.routeName,
                                                        rootStore.routerStore.routerState.params,
                                                        {
                                                            ...rootStore.routerStore.routerState.queryParams,
                                                            kind: clean_kind(kind),
                                                        });
                                                }}/>&nbsp;
                                            <Label circular color={"teal"}>
                                                Abierto
                                                <Label.Detail>
                                                    {CxStore.get_postmark_event(queryParams.id_campaign,
                                                        this.state.term,
                                                        from,
                                                        to,
                                                        [PostmarkEventKind.open]).length}
                                                </Label.Detail>
                                            </Label>
                                        </Form.Field>
                                        <Form.Field style={{alignItems: "center", display: "flex"}}>
                                            <Checkbox
                                                checked={(queryParams.kind || []).indexOf(PostmarkEventKind.delivery) >= 0}
                                                onClick={() => {
                                                    const queryParams: any = rootStore.routerStore.routerState.queryParams;
                                                    let kind = [];
                                                    if (queryParams.kind) {
                                                        if (typeof queryParams.kind === 'string' || queryParams.kind instanceof String) {
                                                            kind.push(queryParams.kind)
                                                        } else {
                                                            kind = Array.from(queryParams.kind);
                                                        }
                                                    }

                                                    if (kind.indexOf(PostmarkEventKind.delivery) < 0) {
                                                        kind.push(PostmarkEventKind.delivery)
                                                    } else {
                                                        removeElement(kind, PostmarkEventKind.delivery)
                                                    }
                                                    rootStore.routerStore.goTo(
                                                        rootStore.routerStore.routerState.routeName,
                                                        rootStore.routerStore.routerState.params,
                                                        {
                                                            ...rootStore.routerStore.routerState.queryParams,
                                                            kind: clean_kind(kind),
                                                        });
                                                }}/>&nbsp;
                                            <Label circular color={"olive"}>
                                                Entregado
                                                <Label.Detail>
                                                    {CxStore.get_postmark_event(queryParams.id_campaign,
                                                        this.state.term,
                                                        from,
                                                        to,
                                                        [PostmarkEventKind.delivery]).length}
                                                </Label.Detail>
                                            </Label>

                                        </Form.Field>
                                        <Form.Field style={{alignItems: "center", display: "flex"}}>
                                            <Checkbox
                                                checked={(queryParams.kind || []).indexOf(PostmarkEventKind.click) >= 0}
                                                onClick={() => {
                                                    const queryParams: any = rootStore.routerStore.routerState.queryParams;
                                                    let kind = [];
                                                    if (queryParams.kind) {
                                                        if (typeof queryParams.kind === 'string' || queryParams.kind instanceof String) {
                                                            kind.push(queryParams.kind)
                                                        } else {
                                                            kind = Array.from(queryParams.kind);
                                                        }
                                                    }
                                                    if (kind.indexOf(PostmarkEventKind.click) < 0) {
                                                        kind.push(PostmarkEventKind.click)
                                                    } else {
                                                        removeElement(kind, PostmarkEventKind.click)
                                                    }
                                                    rootStore.routerStore.goTo(
                                                        rootStore.routerStore.routerState.routeName,
                                                        rootStore.routerStore.routerState.params,
                                                        {
                                                            ...rootStore.routerStore.routerState.queryParams,
                                                            kind: clean_kind(kind),
                                                        });
                                                }}/>&nbsp;
                                            <Label circular color={"purple"}>
                                                Click
                                                <Label.Detail>
                                                    {CxStore.get_postmark_event(queryParams.id_campaign,
                                                        this.state.term,
                                                        from,
                                                        to,
                                                        [PostmarkEventKind.click]).length}
                                                </Label.Detail>
                                            </Label>

                                        </Form.Field>
                                        <Form.Field style={{alignItems: "center", display: "flex"}}>
                                            <Checkbox
                                                checked={(queryParams.kind || []).indexOf(PostmarkEventKind.bounce) >= 0}
                                                onClick={() => {
                                                    const queryParams: any = rootStore.routerStore.routerState.queryParams;
                                                    let kind = [];
                                                    if (queryParams.kind) {
                                                        if (typeof queryParams.kind === 'string' || queryParams.kind instanceof String) {
                                                            kind.push(queryParams.kind)
                                                        } else {
                                                            kind = Array.from(queryParams.kind);
                                                        }
                                                    }
                                                    if (kind.indexOf(PostmarkEventKind.bounce) < 0) {
                                                        kind.push(PostmarkEventKind.bounce)
                                                    } else {
                                                        removeElement(kind, PostmarkEventKind.bounce)
                                                    }
                                                    rootStore.routerStore.goTo(
                                                        rootStore.routerStore.routerState.routeName,
                                                        rootStore.routerStore.routerState.params,
                                                        {
                                                            ...rootStore.routerStore.routerState.queryParams,
                                                            kind: clean_kind(kind),
                                                        });
                                                }}/>&nbsp;
                                            <Label circular color={"red"}>
                                                Rebotados
                                                <Label.Detail>
                                                    {CxStore.get_postmark_event(queryParams.id_campaign,
                                                        this.state.term,
                                                        from,
                                                        to,
                                                        [PostmarkEventKind.bounce]).length}
                                                </Label.Detail>
                                            </Label>

                                        </Form.Field>
                                        <Divider/>
                                        <Header as={"h5"}>Campaña</Header>
                                        <Dropdown upward
                                                  search
                                                  placeholder='Campaña'
                                                  value={queryParams.id_campaign}
                                                  onChange={(e: any, data: any) => {
                                                      rootStore.routerStore.goTo(
                                                          rootStore.routerStore.routerState.routeName,
                                                          rootStore.routerStore.routerState.params,
                                                          {
                                                              ...rootStore.routerStore.routerState.queryParams,
                                                              id_campaign: data.value,
                                                          });
                                                  }}
                                                  selection
                                                  options={CxStore.get_campaign_option()}/>
                                    </Form>
                                    }
                                    on='click'
                                    position='bottom left'
                                />
                            </div>

                            <div>

                                <Dropdown
                                    open={this.state.popup_open == "time"}
                                    onClose={() => {
                                        if (this.state.popup_open == "time") this.setState({popup_open: null})
                                    }}
                                    onOpen={() => {
                                        this.setState({popup_open: "time"})
                                    }}
                                    button
                                    className='icon primary'
                                    floating
                                    labeled
                                    icon='calendar'
                                    text={mapTimeToString(time)}>
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={() => {
                                            rootStore.routerStore.goTo(
                                                rootStore.routerStore.routerState.routeName,
                                                rootStore.routerStore.routerState.params,
                                                {
                                                    ...rootStore.routerStore.routerState.queryParams,
                                                    time: "all_time",
                                                    from: undefined,
                                                    to: undefined
                                                });
                                        }}>Todo el tiempo</Dropdown.Item>
                                        <Dropdown.Item onClick={() => {
                                            rootStore.routerStore.goTo(
                                                rootStore.routerStore.routerState.routeName,
                                                rootStore.routerStore.routerState.params,
                                                {
                                                    ...rootStore.routerStore.routerState.queryParams,
                                                    time: "last_30_days",
                                                    from: undefined,
                                                    to: undefined
                                                });
                                        }}>Último 30 dias</Dropdown.Item>
                                        <Dropdown.Item onClick={() => {
                                            rootStore.routerStore.goTo(
                                                rootStore.routerStore.routerState.routeName,
                                                rootStore.routerStore.routerState.params,
                                                {
                                                    ...rootStore.routerStore.routerState.queryParams,
                                                    time: "last_month",
                                                    from: undefined,
                                                    to: undefined
                                                });
                                        }}>Último mes</Dropdown.Item>
                                        <Dropdown.Item onClick={() => {
                                            rootStore.routerStore.goTo(
                                                rootStore.routerStore.routerState.routeName,
                                                rootStore.routerStore.routerState.params,
                                                {
                                                    ...rootStore.routerStore.routerState.queryParams,
                                                    time: "previous_month",
                                                    from: undefined,
                                                    to: undefined
                                                });
                                        }}>Mes anterior</Dropdown.Item>
                                        <Dropdown.Item onClick={() => {
                                            rootStore.routerStore.goTo(
                                                rootStore.routerStore.routerState.routeName,
                                                rootStore.routerStore.routerState.params,
                                                {
                                                    ...rootStore.routerStore.routerState.queryParams,
                                                    time: "last_7_days",
                                                    from: undefined,
                                                    to: undefined
                                                });
                                        }}>Último 7 dias</Dropdown.Item>
                                        <Dropdown.Item onClick={() => {
                                            rootStore.routerStore.goTo(
                                                rootStore.routerStore.routerState.routeName,
                                                rootStore.routerStore.routerState.params,
                                                {
                                                    ...rootStore.routerStore.routerState.queryParams,
                                                    time: "last_week",
                                                    from: undefined,
                                                    to: undefined
                                                });
                                        }}>Última semana</Dropdown.Item>
                                        <Dropdown.Item onClick={() => {
                                            rootStore.routerStore.goTo(
                                                rootStore.routerStore.routerState.routeName,
                                                rootStore.routerStore.routerState.params,
                                                {
                                                    ...rootStore.routerStore.routerState.queryParams,
                                                    time: "previous_week",
                                                    from: undefined,
                                                    to: undefined
                                                });
                                        }}>Semana anterior</Dropdown.Item>
                                        <Dropdown.Item onClick={() => {
                                            rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                                                rootStore.routerStore.routerState.params,
                                                {...rootStore.routerStore.routerState.queryParams, modal: "custom_time"}
                                            )
                                        }}>Personalizado</Dropdown.Item>

                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                        </div>


                    </Form>

                    <Table celled striped sortable>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell
                                    sorted={sort.field == IPostmarkEventSortableFields.kind ? sort.direction : undefined}
                                    onClick={this.handleSort(IPostmarkEventSortableFields.kind)}>
                                    Evento
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={sort.field == IPostmarkEventSortableFields.recipient ? sort.direction : undefined}
                                    onClick={this.handleSort(IPostmarkEventSortableFields.recipient)}>
                                    Recipiente
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={sort.field == IPostmarkEventSortableFields.subject ? sort.direction : undefined}
                                    onClick={this.handleSort(IPostmarkEventSortableFields.subject)}>
                                    Sujeto
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={sort.field == IPostmarkEventSortableFields.campaign ? sort.direction : undefined}
                                    onClick={this.handleSort(IPostmarkEventSortableFields.campaign)}>
                                    Campaña
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={sort.field == IPostmarkEventSortableFields.received_at ? sort.direction : undefined}
                                    onClick={this.handleSort(IPostmarkEventSortableFields.received_at)}>
                                    Fecha
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {keys.map((pe) => {

                                if (index < ((page - 1) * items_per_page)) {
                                    index = index + 1;
                                    return null;
                                } else if (index >= page * items_per_page) {
                                    return null
                                }

                                index = index + 1;
                                return <Table.Row key={pe.id}>
                                    <Table.Cell>
                                        {mapKind(pe.kind)}
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Label circular style={{marginRight: 16}}>
                                            {pe.recipient[0].toUpperCase()}
                                        </Label>

                                        {pe.recipient.toLowerCase()}
                                    </Table.Cell>
                                    <Table.Cell>
                                        {pe.campaign.mailtemplate.title}
                                    </Table.Cell>
                                    <Table.Cell>
                                        {pe.campaign.name}
                                    </Table.Cell>
                                    <Table.Cell>
                                        {moment(pe.received_at).calendar(undefined, {
                                            sameElse: 'DD MMMM, YYYY'
                                        })}
                                    </Table.Cell>
                                </Table.Row>
                            })}

                        </Table.Body>
                    </Table>

                    <Grid columns='equal' style={{paddingTop: 16}}>
                        <Grid.Column floated='left'>
                            Mostrando
                            del {(page - 1) * items_per_page} al {page * items_per_page} de {keys.length} ítems
                        </Grid.Column>
                        <Grid.Column floated='right' width={8} textAlign="right">
                            {total_pages > 0 ?
                                <Pagination
                                    onPageChange={(event, data) => {
                                        rootStore.routerStore.goTo(
                                            "events",
                                            rootStore.routerStore.routerState.params,
                                            {
                                                ...rootStore.routerStore.routerState.queryParams,
                                                page: data.activePage,
                                            });
                                    }}
                                    defaultActivePage={page}
                                    ellipsisItem={{
                                        content: <Icon name='ellipsis horizontal'/>,
                                        icon: true
                                    }}
                                    firstItem={{content: <Icon name='angle double left'/>, icon: true}}
                                    lastItem={{content: <Icon name='angle double right'/>, icon: true}}
                                    prevItem={{content: <Icon name='angle left'/>, icon: true}}
                                    nextItem={{content: <Icon name='angle right'/>, icon: true}}
                                    totalPages={total_pages}
                                /> : null
                            }
                        </Grid.Column>
                    </Grid>
                </Segment>


            </Container>
        )
    }

}
