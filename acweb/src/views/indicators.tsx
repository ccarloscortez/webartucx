import * as React from "react";
import {observer} from "mobx-react";
import {
    Segment,
    Header,
    Table,
    Label,
    Grid,
    Icon,
    Pagination,
    Button,
    Loader,
    Container,
    Image,
    Form
} from 'semantic-ui-react'
import {CxStore} from "../backend/models";
import * as moment from 'moment';
import {
    Sector,
    PieChart,
    Pie,
} from 'recharts';
import {toast} from 'react-toastify';
import {on} from "cluster";


const renderActiveShape = (props: any) => {
    const RADIAN = Math.PI / 180;
    const {
        cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
        fill, payload, percent, value
    } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? 'start' : 'end';

    return (
        <g>
            <text x={cx} y={cy} dy={8} textAnchor="middle"
                  fill={fill}>{payload.positive ? "Positivo" : "Negativo"}</text>
            <Sector
                cx={cx}
                cy={cy}
                innerRadius={innerRadius}
                outerRadius={outerRadius}
                startAngle={startAngle}
                endAngle={endAngle}
                fill={fill}
            />
            <Sector
                cx={cx}
                cy={cy}
                startAngle={startAngle}
                endAngle={endAngle}
                innerRadius={outerRadius + 6}
                outerRadius={outerRadius + 10}
                fill={fill}
            />
            <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none"/>
            <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none"/>
            <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor}
                  fill="#333">{`${payload.name} ${value}`}</text>
            <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
                {`(Porcentaje ${(percent * 100).toFixed(2)}%)`}
            </text>
        </g>
    );
};

@observer
export class IndicatorChart extends React.Component <any, any> {
    state = {activeIndex: 0};

    onPieEnter = (data: any, index: any) => {
        this.setState({
            activeIndex: index,
        });
    };

    render() {
        let pie_chart_data: any = [
            {
                name: this.props.prediction.statistic.positive_level_name,
                value: parseInt(this.props.prediction.statistic.positive_level),
                positive: true
            },
            {
                name: this.props.prediction.statistic.negative_level_name,
                value: parseInt(this.props.prediction.statistic.negative_level),
                positive: false
            }
        ];
        return <PieChart width={800} height={300} style={{flexGrowth: 1}}>
            <Pie
                activeIndex={this.state.activeIndex}
                activeShape={renderActiveShape}
                data={pie_chart_data}
                cx={300}
                cy={150}
                innerRadius={60}
                outerRadius={80}
                fill="#8884d8"
                onMouseEnter={this.onPieEnter}
            />
        </PieChart>
    }

}

@observer
export class Indicators extends React.Component <any, any> {

    state = {myDate: moment()};

    render() {
        return (
            <Container fluid>


                <Grid columns={2} stackable>
                    <Grid.Row>
                        {Array.from(CxStore.predictions.values()).map((prediction) => {
                            if (!prediction.on_dashboard || !prediction.statistic) return null;


                            return <Grid.Column style={{marginBottom: 16}} key={prediction.id_prediction}>
                                <Segment
                                    loading={CxStore.isLoading(`cx_create_smart_campaign.${prediction.id_prediction}`)}>
                                    <Header as={"h5"}>{prediction.statistic.report_name}</Header>
                                    <p>{prediction.statistic.report_desc}</p>
                                    <IndicatorChart prediction={prediction}/>
                                    <Button primary onClick={() => {
                                        CxStore.cx_create_smart_campaign(prediction.id_prediction, (id_project, id_segment) => {
                                            let project = CxStore.projects.get(id_project);
                                            let segment = CxStore.segments.get(id_segment);
                                            if (!project || !segment) {
                                                return
                                            }
                                            toast(<div>Proyecto <Label>{project.name}</Label> creado</div>);
                                            toast(<div>Segmento <Label>{segment.name}</Label> creado</div>);


                                        })
                                    }}>Crear Campaña Inteligente</Button>
                                </Segment>

                            </Grid.Column>

                        })}
                    </Grid.Row>
                </Grid>
            </Container>

        )
    }

}


