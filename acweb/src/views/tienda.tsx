import * as React from "react";
import {observer} from "mobx-react";
import {
    Label,
    Menu,
    Dropdown,
    Segment,
    Header,
    Table,
    Container,
    Grid,
    Icon,
    Pagination,
    Button,
    Loader, Popup
} from 'semantic-ui-react'
import {CxStore, ICxModel, IModelSortableFields, ISort, ISortDirection} from "../backend/models";
import * as moment from 'moment';
import {rootStore} from "../backend/stores";
import {capatilizedFromSnakeCase} from "../utils"
import {toast} from "react-toastify";

@observer
export class Tienda extends React.Component <any, any> {
    handleSort = (field: IModelSortableFields) => {
        return () => {
            const queryParams: any = rootStore.routerStore.routerState.queryParams;
            rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                rootStore.routerStore.routerState.params,
                {
                    ...queryParams,
                    model_sort_direction: queryParams.model_sort_direction == ISortDirection.ascending ? ISortDirection.descending : ISortDirection.ascending,
                    model_sort_field: field
                })
        };

    };

    render() {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;
        let sort = {direction: ISortDirection.descending, field: IModelSortableFields.creation_time};
        if (queryParams.model_sort_direction) {
            sort.direction = queryParams.model_sort_direction
        }
        if (queryParams.model_sort_field) {
            sort.field = queryParams.model_sort_field
        }
        let models: ICxModel[] = CxStore.get_models(queryParams.category, "", null, null, sort);
        const items_per_page = 5;
        const total_pages = Math.ceil(models.length / items_per_page);
        const page: number = parseInt(queryParams.page || 1);
        let desc: any = CxStore.models.get(queryParams.id_model) && CxStore.models.get(queryParams.id_model)!.desc;
        let desc_title: string = "";
        let desc_type: string = "";

        if (desc) {
            //clone desc
            desc = JSON.parse(JSON.stringify(desc));
            desc_title = desc.title;
            delete  desc.title;
            desc_type = desc.type;
            delete  desc.type;
        }
        let index = 0;

        return (
            <Container fluid>
                <Segment stacked padded={"very"}>
                    <div className={"table-header"}>
                        <div className={"table-header-left"}>
                            <Header as="h3">
                                Tienda:
                                <Label color={"orange"}>
                                    {capatilizedFromSnakeCase(queryParams.category)}
                                </Label>
                                <Loader size={"tiny"}
                                        active={CxStore.isLoading("cx_list_models")}
                                        inline/>
                            </Header>
                        </div>
                        <div className={"table-header-right"}>
                            <Popup trigger={<Button icon primary onClick={() => {
                                CxStore.cx_list_models(true)
                            }}>
                                <Icon name='undo'/>
                            </Button>} content='Actualizar'/>

                        </div>
                    </div>
                    <Table celled selectable sortable>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell
                                    sorted={sort.field == IModelSortableFields.name ? sort.direction : undefined}
                                    onClick={this.handleSort(IModelSortableFields.name)}>
                                    Nombre
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={sort.field == IModelSortableFields.creation_time ? sort.direction : undefined}
                                    onClick={this.handleSort(IModelSortableFields.creation_time)}>
                                    Creado
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={sort.field == IModelSortableFields.category ? sort.direction : undefined}
                                    onClick={this.handleSort(IModelSortableFields.category)}>
                                    Categoría
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={sort.field == IModelSortableFields.industry ? sort.direction : undefined}
                                    onClick={this.handleSort(IModelSortableFields.industry)}>
                                    Servicio
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={sort.field == IModelSortableFields.version ? sort.direction : undefined}
                                    onClick={this.handleSort(IModelSortableFields.version)}>
                                    Versión
                                </Table.HeaderCell>
                                <Table.HeaderCell collapsing/>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {models.map((model) => {
                                if (index < ((page - 1) * items_per_page)) {
                                    index = index + 1;
                                    return null;
                                } else if (index >= page * items_per_page) {
                                    return null
                                }

                                index = index + 1;
                                let add_button;
                                if (model.is_added) {
                                    add_button = <Button size={"tiny"} disabled>Agregado</Button>
                                } else if (CxStore.isLoading(`cx_add_model.${model.id_model}`)) {
                                    add_button = <Button size={"tiny"} disabled> <Loader size={"tiny"} active inline/>Agregando</Button>
                                }
                                else {
                                    add_button = <Button size={"tiny"} positive onClick={() => {
                                        CxStore.cx_add_model(model.id_model).then(()=>{
                                            toast(<div>Modelo <Label>{model.name}</Label> agregado</div>);

                                        });
                                    }}>Agregar</Button>

                                }

                                return <Table.Row key={model.id_model}
                                                  positive={queryParams.id_model == model.id_model}>
                                    <Table.Cell>
                                        <Button size={"tiny"} onClick={() => {
                                            rootStore.routerStore.goTo("tienda", {}, {
                                                page: page,
                                                category: queryParams.category,
                                                id_model: model.id_model
                                            });

                                        }} secondary={queryParams.id_model == model.id_model}>
                                            {model.name}
                                        </Button>
                                    </Table.Cell>
                                    <Table.Cell>{moment(model.creation_time).fromNow()}</Table.Cell>
                                    <Table.Cell>{capatilizedFromSnakeCase(model.category)}</Table.Cell>
                                    <Table.Cell>{model.industry}</Table.Cell>
                                    <Table.Cell>{model.version}</Table.Cell>
                                    <Table.Cell collapsing>
                                        {add_button}
                                    </Table.Cell>
                                </Table.Row>
                            })}

                        </Table.Body>

                    </Table>
                    <Grid columns='equal' style={{paddingTop: 16}}>
                        <Grid.Column floated='left'>
                            Mostrando
                            del {(page - 1) * items_per_page} al {page * items_per_page} de {models.length} ítems
                        </Grid.Column>
                        <Grid.Column floated='right' width={8} textAlign="right">
                            {total_pages > 0 ?

                                <Pagination onPageChange={(event, data) => {
                                    rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                                        rootStore.routerStore.routerState.params,
                                        {
                                            ...rootStore.routerStore.routerState.queryParams,
                                            page: data.activePage,
                                        });
                                }}
                                            defaultActivePage={page}
                                            ellipsisItem={{content: <Icon name='ellipsis horizontal'/>, icon: true}}
                                            firstItem={{content: <Icon name='angle double left'/>, icon: true}}
                                            lastItem={{content: <Icon name='angle double right'/>, icon: true}}
                                            prevItem={{content: <Icon name='angle left'/>, icon: true}}
                                            nextItem={{content: <Icon name='angle right'/>, icon: true}}
                                            totalPages={total_pages}
                                /> : null
                            }
                        </Grid.Column>
                    </Grid>

                </Segment>
                {desc ?
                    <Segment padded={"very"}>
                        <Header as={"h4"}>
                            Descripción:
                        </Header>
                        <Segment basic>
                            {desc_title}
                        </Segment>
                        <Header as={"h4"}>
                            Formato: <Label color={"green"}>{desc_type}</Label>
                        </Header>
                        <Header as={"h4"}>
                            Esquema:
                        </Header>
                        <Table striped collapsing>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>Columna</Table.HeaderCell>
                                    <Table.HeaderCell>Tipo</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>

                            <Table.Body>
                                {Object.keys(desc).map((key) => {
                                    return <Table.Row key={key}>
                                        <Table.Cell>
                                            {key}
                                        </Table.Cell>
                                        <Table.Cell>
                                            {desc[key]}
                                        </Table.Cell>
                                    </Table.Row>

                                })}

                            </Table.Body>

                        </Table>
                    </Segment> : null}
            </Container>
        )
    }

}


