import * as React from "react";
import {observer} from "mobx-react";
import {
    Label,
    Form,
    Segment,
    Header,
    Table,
    Container,
    Grid,
    Icon,
    Pagination,
    Button,
    TextArea,
    Loader,
    Popup, Modal, Message
} from 'semantic-ui-react'
import {
    CxStore,
    ICxProject,
    ICxSegment,
    IProjectSortableFields,
    ISegmentSortableFields,
    ISortDirection
} from "../backend/models";
import * as moment from 'moment';
import {rootStore} from "../backend/stores";
import {toast} from "react-toastify";
import {removeModal} from "../utils";
import * as Papa from "papaparse"

@observer
class ProjectDeleteConfirmModal extends React.Component<any, any> {
    close = () => {
        rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
            rootStore.routerStore.routerState.params,
            removeModal(rootStore.routerStore.routerState.queryParams)
        )
    };

    remove = () => {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;
        let project_id = queryParams.modal_project_id;
        let project = CxStore.projects.get(project_id);
        if (!project) {
            return null
        }
        const tn = project.name;
        CxStore.cx_delete_project(project.id_project).then(() => {
            toast.error(
                <div>Proyecto <Label>{tn}</Label> eliminado
                </div>);
        });
        this.close()
    };

    render() {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;
        let project_id = queryParams.modal_project_id;
        let project = CxStore.projects.get(project_id);
        if (!project) {
            return null
        }
        return <Modal open={queryParams.modal == "project_delete_confirmation"}
                      closeOnEscape
                      size={'tiny'}
                      closeOnDimmerClick
                      onClose={this.close}>
            <Modal.Header>Eliminar proyecto</Modal.Header>
            <Modal.Content>
                <p>Esta seguro que desea eliminar el proyecto <Label>{project.name}</Label></p>
            </Modal.Content>
            <Modal.Actions>
                <Button onClick={this.close} negative>
                    No
                </Button>
                <Button onClick={this.remove}
                        positive
                        labelPosition='right'
                        icon='checkmark'
                        content='Si'/>
            </Modal.Actions>
        </Modal>
    }
}

@observer
class SegmentDeleteConfirmModal extends React.Component<any, any> {
    close = () => {
        rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
            rootStore.routerStore.routerState.params,
            removeModal(rootStore.routerStore.routerState.queryParams)
        )
    };

    remove = () => {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;
        let segment_id = queryParams.modal_segment_id;
        let segment = CxStore.segments.get(segment_id);
        if (!segment) {
            return null
        }
        const tn = segment.name;
        CxStore.cx_delete_segment(segment.id_segment).then(() => {
            toast.error(
                <div>Segmento <Label>{tn}</Label> eliminado
                </div>);
        });
        this.close()
    };

    render() {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;
        let segment_id = queryParams.modal_segment_id;
        let segment = CxStore.segments.get(segment_id);
        if (!segment) {
            return null
        }
        return <Modal open={queryParams.modal == "segment_delete_confirmation"}
                      closeOnEscape
                      size={'tiny'}
                      closeOnDimmerClick
                      onClose={this.close}>
            <Modal.Header>Eliminar segmento</Modal.Header>
            <Modal.Content>
                <p>Esta seguro que desea eliminar el segmento <Label>{segment.name}</Label></p>
            </Modal.Content>
            <Modal.Actions>
                <Button onClick={this.close} negative>
                    No
                </Button>
                <Button onClick={this.remove}
                        positive
                        labelPosition='right'
                        icon='checkmark'
                        content='Si'/>
            </Modal.Actions>
        </Modal>
    }
}

@observer
export class SegmentList extends React.Component <any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            segment_form: {
                isOpen: false,
                name: "",
                desc: "",
                campaign_input_url: "",
                channel_type: "",
                uploading: false,
                csv_errors: []
            },
            id_segment: null,
            page: 1
        };
    }

    handleSort = (field: ISegmentSortableFields) => {
        return () => {
            const queryParams: any = rootStore.routerStore.routerState.queryParams;
            rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                rootStore.routerStore.routerState.params,
                {
                    ...queryParams,
                    segment_sort_direction: queryParams.segment_sort_direction == ISortDirection.ascending ? ISortDirection.descending : ISortDirection.ascending,
                    segment_sort_field: field
                })
        };

    };
    upload_segment = (e: any) => {
        console.log("called");//dont remove this line, there is a strange bugs when is removed you cant upload many images at the same time
        let file = e.target.files[0];
        this.setState({
            segment_form: {
                ...this.state.segment_form,
                uploading: true
            }
        });
        Papa.parse(file, {
            skipEmptyLines: true,
            complete: (result) => {
                if (result.errors.length == 0) {
                    const csv_content = btoa(Papa.unparse(result.data));
                    CxStore.cx_upload_segment("predictinput", "csv", csv_content, (url: string) => {
                        this.setState({
                            segment_form: {
                                ...this.state.segment_form,
                                campaign_input_url: url,
                                uploading: false,
                                csv_errors: []
                            }
                        });
                    });
                } else {
                    this.setState({
                        segment_form: {
                            ...this.state.segment_form,
                            csv_errors: result.errors,
                            uploading: false
                        }
                    });
                }

            }
        });

    };

    render() {
        if (!this.props.project) {
            return
        }
        const queryParams: any = rootStore.routerStore.routerState.queryParams;

        let sort = {direction: ISortDirection.descending, field: ISegmentSortableFields.creation_time};
        if (queryParams.segment_sort_direction) {
            sort.direction = queryParams.segment_sort_direction
        }
        if (queryParams.segment_sort_field) {
            sort.field = queryParams.segment_sort_field
        }
        let segments: ICxSegment[] = CxStore.get_segments(this.props.project.id_project, sort);

        const items_per_page = 7;
        const total_pages = Math.ceil(segments.length / items_per_page);
        const page: number = this.state.page;
        let picked_segment = CxStore.segments.get(this.state.id_segment);
        let index = 0;
        let key_index = 0 ;


        return <Segment>
            <Header as={"h4"}>Descripción del proyecto</Header>
            <p>{this.props.project.desc}</p>
            <div className={"table-header"}>
                <div className={"table-header-left"}>
                    <Header as="h3">
                        Segmentos
                        <Loader size={"tiny"}
                                active={CxStore.isLoading("cx_list_segments")}
                                inline/>
                    </Header>
                </div>
                <div className={"table-header-right"}>
                    <Popup trigger={<Button icon primary onClick={() => {
                        CxStore.cx_list_segments(this.props.project.id_project, true)
                    }}>
                        <Icon name='undo'/>
                    </Button>} content='Actualizar'/>
                    <Popup trigger={<Button icon primary={!this.state.segment_form.isOpen}
                                            negative={this.state.segment_form.isOpen} onClick={() => {
                        this.setState({
                            segment_form: {
                                ...this.state.segment_form,
                                isOpen: !this.state.segment_form.isOpen,
                            }
                        });
                    }}>
                        {this.state.project_form_open ? <Icon name='minus'/> : <Icon name='plus'/>}
                    </Button>} content='Agregar segmento'/>
                </div>
            </div>
            {this.state.segment_form.isOpen ? <Segment basic loading={CxStore.isLoading("cx_create_segment")}>
                <Form
                    loading={CxStore.isLoading('cx_create_segment') || CxStore.isLoading('cx_upload_segment') || this.state.segment_form.uploading}
                    onSubmit={() => {
                        if (this.state.segment_form.csv_errors.length > 0) {
                            return
                        }
                        CxStore.cx_create_segment(
                            this.props.project.id_project,
                            this.state.segment_form.campaign_input_url,
                            this.state.segment_form.channel_type,
                            this.state.segment_form.name,
                            this.state.segment_form.desc).then(() => {
                            toast(<div>Segmento <Label>{this.state.segment_form.name}</Label> creado</div>);

                            this.setState({
                                segment_form: {
                                    isOpen: false,
                                    campaign_input_url: "",
                                    channel_type: "",
                                    name: "",
                                    desc: "",
                                    uploading: false
                                }
                            })
                        })
                    }}>
                    <Form.Group widths={2}>
                        <Form.Input label='Nombre' placeholder='Nombre' onChange={(e: any, data: any) => {
                            this.setState({
                                segment_form: {
                                    ...this.state.segment_form,
                                    name: data.value,
                                }
                            })
                        }} value={this.state.segment_form.name} required/>
                        <Form.Select label='Canal'
                                     options={[
                                         {key: 'e', text: 'Email', value: 'email'},
                                         {key: 's', text: 'SMS', value: 'sms'},
                                         {key: 'l', text: 'LLamadas', value: 'llamadas'},
                                     ]}
                                     placeholder='Canal'
                                     onChange={(e: any, data: any) => {
                                         this.setState({
                                             segment_form: {
                                                 ...this.state.segment_form,
                                                 channel_type: data.value,
                                             }
                                         })
                                     }}
                                     value={this.state.segment_form.channel_type}
                                     required/>

                        <Form.Input label='Datos' type="file" onChange={this.upload_segment}
                                    className={this.state.segment_form.csv_errors.length > 0 ? "error" : undefined}/>
                    </Form.Group>
                    <Message negative hidden={! (this.state.segment_form.csv_errors.length > 0)}>
                        <Message.Header>Errores en el csv</Message.Header>
                        {this.state.segment_form.csv_errors.map((item: any) => {
                            key_index += 1;
                            return (
                                <p key={key_index}>fila: {item.row},   error: {item.message}</p>
                            )
                        })}
                        <p>That offer has expired</p>
                    </Message>
                    <div className={"field"}>
                        <label>Descripción</label>
                        <TextArea autoHeight placeholder='Descripción' onChange={(e: any, data: any) => {
                            this.setState({
                                segment_form: {
                                    ...this.state.segment_form,
                                    desc: data.value,
                                }
                            })
                        }} value={this.state.segment_form.desc}/>
                    </div>
                    <Button secondary>Crear</Button>
                </Form>
            </Segment> : null}
            <Table celled selectable sortable>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell
                            sorted={sort.field == ISegmentSortableFields.name ? sort.direction : undefined}
                            onClick={this.handleSort(ISegmentSortableFields.name)}>
                            Nombre
                        </Table.HeaderCell>
                        <Table.HeaderCell
                            sorted={sort.field == ISegmentSortableFields.creation_time ? sort.direction : undefined}
                            onClick={this.handleSort(ISegmentSortableFields.creation_time)}>
                            Creado
                        </Table.HeaderCell>
                        <Table.HeaderCell>Canal</Table.HeaderCell>
                        <Table.HeaderCell>Acciones</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {segments.map((segment) => {
                        if (index < ((page - 1) * items_per_page)) {
                            index = index + 1;
                            return null;
                        } else if (index >= page * items_per_page) {
                            return null
                        }

                        index = index + 1;

                        return <Table.Row key={segment.id_segment}
                                          positive={this.state.id_segment == segment.id_segment}>
                            <Table.Cell>
                                <Button size={"tiny"} onClick={() => {
                                    this.setState({id_segment: segment.id_segment})

                                }} secondary={this.state.id_segment == segment.id_segment}>
                                    {segment.name}
                                </Button>
                            </Table.Cell>
                            <Table.Cell>{moment(segment.creation_time).fromNow()}</Table.Cell>
                            <Table.Cell>
                                {segment.channel_type}
                            </Table.Cell>
                            <Table.Cell>
                                <div>
                                    <Popup trigger={<Button
                                        attached='left'
                                        size={"tiny"}
                                        basic
                                    >
                                        {segment.campaign_count}
                                    </Button>} content={"Campañas usando este segmento"}/>

                                    <Popup trigger={<Button attached='right'
                                                            negative
                                                            icon
                                                            disabled={CxStore.isLoading(`cx_delete_segment.${segment.id_segment}`) || segment.campaign_count > 0}
                                                            onClick={() => {
                                                                rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                                                                    rootStore.routerStore.routerState.params,
                                                                    {
                                                                        ...rootStore.routerStore.routerState.queryParams,
                                                                        modal: "segment_delete_confirmation",
                                                                        modal_segment_id: segment.id_segment
                                                                    }
                                                                )
                                                            }}
                                                            size={"tiny"}>
                                        <Icon name='close'/>
                                    </Button>} content={"Eliminar"}/>


                                </div>
                            </Table.Cell>
                        </Table.Row>
                    })}

                </Table.Body>
            </Table>
            <Grid columns='equal' style={{paddingTop: 16}}>
                <Grid.Column floated='left'>
                    Mostrando
                    del {(page - 1) * items_per_page} al {page * items_per_page} de {segments.length} ítems
                </Grid.Column>
                <Grid.Column floated='right' width={8} textAlign="right">
                    {total_pages > 0 ?

                        <Pagination onPageChange={(event, data) => {
                            this.setState(
                                {page: data.activePage}
                            );
                        }}
                                    defaultActivePage={page}
                                    ellipsisItem={{content: <Icon name='ellipsis horizontal'/>, icon: true}}
                                    firstItem={{content: <Icon name='angle double left'/>, icon: true}}
                                    lastItem={{content: <Icon name='angle double right'/>, icon: true}}
                                    prevItem={{content: <Icon name='angle left'/>, icon: true}}
                                    nextItem={{content: <Icon name='angle right'/>, icon: true}}
                                    totalPages={total_pages}
                        /> : null
                    }
                </Grid.Column>
            </Grid>
            {(picked_segment && picked_segment.project && picked_segment.project.id_project == this.props.project.id_project) ?
                <Segment basic>
                    <Header as={"h4"}>Descripción del segmento</Header>
                    <p>
                        {CxStore.segments.get(this.state.id_segment)!.desc}
                    </p>
                </Segment> : null}
        </Segment>

    }
}

@observer
export class Projects extends React.Component<any, any> {
    state = {project_form_open: false, project_name: "", project_desc: "", project_from_address: "test@artu.cl"};
    handleSort = (field: IProjectSortableFields) => {
        return () => {
            const queryParams: any = rootStore.routerStore.routerState.queryParams;
            rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                rootStore.routerStore.routerState.params,
                {
                    ...queryParams,
                    project_sort_direction: queryParams.project_sort_direction == ISortDirection.ascending ? ISortDirection.descending : ISortDirection.ascending,
                    project_sort_field: field
                })
        };

    };

    render() {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;
        let sort = {direction: ISortDirection.descending, field: IProjectSortableFields.creation_date};

        if (queryParams.project_sort_direction) {
            sort.direction = queryParams.project_sort_direction
        }
        if (queryParams.project_sort_field) {
            sort.field = queryParams.project_sort_field
        }
        let projects: ICxProject[] = CxStore.get_projects(sort);


        const items_per_page = 7;
        const total_pages = Math.ceil(projects.length / items_per_page);
        const page: number = parseInt(queryParams.page || 1);

        let index = 0;

        return (
            <Container fluid>
                <ProjectDeleteConfirmModal/>
                <SegmentDeleteConfirmModal/>
                <Segment stacked padded={"very"}>
                    <div className={"table-header"}>
                        <div className={"table-header-left"}>

                            <Header as="h3">
                                Proyectos
                                <Loader size={"tiny"}
                                        active={CxStore.isLoading("cx_list_projects")}
                                        inline/>
                            </Header>
                        </div>
                        <div className={"table-header-right"}>
                            <Popup trigger={<Button icon primary onClick={() => {
                                CxStore.cx_list_projects(true)
                            }}>
                                <Icon name='undo'/>
                            </Button>} content='Actualizar'/>
                            <Popup trigger={<Button icon primary={!this.state.project_form_open}
                                                    negative={this.state.project_form_open} onClick={() => {
                                this.setState({project_form_open: !this.state.project_form_open});
                            }}>
                                {this.state.project_form_open ? <Icon name='minus'/> : <Icon name='plus'/>}
                            </Button>} content='Agregar proyecto'/>
                        </div>
                    </div>
                    {this.state.project_form_open ? <Segment basic loading={CxStore.isLoading("cx_create_project")}>
                        <Form onSubmit={() => {
                            CxStore.cx_create_project(this.state.project_name, this.state.project_desc, this.state.project_from_address).then(() => {
                                toast(<div>Proyecto <Label>{this.state.project_name}</Label> creado</div>);
                                this.setState({
                                    project_form_open: false,
                                    project_desc: "",
                                    project_from_address: "test@artu.cl",
                                    project_name: ""
                                })
                            })
                        }}>
                            <Form.Group widths={2}>
                                <Form.Input label='Nombre' placeholder='Nombre' onChange={(e: any, data: any) => {
                                    this.setState({project_name: data.value})
                                }} value={this.state.project_name} required/>
                                <Form.Input label='Remitente' placeholder='de@ejemplo.com' type="email"
                                            onChange={(e: any, data: any) => {
                                                this.setState({project_from_address: data.value})
                                            }} value={this.state.project_from_address} required/>
                            </Form.Group>
                            <div className={"field"}>
                                <label>Descripción</label>
                                <TextArea autoHeight placeholder='Descripción' onChange={(e: any, data: any) => {
                                    this.setState({project_desc: data.value})
                                }} value={this.state.project_desc}/>
                            </div>
                            <Button secondary>Crear</Button>
                        </Form>
                    </Segment> : null}
                    <Table celled selectable sortable>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell
                                    sorted={sort.field == IProjectSortableFields.name ? sort.direction : undefined}
                                    onClick={this.handleSort(IProjectSortableFields.name)}>
                                    Nombre
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={sort.field == IProjectSortableFields.creation_date ? sort.direction : undefined}
                                    onClick={this.handleSort(IProjectSortableFields.creation_date)}>
                                    Creado
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={sort.field == IProjectSortableFields.from_address ? sort.direction : undefined}
                                    onClick={this.handleSort(IProjectSortableFields.from_address)}>
                                    Remitente
                                </Table.HeaderCell>
                                <Table.HeaderCell collapsing>Acciones</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {projects.map((project) => {
                                if (index < ((page - 1) * items_per_page)) {
                                    index = index + 1;
                                    return null;
                                } else if (index >= page * items_per_page) {
                                    return null
                                }

                                index = index + 1;

                                return <Table.Row key={project.id_project}
                                                  positive={queryParams.id_project == project.id_project}>
                                    <Table.Cell>
                                        <Button size={"tiny"} onClick={() => {
                                            rootStore.routerStore.goTo("proyectos", {}, {
                                                page: page,
                                                id_project: project.id_project
                                            });

                                        }} secondary={queryParams.id_project == project.id_project}>
                                            {project.name}
                                        </Button>
                                    </Table.Cell>
                                    <Table.Cell>{moment(project.creation_date).fromNow()}</Table.Cell>
                                    <Table.Cell>
                                        {project.from_address}
                                    </Table.Cell>
                                    <Table.Cell>
                                        <div>
                                            <Popup trigger={<Button
                                                icon
                                                negative
                                                onClick={() => {
                                                    rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                                                        rootStore.routerStore.routerState.params,
                                                        {
                                                            ...rootStore.routerStore.routerState.queryParams,
                                                            modal: "project_delete_confirmation",
                                                            modal_project_id: project.id_project
                                                        }
                                                    )

                                                }}
                                                size={"mini"} disabled={project.segment_count > 0}>
                                                <Loader size={"tiny"}
                                                        active={CxStore.isLoading(`cx_delete_project.${project.id_project}`)}
                                                        inline/>
                                                <Icon name='close'/>
                                            </Button>} content={"Eliminar"}/>
                                        </div>

                                    </Table.Cell>
                                </Table.Row>
                            })}

                        </Table.Body>

                    </Table>
                    <Grid columns='equal' style={{paddingTop: 16}}>
                        <Grid.Column floated='left'>
                            Mostrando
                            del {(page - 1) * items_per_page} al {page * items_per_page} de {projects.length} ítems
                        </Grid.Column>
                        <Grid.Column floated='right' width={8} textAlign="right">
                            {total_pages > 0 ?

                                <Pagination onPageChange={(event, data) => {
                                    rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                                        rootStore.routerStore.routerState.params,
                                        {
                                            ...rootStore.routerStore.routerState.queryParams,
                                            page: data.activePage,
                                            id_project: undefined
                                        });
                                }}
                                            defaultActivePage={page}
                                            ellipsisItem={{
                                                content: <Icon name='ellipsis horizontal'/>,
                                                icon: true
                                            }}
                                            firstItem={{
                                                content: <Icon name='angle double left'/>,
                                                icon: true
                                            }}
                                            lastItem={{
                                                content: <Icon name='angle double right'/>,
                                                icon: true
                                            }}
                                            prevItem={{
                                                content: <Icon name='angle left'/>,
                                                icon: true
                                            }}
                                            nextItem={{
                                                content: <Icon name='angle right'/>,
                                                icon: true
                                            }}
                                            totalPages={total_pages}
                                /> : null
                            }
                        </Grid.Column>
                    </Grid>

                </Segment>
                {CxStore.projects.get(queryParams.id_project) ?
                    <SegmentList project={CxStore.projects.get(queryParams.id_project)}/> : null}

            </Container>
        )
    }

}


