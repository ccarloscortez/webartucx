import * as React from "react";
import {observer} from "mobx-react";
import {
    Label,
    Menu,
    Dropdown,
    Form,
    Segment,
    Header,
    Table,
    Container,
    Grid,
    Icon,
    Pagination,
    Button,
    TextArea,
    Loader,
    Popup,
    Radio,
    Modal, Checkbox,
} from 'semantic-ui-react'
import {toast} from 'react-toastify';

import {
    CxStore,
    ICampaignSortableFields,
    IClientModelSortableFields,
    ICxCampaign,
    ISortDirection, PostmarkEventKind, ICxSmsCampaign
} from "../backend/models";
import * as moment from 'moment';
import {rootStore} from "../backend/stores";
import {CXApi} from "../backend/api";
import {removeModal} from "../utils";

interface ISchedule {

    hour: string,
    minutes: string,
    day_of_month: string,
    month: string,
    day_of_week: string,
    year: string

}

function schedule_expression(ob: ISchedule) {
    let d = new Date();
    let diff = (d.getTimezoneOffset() / 60);
    let hour = parseInt(ob.hour);
    hour = hour + diff;
    let day_off = false;
    if (hour >= 24) {
        hour = hour - 24;
        day_off = true
    }
    let text_hour = `${hour}`;
    if (hour < 10) text_hour = `0${hour}`;
    let day_of_week = ob.day_of_week;

    if (day_off) {
        let dw_final = [];
        let d_w = ob.day_of_week.split(",");
        for (let day of d_w) {
            let cd = (parseInt(day) || 1) + 1;
            if (cd > 7) cd = 1;
            dw_final.push(cd)
        }
        day_of_week = dw_final.sort().join(",").replace(/^,/, '');
    }
    return `cron(${ob.minutes} ${text_hour} ${ob.day_of_month} ${ob.month} ${day_of_week} ${ob.year})`
}

const day_options = [
    {key: 'monday', text: 'Lunes', value: '2'},
    {key: 'tuesday', text: 'Martes', value: '3'},
    {key: 'wednesday', text: 'Miércoles', value: '4'},
    {key: 'thursday', text: 'Jueves', value: '5'},
    {key: 'friday', text: 'Viernes', value: '6'},
    {key: 'saturday', text: 'Sábado', value: '7'},
    {key: 'sunday', text: 'Domingo', value: '1'},
];

const hours_options = function () {
    let ret = [];
    for (let i in Array.from(Array(24).keys())) {
        let index = parseInt(i);
        let hour: any = index;
        if (hour < 10) {
            hour = `0${hour}`
        }
        let time = `${hour}`;
        ret.push(
            {
                key: time,
                text: time,
                value: time
            })
    }
    return ret;
}();

const minutes_options = function () {
    let ret = [];
    for (let i in Array.from(Array(60).keys())) {
        let index = parseInt(i);
        let minute: any = index;
        if (minute < 10) {
            minute = `0${minute}`
        }
        let time = `${minute}`;
        ret.push(
            {
                key: time,
                text: time,
                value: time
            })
    }
    return ret;
}();

@observer
class CampaignDeleteConfirmModal extends React.Component<any, any> {
    close = () => {
        rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
            rootStore.routerStore.routerState.params,
            removeModal(rootStore.routerStore.routerState.queryParams)
        )
    };

    remove = () => {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;
        let campaign_id = queryParams.modal_campaign_id;
        let campaign = CxStore.campaigns.get(campaign_id);
        let sms_campaign = CxStore.sms_campaigns.get(campaign_id);
        if (campaign) {
            let cn = campaign.name;
            CxStore.cx_delete_campaign(campaign.id_campaign).then(() => {
                toast.error(<div>Campaña <Label>{cn}</Label> eliminada
                </div>);
            });
        } else if (sms_campaign) {
            let cn = sms_campaign.name;
            CxStore.cx_delete_smscampaign(sms_campaign.id_smscampaign).then(() => {
                toast.error(<div>Campaña <Label>{cn}</Label> eliminada
                </div>);
            });
        } else {
            return null
        }
        this.close()
    };

    render() {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;
        let campaign_id = queryParams.modal_campaign_id;
        let campaign = CxStore.campaigns.get(campaign_id);
        let sms_campaign = CxStore.sms_campaigns.get(campaign_id);
        if (!campaign && !sms_campaign) {
            return null
        }
        return <Modal open={queryParams.modal == "campaign_delete_confirmation"}
                      closeOnEscape
                      size={'tiny'}
                      closeOnDimmerClick
                      onClose={this.close}>
            <Modal.Header>Eliminar campaña</Modal.Header>
            <Modal.Content>
                {campaign ? <p>Esta seguro que desea eliminar la campaña <Label>{campaign.name}</Label></p> : null}
                {sms_campaign ? <p>Esta seguro que desea eliminar la campaña <Label>{sms_campaign.name}</Label></p> : null}
            </Modal.Content>
            <Modal.Actions>
                <Button onClick={this.close} negative>
                    No
                </Button>
                <Button onClick={this.remove}
                        positive
                        labelPosition='right'
                        icon='checkmark'
                        content='Si'/>
            </Modal.Actions>
        </Modal>
    }
}

@observer
export class Campaigns extends React.Component<any, any> {
    state = {
        list_type: "both",
        form: {
            open: false,
            send_now: false,
            type: "email",
            id_project: "",
            id_segment: "",
            name: "",
            desc: "",
            frequency: "daily",
            start_time: new Date(),
            end_time: new Date(),
            schedule_expression: {
                hour: "00",
                minutes: "00",
                day_of_month: "?",
                month: "*",
                day_of_week: "",
                year: "*"
            },
            id_mailtemplate: "",
            id_smstemplate: "",
            email_images: {}
        }
    };

    selected_hour() {
        return this.state.form.schedule_expression.hour
    }

    selected_minute() {
        return this.state.form.schedule_expression.minutes
    }


    selected_days() {
        return this.state.form.schedule_expression.day_of_week.split(",")
    }

    get_hour_from_cron(cron: string) {
        let cs = cron.trim().replace("cron(", "").replace(")", "").split(" ");
        let hour = parseInt(cs[1]) || 0;
        let d = new Date();
        let diff = -(d.getTimezoneOffset() / 60);
        hour = hour + diff;
        if (hour < 0) {
            hour = 24 + hour
        }
        return `${hour}:${cs[0]}`
    }

    is_day_of(cron: string): boolean {
        let cs = cron.trim().replace("cron(", "").replace(")", "").split(" ");
        let hour = parseInt(cs[1]) || 0;
        let d = new Date();
        let diff = -(d.getTimezoneOffset() / 60);
        hour = hour + diff;
        if (hour < 0) {
            return true
        }
        return false;

    }

    change_hour = (evt: any, data: any) => {

        this.setState({
            form: {
                ...this.state.form,
                schedule_expression: {
                    ...this.state.form.schedule_expression,
                    hour: data.value,
                }
            }
        });
    };

    change_minutes = (evt: any, data: any) => {
        this.setState({
            form: {
                ...this.state.form,
                schedule_expression: {
                    ...this.state.form.schedule_expression,
                    minutes: data.value,
                }
            }
        });
    };

    change_day = (evt: any, data: any) => {
        this.setState({
            form: {
                ...this.state.form,
                schedule_expression: {
                    ...this.state.form.schedule_expression,
                    day_of_week: data.value.join(",").replace(/^,/, ''),
                }
            }
        });
    };

    get_days_map(cron: string):
        Map<number, boolean> {
        let ret = new Map<number, boolean>();
        ret.set(1, false);
        ret.set(2, false);
        ret.set(3, false);
        ret.set(4, false);
        ret.set(5, false);
        ret.set(6, false);
        ret.set(7, false);

        // Handle empty schedule string
        if (!cron) {
            console.warn('Cron string found to be empty!');
            return ret;
        }

        let cs = cron.trim().replace("cron(", "").replace(")", "").split(" ");
        if (cs[4] == "?" || cs[4] == "*") {
            cs[4] = "1,2,3,4,5,6,7"
        }
        for (const item of cs[4].split(",")) {
            if (this.is_day_of(cron)) {
                let day = (parseInt(item) || 1) - 1;
                if (day == 0) day = 7;
                ret.set(day, true)

            } else {
                ret.set(parseInt(item), true)
            }
        }
        return ret
    }

    handleSort = (field: ICampaignSortableFields) => {
        return () => {
            const queryParams: any = rootStore.routerStore.routerState.queryParams;
            rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                rootStore.routerStore.routerState.params,
                {
                    ...queryParams,
                    campaign_sort_direction: queryParams.campaign_sort_direction == ISortDirection.ascending ? ISortDirection.descending : ISortDirection.ascending,
                    campaign_sort_field: field
                })
        };

    };

    render() {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;

        let sort = {direction: ISortDirection.descending, field: ICampaignSortableFields.creation_time};
        if (queryParams.campaign_sort_direction) {
            sort.direction = queryParams.campaign_sort_direction
        }
        if (queryParams.campaign_sort_field) {
            sort.field = queryParams.campaign_sort_field
        }
        let campaigns: ICxCampaign[] = [];
        let sms_campaigns: ICxSmsCampaign[] = [];
        const items_per_page = 5;
        let total_pages = 0;

        switch (this.state.list_type) {
            case "email":
                campaigns = CxStore.get_campaigns(sort);
                total_pages = Math.ceil(campaigns.length / items_per_page);
                break;
            case "sms":
                sms_campaigns = CxStore.get_smscampaigns(sort);
                total_pages = Math.ceil(sms_campaigns.length / items_per_page);
                break;
            default:
            case "both":
                campaigns = CxStore.get_campaigns(sort);
                sms_campaigns = CxStore.get_smscampaigns(sort);
                total_pages = Math.ceil((campaigns.length + sms_campaigns.length) / items_per_page);
                break;
        }

        const page: number = parseInt(queryParams.page || 1);
        let campaign = CxStore.campaigns.get(queryParams.id_campaign);
        let sms_campaign = CxStore.sms_campaigns.get(queryParams.id_campaign);
        let index = 0;

        return (
            <Container fluid>
                <CampaignDeleteConfirmModal/>

                <Segment stacked padded={"very"}>
                    <div className={"table-header"}>
                        <div className={"table-header-left"}>
                            <Header as="h3">
                                Campañas
                                <Loader size={"tiny"}
                                        active={CxStore.isLoading("cx_list_campaigns")}
                                        inline/>
                            </Header>
                        </div>
                        <div className={"table-header-right"}>
                            <Popup trigger={<Button icon primary onClick={() => {
                                CxStore.cx_list_campaigns( true)
                                CxStore.cx_list_smscampaigns( true)
                            }}>
                                <Icon name='undo'/>
                            </Button>} content='Actualizar'/>
                            <Popup trigger={<Button icon primary={!this.state.form.open}
                                                    negative={this.state.form.open} onClick={() => {
                                if (!this.state.form.open) {
                                    CxStore.cx_list_projects(false);
                                    CxStore.cx_list_templates(false);
                                    CxStore.cx_list_smstemplates(false);
                                }
                                this.setState({
                                    form: {
                                        ...this.state.form,
                                        open: !this.state.form.open
                                    }
                                });
                            }}>
                                {this.state.form.open ? <Icon name='minus'/> : <Icon name='plus'/>}
                            </Button>} content='Agregar campaña'/>
                        </div>
                    </div>

                    {this.state.form.open ? <Segment basic
                                                     loading={CxStore.isLoading("cx_list_templates") || CxStore.isLoading("cx_create_campaign")|| CxStore.isLoading("cx_send_mailcampaign")  || CxStore.isLoading("cx_list_projects")}>
                        <Form onSubmit={() => {
                            let pr;
                            if (this.state.form.type == "email") {
                                if (this.state.form.send_now) {
                                    pr = CxStore.cx_send_mailcampaign({
                                        id_segment: this.state.form.id_segment,
                                        name: this.state.form.name,
                                        desc: this.state.form.desc,
                                        frequency: this.state.form.frequency,
                                        start_time: moment(this.state.form.start_time).format('YYYY-M-D h:mm:ss'),
                                        end_time: moment(this.state.form.end_time).format('YYYY-M-D h:mm:ss'),
                                        schedule_expression: schedule_expression(this.state.form.schedule_expression),
                                        id_mailtemplate: this.state.form.id_mailtemplate,
                                        email_images: this.state.form.email_images
                                    })
                                } else {
                                    pr = CxStore.cx_create_campaign({
                                        id_segment: this.state.form.id_segment,
                                        name: this.state.form.name,
                                        desc: this.state.form.desc,
                                        frequency: this.state.form.frequency,
                                        start_time: moment(this.state.form.start_time).format('YYYY-M-D h:mm:ss'),
                                        end_time: moment(this.state.form.end_time).format('YYYY-M-D h:mm:ss'),
                                        schedule_expression: schedule_expression(this.state.form.schedule_expression),
                                        id_mailtemplate: this.state.form.id_mailtemplate,
                                        email_images: this.state.form.email_images
                                    })
                                }

                            } else {
                                if (this.state.form.send_now) {
                                    pr = CXApi.campaigns.cx_send_sms_now(
                                        this.state.form.id_segment,
                                        this.state.form.name,
                                        this.state.form.desc,
                                        this.state.form.id_smstemplate
                                    )
                                } else {
                                    pr = CXApi.campaigns.cx_schedule_smscampaign(
                                        this.state.form.id_segment,
                                        this.state.form.name,
                                        this.state.form.desc,
                                        schedule_expression(this.state.form.schedule_expression),
                                        this.state.form.id_smstemplate
                                    )
                                }
                            }
                            pr.then(() => {
                                toast(<div>Campaña <Label>{this.state.form.name}</Label> creada</div>);
                                this.setState({
                                    form: {
                                        open: false,
                                        send_now: false,
                                        type: "email",
                                        id_project: "",
                                        id_segment: "",
                                        name: "",
                                        desc: "",
                                        frequency: "daily",
                                        start_time: new Date(),
                                        end_time: new Date(),
                                        schedule_expression: {
                                            hour: "00",
                                            minutes: "00",
                                            day_of_month: "?",
                                            month: "*",
                                            day_of_week: "",
                                            year: "*"
                                        },
                                        id_smstemplate: "",
                                        id_mailtemplate: "",
                                        email_images: {}
                                    }
                                })
                            })
                        }}>
                            <Form.Group widths={2}>
                                <Form.Input label='Nombre' placeholder='Nombre' onChange={(e: any, data: any) => {
                                    this.setState({
                                        form: {
                                            ...this.state.form,
                                            name: data.value
                                        }
                                    })
                                }} value={this.state.form.name} required/>
                                <Form.Select label='Proyecto'
                                             options={CxStore.get_projects_option()}
                                             placeholder='Proyecto'
                                             onChange={(e: any, data: any) => {
                                                 CxStore.cx_list_segments(data.value, false);
                                                 this.setState({
                                                     form: {
                                                         ...this.state.form,
                                                         id_project: data.value,
                                                     }
                                                 })
                                             }}
                                             value={this.state.form.id_project}
                                             required/>
                                <Form.Select label='Segmento'
                                             options={CxStore.get_segments_option(this.state.form.id_project)}
                                             placeholder='Segmento'
                                             loading={CxStore.isLoading("cx_list_segments")}
                                             onChange={(e: any, data: any) => {
                                                 this.setState({
                                                     form: {
                                                         ...this.state.form,
                                                         id_segment: data.value,
                                                     }
                                                 })
                                             }}
                                             value={this.state.form.id_segment}
                                             required/>
                            </Form.Group>
                            <Form.Group widths={2}>
                                <Form.Select label='Tipo'
                                             options={[
                                                 {
                                                     key: "email",
                                                     value: "email",
                                                     text: "Email"
                                                 }, {
                                                     key: "sms",
                                                     value: "sms",
                                                     text: "SMS"
                                                 }]}
                                             placeholder='Tipo'
                                             onChange={(e: any, data: any) => {
                                                 this.setState({
                                                     form: {
                                                         ...this.state.form,
                                                         type: data.value,
                                                     }
                                                 })
                                             }}
                                             value={this.state.form.type}
                                             required/>

                                <Form.Field style={{display: "flex", alignItems: "center", marginTop: 16}}>
                                    <Checkbox label='Enviar ahora mismo' checked={this.state.form.send_now}
                                              onChange={(event: any, data: any) => {
                                                  this.setState({
                                                      form: {
                                                          ...this.state.form,
                                                          send_now: data.checked,
                                                          schedule_expression: {
                                                              hour: "00",
                                                              minutes: "00",
                                                              day_of_month: "?",
                                                              month: "*",
                                                              day_of_week: "",
                                                              year: "*"
                                                          },
                                                      }
                                                  })
                                              }}/>
                                </Form.Field>

                            </Form.Group>
                            <div className={"field"}>
                                <label>Descripción</label>
                                <TextArea autoHeight placeholder='Descripción' onChange={(e: any, data: any) => {
                                    this.setState({
                                        form: {
                                            ...this.state.form,
                                            desc: data.value,
                                        }
                                    })
                                }} value={this.state.form.desc}/>
                            </div>
                            {this.state.form.send_now ? null :
                                <Form.Group widths={2}>
                                    <div className={"field required"}>
                                        <label>Dias</label>
                                        <Dropdown placeholder='Dias'
                                                  fluid
                                                  multiple
                                                  selection
                                                  options={day_options}
                                                  onChange={this.change_day}
                                                  value={this.selected_days()}
                                        />
                                    </div>
                                    <div className={"field"}>
                                        <label>Hora</label>
                                        <Dropdown placeholder='Hora'
                                                  fluid
                                                  selection
                                                  options={hours_options}
                                                  onChange={this.change_hour}
                                                  value={this.selected_hour()}/>
                                    </div>
                                    <div className={"field"}>
                                        <label>Minutos</label>
                                        <Dropdown placeholder='Minutos'
                                                  fluid
                                                  selection
                                                  options={minutes_options}
                                                  onChange={this.change_minutes}
                                                  value={this.selected_minute()}/>
                                    </div>
                                </Form.Group>}
                            <Form.Group widths={2}>
                                {this.state.form.type == "sms" ?
                                    <Form.Select label='Template'
                                        options={CxStore.get_sms_template_option()}
                                        placeholder='Template'
                                        onChange={(e: any, data: any) => {
                                            this.setState({
                                                form: {
                                                    ...this.state.form,
                                                    id_smstemplate: data.value,
                                                }
                                            })
                                        }}
                                        value={this.state.form.id_smstemplate}
                                        required
                                    /> :
                                    <Form.Select label='Template'
                                        options={CxStore.get_template_option()}
                                        placeholder='Template'
                                        onChange={(e: any, data: any) => {
                                            this.setState({
                                                form: {
                                                    ...this.state.form,
                                                    id_mailtemplate: data.value,
                                                }
                                            })
                                        }}
                                        value={this.state.form.id_mailtemplate}
                                        required
                                    />
                                }

                            </Form.Group>
                            <Button secondary>Crear</Button>
                        </Form>
                    </Segment> : null}

                    <Table celled selectable sortable>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell
                                    sorted={sort.field == ICampaignSortableFields.name ? sort.direction : undefined}
                                    onClick={this.handleSort(ICampaignSortableFields.name)}>
                                    Nombre
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={sort.field == ICampaignSortableFields.creation_time ? sort.direction : undefined}
                                    onClick={this.handleSort(ICampaignSortableFields.creation_time)}>
                                    Creado
                                </Table.HeaderCell>
                                <Table.HeaderCell style={{width: 288}}>Programación</Table.HeaderCell>
                                <Table.HeaderCell collapsing>Activa</Table.HeaderCell>
                                <Table.HeaderCell collapsing>Acciones</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {campaigns.map((campaign) => {

                                if (index < ((page - 1) * items_per_page)) {
                                    index = index + 1;
                                    return null;
                                } else if (index >= page * items_per_page) {
                                    return null
                                }

                                index = index + 1;
                                let schedule_days: Map<number, boolean> = this.get_days_map(campaign.schedule_expression);
                                return <Table.Row key={campaign.id_campaign}
                                                  positive={queryParams.id_campaign == campaign.id_campaign}
                                                  warning={CxStore.isLoading(`cx_campaign_state.${campaign.id_campaign}`)}
                                                  negative={CxStore.isLoading(`cx_delete_campaign.${campaign.id_campaign}`)}>
                                    <Table.Cell>
                                        <Button size={"tiny"} onClick={() => {
                                            rootStore.routerStore.goTo("campaigns", {}, {
                                                page: page,
                                                id_campaign: campaign.id_campaign
                                            });

                                        }} secondary={queryParams.campaigns == campaign.id_campaign}>
                                            {campaign.name}
                                        </Button>
                                        <Loader size={"tiny"}
                                                active={CxStore.isLoading(`cx_delete_campaign.${campaign.id_campaign}`) || CxStore.isLoading(`cx_campaign_state.${campaign.id_campaign}`)}
                                                inline/>
                                    </Table.Cell>
                                    <Table.Cell>{moment(campaign.creation_time!).fromNow()}</Table.Cell>
                                    <Table.Cell>
                                        <Label.Group circular>
                                            <Popup trigger={<Label
                                                color={schedule_days.get(2) ? "green" : "grey"}>L</Label>}
                                                   content={"Lunes"}/>
                                            <Popup trigger={<Label
                                                color={schedule_days.get(3) ? "green" : "grey"}>M</Label>}
                                                   content={"Martes"}/>
                                            <Popup trigger={<Label
                                                color={schedule_days.get(4) ? "green" : "grey"}>W</Label>}
                                                   content={"Miércoles"}/>
                                            <Popup trigger={<Label
                                                color={schedule_days.get(5) ? "green" : "grey"}>J</Label>}
                                                   content={"Jueves"}/>
                                            <Popup trigger={<Label
                                                color={schedule_days.get(6) ? "green" : "grey"}>V</Label>}
                                                   content={"Viernes"}/>
                                            <Popup trigger={<Label
                                                color={schedule_days.get(7) ? "green" : "grey"}>S</Label>}
                                                   content={"Sabado"}/>
                                            <Popup trigger={<Label
                                                color={schedule_days.get(1) ? "green" : "grey"}>D</Label>}
                                                   content={"Domingo"}/>
                                            &nbsp;&nbsp;
                                            {this.get_hour_from_cron(campaign.schedule_expression)}
                                        </Label.Group>
                                    </Table.Cell>

                                    <Table.Cell>
                                        <div style={{alignItems: "center", display: "flex"}}>
                                            <Radio
                                                disabled={CxStore.isLoading(`cx_campaign_state.${campaign.id_campaign}`)}
                                                toggle
                                                checked={campaign.state}
                                                onChange={() => {
                                                    CxStore.cx_campaign_state(campaign.id_campaign)
                                                }
                                                }/>
                                        </div>
                                    </Table.Cell>

                                    <Table.Cell textAlign={"center"}>

                                        <Dropdown icon='ellipsis horizontal'
                                                  className={"grey-icon"}
                                                  direction={"left"}>
                                            <Dropdown.Menu>
                                                <Dropdown.Item onClick={() => {
                                                    rootStore.routerStore.goTo("events", {}, {id_campaign: campaign.id_campaign})
                                                }
                                                }>
                                                    <Icon name='list alternate outline'/>
                                                    <span className='text'>Eventos</span>
                                                </Dropdown.Item>
                                                <Dropdown.Item onClick={() => {
                                                    rootStore.routerStore.goTo("analitica_por_campagna", {}, {id_campaign: campaign.id_campaign})
                                                }
                                                }>
                                                    <Icon name='chart bar'/>
                                                    <span className='text'>Análisis de negocio</span>
                                                </Dropdown.Item>
                                                <Dropdown.Item onClick={() => {
                                                    rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                                                        rootStore.routerStore.routerState.params,
                                                        {
                                                            ...rootStore.routerStore.routerState.queryParams,
                                                            modal: "campaign_delete_confirmation",
                                                            modal_campaign_id: campaign.id_campaign
                                                        }
                                                    )
                                                }}>
                                                    <Icon name='close' color={"red"}/>
                                                    <span className='text'>Eliminar</span>
                                                </Dropdown.Item>
                                            </Dropdown.Menu>
                                        </Dropdown>


                                    </Table.Cell>
                                </Table.Row>
                            })}
                            {sms_campaigns.map((campaign) => {
                                if (index < ((page - 1) * items_per_page)) {
                                    index = index + 1;
                                    return null;
                                } else if (index >= page * items_per_page) {
                                    return null
                                }

                                index = index + 1;
                                let schedule_days: Map<number, boolean> = this.get_days_map(campaign.schedule_expression);
                                return <Table.Row key={campaign.id_smscampaign}
                                                positive={queryParams.id_campaign == campaign.id_smscampaign}
                                                warning={CxStore.isLoading(`cx_campaign_state.${campaign.id_smscampaign}`)}
                                                negative={CxStore.isLoading(`cx_delete_smscampaign.${campaign.id_smscampaign}`)}>
                                    <Table.Cell>
                                        <Button size={"tiny"} onClick={() => {
                                            rootStore.routerStore.goTo("campaigns", {}, {
                                                page: page,
                                                id_campaign: campaign.id_smscampaign
                                            });

                                        }} secondary={queryParams.campaigns == campaign.id_smscampaign}>
                                            {campaign.name}
                                        </Button>
                                        <Loader size={"tiny"}
                                                active={CxStore.isLoading(`cx_delete_smscampaign.${campaign.id_smscampaign}`) || CxStore.isLoading(`cx_campaign_state.${campaign.id_smscampaign}`)}
                                                inline/>
                                    </Table.Cell>
                                    <Table.Cell>{moment(campaign.creation_time!).fromNow()}</Table.Cell>
                                    <Table.Cell>
                                        <Label.Group circular>
                                            <Popup trigger={<Label
                                                color={schedule_days.get(2) ? "green" : "grey"}>L</Label>}
                                                content={"Lunes"}/>
                                            <Popup trigger={<Label
                                                color={schedule_days.get(3) ? "green" : "grey"}>M</Label>}
                                                content={"Martes"}/>
                                            <Popup trigger={<Label
                                                color={schedule_days.get(4) ? "green" : "grey"}>W</Label>}
                                                content={"Miércoles"}/>
                                            <Popup trigger={<Label
                                                color={schedule_days.get(5) ? "green" : "grey"}>J</Label>}
                                                content={"Jueves"}/>
                                            <Popup trigger={<Label
                                                color={schedule_days.get(6) ? "green" : "grey"}>V</Label>}
                                                content={"Viernes"}/>
                                            <Popup trigger={<Label
                                                color={schedule_days.get(7) ? "green" : "grey"}>S</Label>}
                                                content={"Sabado"}/>
                                            <Popup trigger={<Label
                                                color={schedule_days.get(1) ? "green" : "grey"}>D</Label>}
                                                content={"Domingo"}/>
                                            &nbsp;&nbsp;
                                            {this.get_hour_from_cron(campaign.schedule_expression)}
                                        </Label.Group>
                                    </Table.Cell>

                                    <Table.Cell>
                                        <div style={{alignItems: "center", display: "flex"}}>
                                            <Radio
                                                disabled={CxStore.isLoading(`cx_campaign_state.${campaign.id_smscampaign}`)}
                                                toggle
                                                checked={campaign.state}
                                                onChange={() => {
                                                    CxStore.cx_smscampaign_state(campaign.id_smscampaign)
                                                }
                                                }/>
                                        </div>
                                    </Table.Cell>

                                    <Table.Cell textAlign={"center"}>

                                        <Dropdown icon='ellipsis horizontal'
                                                className={"grey-icon"}
                                                direction={"left"}>
                                            <Dropdown.Menu>
                                                <Dropdown.Item onClick={() => {
                                                    rootStore.routerStore.goTo("events", {}, {id_campaign: campaign.id_smscampaign})
                                                }
                                                }>
                                                    <Icon name='list alternate outline'/>
                                                    <span className='text'>Eventos</span>
                                                </Dropdown.Item>
                                                <Dropdown.Item onClick={() => {
                                                    rootStore.routerStore.goTo("analitica_por_campagna", {}, {id_campaign: campaign.id_smscampaign})
                                                }
                                                }>
                                                    <Icon name='chart bar'/>
                                                    <span className='text'>Análisis de negocio</span>
                                                </Dropdown.Item>
                                                <Dropdown.Item onClick={() => {
                                                    rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                                                        rootStore.routerStore.routerState.params,
                                                        {
                                                            ...rootStore.routerStore.routerState.queryParams,
                                                            modal: "campaign_delete_confirmation",
                                                            modal_campaign_id: campaign.id_smscampaign
                                                        }
                                                    )
                                                }}>
                                                    <Icon name='close' color={"red"}/>
                                                    <span className='text'>Eliminar</span>
                                                </Dropdown.Item>
                                            </Dropdown.Menu>
                                        </Dropdown>


                                    </Table.Cell>
                                </Table.Row>
                                })}
                        </Table.Body>
                    </Table>

                    <Grid columns='equal' style={{paddingTop: 16}}>
                        <Grid.Column floated='left'>
                            Mostrando
                            del {(page - 1) * items_per_page} al {page * items_per_page} de {total_pages} ítems
                        </Grid.Column>
                        <Grid.Column floated='right' width={8} textAlign="right">
                            {total_pages > 0 ?

                                <Pagination onPageChange={(event, data) => {
                                    rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                                        rootStore.routerStore.routerState.params,
                                        {
                                            ...rootStore.routerStore.routerState.queryParams,
                                            page: data.activePage,
                                        });
                                }}
                                            defaultActivePage={page}
                                            ellipsisItem={{
                                                content: <Icon name='ellipsis horizontal'/>,
                                                icon: true
                                            }}
                                            firstItem={{
                                                content: <Icon name='angle double left'/>,
                                                icon: true
                                            }}
                                            lastItem={{
                                                content: <Icon name='angle double right'/>,
                                                icon: true
                                            }}
                                            prevItem={{
                                                content: <Icon name='angle left'/>,
                                                icon: true
                                            }}
                                            nextItem={{
                                                content: <Icon name='angle right'/>,
                                                icon: true
                                            }}
                                            totalPages={total_pages}
                                /> : null
                            }
                        </Grid.Column>
                    </Grid>
                </Segment>

                {campaign ? <Segment padded={"very"}
                                     loading={CxStore.isLoading(`cx_campaign_clicks.${campaign.id_campaign}`) ||
                                     CxStore.isLoading(`cx_campaign_opens.${campaign.id_campaign}`) ||
                                     CxStore.isLoading(`cx_campaign_delivery.${campaign.id_campaign}`) ||
                                     CxStore.isLoading(`cx_campaign_bounce.${campaign.id_campaign}`)}>
                    <Header as={"h4"}>Descripción</Header>
                    <p>{campaign.desc}</p>
                    <br/>
                    <Table collapsing striped>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Metrica</Table.HeaderCell>
                                <Table.HeaderCell>Valor</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            <Table.Row>
                                <Table.Cell>
                                    Enviados
                                </Table.Cell>
                                <Table.Cell>
                                    {CxStore.get_postmark_event(campaign.id_campaign,
                                        "",
                                        null,
                                        null,
                                        [PostmarkEventKind.delivery, PostmarkEventKind.bounce]).length}
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    Entregados
                                </Table.Cell>
                                <Table.Cell>
                                    {CxStore.get_postmark_event(campaign.id_campaign,
                                        "",
                                        null,
                                        null,
                                        [PostmarkEventKind.delivery]).length}
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    Abiertos
                                </Table.Cell>
                                <Table.Cell>
                                    {CxStore.get_postmark_event(campaign.id_campaign,
                                        "",
                                        null,
                                        null,
                                        [PostmarkEventKind.open]).length}
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    Porcentaje abiertos (%)
                                </Table.Cell>
                                <Table.Cell>
                                    {(CxStore.get_postmark_event(campaign.id_campaign,
                                        "",
                                        null,
                                        null, [PostmarkEventKind.delivery]).length > 0) ? (CxStore.get_postmark_event(campaign.id_campaign,
                                        "",
                                        null,
                                        null,
                                        [PostmarkEventKind.open]).length / CxStore.get_postmark_event(campaign.id_campaign,
                                        "",
                                        null,
                                        null, [PostmarkEventKind.delivery]).length * 100).toFixed(2) : "---"}
                                </Table.Cell>
                            </Table.Row>

                            <Table.Row>
                                <Table.Cell>
                                    Rebotados
                                </Table.Cell>
                                <Table.Cell>
                                    {CxStore.get_postmark_event(campaign.id_campaign,
                                        "",
                                        null,
                                        null,
                                        [PostmarkEventKind.bounce]).length}
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    Porcentaje de rebotados (%)
                                </Table.Cell>
                                <Table.Cell>
                                    {(CxStore.get_postmark_event(campaign.id_campaign,
                                        "",
                                        null,
                                        null, [PostmarkEventKind.delivery, PostmarkEventKind.bounce]).length > 0) ? (CxStore.get_postmark_event(campaign.id_campaign,
                                        "",
                                        null,
                                        null,
                                        [PostmarkEventKind.bounce]).length / CxStore.get_postmark_event(campaign.id_campaign,
                                        "",
                                        null,
                                        null, [PostmarkEventKind.delivery, PostmarkEventKind.bounce]).length * 100).toFixed(2) : "---"}
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    Clicks totales
                                </Table.Cell>
                                <Table.Cell>
                                    {CxStore.get_postmark_event(campaign.id_campaign,
                                        "",
                                        null,
                                        null,
                                        [PostmarkEventKind.click]).length}
                                </Table.Cell>
                            </Table.Row>
                        </Table.Body>
                    </Table>
                </Segment> : sms_campaign ? <Segment padded={"very"}
                                     loading={CxStore.isLoading(`cx_campaign_clicks.${sms_campaign.id_smscampaign}`) ||
                                     CxStore.isLoading(`cx_campaign_opens.${sms_campaign.id_smscampaign}`) ||
                                     CxStore.isLoading(`cx_campaign_delivery.${sms_campaign.id_smscampaign}`) ||
                                     CxStore.isLoading(`cx_campaign_bounce.${sms_campaign.id_smscampaign}`)}>
                    <Header as={"h4"}>Descripción</Header>
                    <p>{sms_campaign.desc}</p>
                    <br/>
                    <Table collapsing striped>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Metrica</Table.HeaderCell>
                                <Table.HeaderCell>Valor</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            <Table.Row>
                                <Table.Cell>
                                    Enviados
                                </Table.Cell>
                                <Table.Cell>
                                    {CxStore.get_postmark_event(sms_campaign.id_smscampaign,
                                        "",
                                        null,
                                        null,
                                        [PostmarkEventKind.delivery, PostmarkEventKind.bounce]).length}
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    Entregados
                                </Table.Cell>
                                <Table.Cell>
                                    {CxStore.get_postmark_event(sms_campaign.id_smscampaign,
                                        "",
                                        null,
                                        null,
                                        [PostmarkEventKind.delivery]).length}
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    Abiertos
                                </Table.Cell>
                                <Table.Cell>
                                    {CxStore.get_postmark_event(sms_campaign.id_smscampaign,
                                        "",
                                        null,
                                        null,
                                        [PostmarkEventKind.open]).length}
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    Porcentaje abiertos (%)
                                </Table.Cell>
                                <Table.Cell>
                                    {(CxStore.get_postmark_event(sms_campaign.id_smscampaign,
                                        "",
                                        null,
                                        null, [PostmarkEventKind.delivery]).length > 0) ? (CxStore.get_postmark_event(sms_campaign.id_smscampaign,
                                        "",
                                        null,
                                        null,
                                        [PostmarkEventKind.open]).length / CxStore.get_postmark_event(sms_campaign.id_smscampaign,
                                        "",
                                        null,
                                        null, [PostmarkEventKind.delivery]).length * 100).toFixed(2) : "---"}
                                </Table.Cell>
                            </Table.Row>

                            <Table.Row>
                                <Table.Cell>
                                    Rebotados
                                </Table.Cell>
                                <Table.Cell>
                                    {CxStore.get_postmark_event(sms_campaign.id_smscampaign,
                                        "",
                                        null,
                                        null,
                                        [PostmarkEventKind.bounce]).length}
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    Porcentaje de rebotados (%)
                                </Table.Cell>
                                <Table.Cell>
                                    {(CxStore.get_postmark_event(sms_campaign.id_smscampaign,
                                        "",
                                        null,
                                        null, [PostmarkEventKind.delivery, PostmarkEventKind.bounce]).length > 0) ? (CxStore.get_postmark_event(sms_campaign.id_smscampaign,
                                        "",
                                        null,
                                        null,
                                        [PostmarkEventKind.bounce]).length / CxStore.get_postmark_event(sms_campaign.id_smscampaign,
                                        "",
                                        null,
                                        null, [PostmarkEventKind.delivery, PostmarkEventKind.bounce]).length * 100).toFixed(2) : "---"}
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    Clicks totales
                                </Table.Cell>
                                <Table.Cell>
                                    {CxStore.get_postmark_event(sms_campaign.id_smscampaign,
                                        "",
                                        null,
                                        null,
                                        [PostmarkEventKind.click]).length}
                                </Table.Cell>
                            </Table.Row>
                        </Table.Body>
                    </Table>
                </Segment> : null}
            </Container>
        )
    }

}
