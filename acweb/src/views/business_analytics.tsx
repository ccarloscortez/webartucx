import * as React from "react";
import {observer} from "mobx-react";
import {
    Segment,
    Header,
    Label,
    Grid,
    Icon,
    Button,
    Container,
    Form
} from 'semantic-ui-react'
import * as moment from 'moment';
import {
    AreaChart,
    Area,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    BarChart,
    Bar,
    Radar,
    RadarChart,
    PolarGrid,
    Legend,
    PieChart,
    Pie,
    PolarAngleAxis,
    PolarRadiusAxis
} from 'recharts';

import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

@observer
export class TextCard extends React.Component <any, any> {


    render() {
        return (
            <Segment>
                <Header as="h5">{this.props.title}</Header>
                <Label color={"black"} size={"mini"}>SEMANA PASADA</Label>
                <Segment textAlign={"center"} basic style={{fontSize: 40}}>
                    {this.props.message}
                </Segment>
            </Segment>
        )
    }

}


@observer
export class TextCard2 extends React.Component <any, any> {


    render() {
        return (
            <Segment>
                <Header as="h5">{this.props.title}</Header>
                <Label style={{visibility: "hidden"}} color={"black"} size={"mini"}>SEMANA PASADA</Label>
                <Segment textAlign={"center"} basic style={{fontSize: 40}}>
                    {this.props.message}
                </Segment>
            </Segment>
        )
    }

}

const SimpleBarChart = (props: any) => {
    const data = [
        {name: 'Page A', uv: 4000, pv: 2400, amt: 2400},
        {name: 'Page B', uv: 3000, pv: 1398, amt: 2210},
        {name: 'Page C', uv: 2000, pv: 9800, amt: 2290},
        {name: 'Page D', uv: 2780, pv: 3908, amt: 2000},
        {name: 'Page E', uv: 1890, pv: 4800, amt: 2181},
        {name: 'Page F', uv: 2390, pv: 3800, amt: 2500},
        {name: 'Page G', uv: 3490, pv: 4300, amt: 2100},
    ];
    return (
        <BarChart width={600} height={300} data={data}
                  margin={{top: 5, right: 30, left: 20, bottom: 5}}>
            <CartesianGrid strokeDasharray="3 3"/>
            <XAxis dataKey="name"/>
            <YAxis/>
            <Tooltip/>
            <Legend/>
            <Bar dataKey="pv" fill="#8884d8"/>
            <Bar dataKey="uv" fill="#82ca9d"/>
        </BarChart>
    );

};


const TwoLevelRadarChart = (props: any) => {
    const data = [
        {subject: 'Math', A: 120, B: 110, fullMark: 150},
        {subject: 'Chinese', A: 98, B: 130, fullMark: 150},
        {subject: 'English', A: 86, B: 130, fullMark: 150},
        {subject: 'Geography', A: 99, B: 100, fullMark: 150},
        {subject: 'Physics', A: 85, B: 90, fullMark: 150},
        {subject: 'History', A: 65, B: 85, fullMark: 150},
    ];
    return (
        <RadarChart cx={300} cy={250} outerRadius={150} width={600} height={500} data={data}>
            <PolarGrid/>
            <PolarAngleAxis dataKey="subject"/>
            <PolarRadiusAxis angle={30} domain={[0, 150]}/>
            <Radar name="Mike" dataKey="A" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6}/>
            <Radar name="Lily" dataKey="B" stroke="#82ca9d" fill="#82ca9d" fillOpacity={0.6}/>
            <Legend/>

        </RadarChart>
    );
};


const TwoLevelPieChart = (props: any) => {
    const data01 = [{name: 'Group A', value: 400}, {name: 'Group B', value: 300},
        {name: 'Group C', value: 300}, {name: 'Group D', value: 200}]

    const data02 = [{name: 'A1', value: 100},
        {name: 'A2', value: 300},
        {name: 'B1', value: 100},
        {name: 'B2', value: 80},
        {name: 'B3', value: 40},
        {name: 'B4', value: 30},
        {name: 'B5', value: 50},
        {name: 'C1', value: 100},
        {name: 'C2', value: 200},
        {name: 'D1', value: 150},
        {name: 'D2', value: 50}]
    return (
        <PieChart width={600} height={300}>
            <Pie data={data01} cx={300} cy={150} outerRadius={60} fill="#8884d8"/>
            <Pie data={data02} cx={300} cy={150} innerRadius={70} outerRadius={90} fill="#82ca9d" label/>
        </PieChart>
    );
};

@observer
export class BusinessAnalytics extends React.Component <any, any> {

    state = {myDate: moment()};

    render() {
        return (
            <Container fluid>
                <Segment>
                    <Form>
                        <Form.Group>
                            <div className="field">
                                <label>Campaña</label>
                                <Form.Select options={[{a: "SDSd"}]}/>
                            </div>
                            <div className="field">
                                <label>Desde</label>
                                <DayPickerInput/>
                            </div>
                            <div className="field">
                                <label>Hasta</label>
                                <DayPickerInput/>
                            </div>
                            <div className="field">
                                <label style={{visibility: "hidden"}}>Hasta</label>

                                <Button icon labelPosition='left' primary size={"small"}>
                                    <Icon name='search'/>
                                    Buscar
                                </Button>
                            </div>

                        </Form.Group>
                    </Form>


                </Segment>
                <Grid columns={6} stackable>
                    <Grid.Row>
                        <Grid.Column>

                            <TextCard2 title={"Click Totales"} message={"500"}/>
                        </Grid.Column>
                        <Grid.Column>
                            <TextCard2 title={"Enviados"} message={"5000"}/>
                        </Grid.Column>
                        <Grid.Column>
                            <TextCard2 title={"Errores"} message={"30"}/>
                        </Grid.Column>
                        <Grid.Column>
                            <TextCard title={"Número de Interrupciones"} message={"927"}/>
                        </Grid.Column>
                        <Grid.Column>
                            <TextCard title={"Duración media"} message={"96.87s"}/>
                        </Grid.Column>
                        <Grid.Column>
                            <TextCard title={"Tiempo Medio de Respuesta"} message={"35ms"}/>
                        </Grid.Column>
                    </Grid.Row>

                </Grid>
                <Grid columns={2} stackable>
                    <Grid.Row>
                        <Grid.Column>
                            <Segment>
                                <Header>Interrupciones por segundo</Header>

                                <TwoLevelPieChart/>
                            </Segment>
                        </Grid.Column>
                        <Grid.Column>
                            <Segment>
                                <Header>Detectados como spam cada dia</Header>
                                <SimpleBarChart/>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Segment>
                                <Header>Palabras claves con mas exito</Header>
                                <TwoLevelRadarChart/>
                            </Segment>
                        </Grid.Column>
                        <Grid.Column>
                            <Segment>
                                <Header>Email abiertos segun la hora</Header>
                                <SimpleBarChart/>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>

        )
    }

}


