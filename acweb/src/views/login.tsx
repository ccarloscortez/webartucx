import * as React from "react";
import {observer} from "mobx-react";
import {Divider, Button, Form, Input, Icon, Grid, Header, Message, Segment} from 'semantic-ui-react'
import {CxStore} from "../backend/models";
import * as moment from 'moment';
import * as logo1 from '../assets/images/logo1.png';
import * as logo2 from '../assets/images/logo2.png';

@observer
export class LoginView extends React.Component <any, any> {
    saveUsername(e: any) {
        CxStore.login_form.setUsername(e.target.value);
    }

    savePassword(e: any) {
        CxStore.login_form.setPassword(e.target.value);
    }

    rememberMe(e: any, data: any) {
        CxStore.login_form.setRemember(data.checked);
    }

    render() {
        return (
            <div className="login-layout">
                <div className={"login-main-content"}>
                    <div className={"login-panel"}>
                        <div className={"login-logo"}>
                            <img src={logo1}/>
                            <img src={logo2}/>
                        </div>
                        <Segment raised padded='very' style={{maxWidth: 512}}>
                            <Divider horizontal><Header as="h3">Panel</Header></Divider>
                            <Form loading={CxStore.login_form.is_loading}>
                                <Form.Field required error={CxStore.login_form.error}>
                                    <label>Nombre de usuario</label>
                                    <Input iconPosition={"left"} icon="user circle" placeholder='Usuario'
                                           onChange={this.saveUsername} value={CxStore.login_form.username}/>
                                </Form.Field>
                                <Form.Field required error={CxStore.login_form.error}>
                                    <label>Contraseña</label>
                                    <Input type="password" iconPosition={"left"} icon="user secret"
                                           onChange={this.savePassword} value={CxStore.login_form.password}
                                           placeholder='Contraseña'/>
                                </Form.Field>
                                <Message
                                    visible={CxStore.login_form.error}
                                    error
                                    header='Credenciales Invalidas'
                                    content='Por favor ingresa un nombre de usuario y contraseña correctos.'
                                />
                                <Grid style={{marginTop: 32}}>
                                    <Grid.Column floated='left' width={5}>
                                        <Form.Checkbox label="Recordarme" onChange={this.rememberMe}
                                                       checked={CxStore.login_form.remember}/>
                                    </Grid.Column>
                                    <Grid.Column floated='right' textAlign="right" width={5}>
                                        <Button primary onClick={CxStore.login_form.login}>
                                            <Icon name="lock open"/>
                                            Entrar
                                        </Button>
                                    </Grid.Column>
                                </Grid>
                            </Form>

                        </Segment>
                    </div>


                </div>
                <div className="login-footer">
                    <Segment inverted basic>Copyright © {moment().year()} Artu CX.</Segment>
                </div>

            </div>

        )
    }

}


