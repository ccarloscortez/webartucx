import * as React from "react";
import {observer} from "mobx-react";
import {
    Segment,
    Header,
    Table,
    Label,
    Grid,
    Icon,
    Pagination,
    Button,
    Loader,
    Form,
    Container,
    Popup
} from 'semantic-ui-react'
import {
    CxStore,
    ICxPrediction,
    ICxTraining,
    IPredictionSortableFields,
    ISortDirection,
    ITrainingSortableFields
} from "../backend/models";
import * as moment from 'moment';
import {rootStore} from "../backend/stores";
import {
    PieChart,
    LineChart,
    CartesianGrid,
    Pie,
    XAxis,
    YAxis,
    Sector,
    Tooltip,
    Line,
    Legend,
    BarChart,
    Bar,
    Label as ReLabel
} from 'recharts';
import {toast} from "react-toastify";

class VariableImportance extends React.Component<{ importances: any }, any> {
    render() {
        let data = [];
        for (let key of Object.keys(this.props.importances)) {
            data.push({name: key, importancia: parseFloat(this.props.importances[key].toFixed(2))})
        }
        data.sort(function (a: any, b: any) {
            if (a["importancia"] > b["importancia"]) return -1;
            if (a["importancia"] < b["importancia"]) return 1;
            return 0;
        });

        return (
            <BarChart layout={"vertical"}
                      width={900}
                      height={300}
                      data={data}
                      margin={{top: 5, right: 30, left: 300, bottom: 5}}>
                <CartesianGrid strokeDasharray="3 3"/>
                <XAxis type={"number"}/>
                <YAxis dataKey="name" type="category"/>
                <Tooltip/>
                <Legend/>
                <Bar dataKey="importancia" fill="#8884d8"/>
            </BarChart>
        );
    }

}


class RocCurve extends React.Component<{ roc_curve: any }, any> {

    render() {
        let roc_data = [];
        const fpr: number[] = this.props.roc_curve.fpr;
        const tpr: number[] = this.props.roc_curve.tpr;

        for (let i = 0; i < fpr.length; i++) {
            roc_data.push({fpr: fpr[i], tpr: tpr[i]})
        }

        return (
            <LineChart width={600} height={300} margin={{top: 5, right: 30, left: 20, bottom: 5}}
                       style={{margin: "0 auto"}}>
                <CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="fpr" type="number" allowDuplicatedCategory={false}>
                    <ReLabel value="Tasa de falsos positivos" offset={0} position="insideBottom"/>
                </XAxis>
                <YAxis dataKey="tpr">
                    <ReLabel value="Tasa de verdaderos positivos" angle={-90} offset={0} position="insideLeft"
                             style={{textAnchor: 'middle'}}/>
                </YAxis>
                <Tooltip/>
                <Legend verticalAlign="top" height={36}/>
                <Line type="monotone" dataKey="tpr" data={roc_data}
                      name={`curva roc (area=${this.props.roc_curve.aoc.toFixed(2)})`} stroke="#FF4500"/>
                <Line type="monotone" dataKey="tpr" data={[{fpr: 0, tpr: 0}, {fpr: 1, tpr: 1}]} name={"al azar"}
                      stroke="#0000FF" strokeDasharray="3 4 5 2"/>
            </LineChart>
        );
    }
};

function as_indicator_name(id_prediction: string) {
    let on_dashboard = CxStore.predictions.get(id_prediction)!.on_dashboard;
    if (CxStore.isLoading(`cx_prediction_state.${id_prediction}`)) {
        if (on_dashboard) {
            return "Desactivando"
        } else {
            return "Activando"
        }
    } else {
        if (on_dashboard) {
            return "Activado";
        } else {
            return "Desactivado";
        }

    }

}

function as_indicator_color(id_prediction: string) {
    let on_dashboard = CxStore.predictions.get(id_prediction)!.on_dashboard;

    if (on_dashboard) {
        return "green"
    } else {
        return "red"
    }


}

const renderActiveShape = (props: any) => {
    const RADIAN = Math.PI / 180;
    const {
        cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
        fill, payload, percent, value
    } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? 'start' : 'end';

    return (
        <g>
            <text x={cx} y={cy} dy={8} textAnchor="middle"
                  fill={fill}>{payload.positive ? "Positivo" : "Negativo"}</text>
            <Sector
                cx={cx}
                cy={cy}
                innerRadius={innerRadius}
                outerRadius={outerRadius}
                startAngle={startAngle}
                endAngle={endAngle}
                fill={fill}
            />
            <Sector
                cx={cx}
                cy={cy}
                startAngle={startAngle}
                endAngle={endAngle}
                innerRadius={outerRadius + 6}
                outerRadius={outerRadius + 10}
                fill={fill}
            />
            <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none"/>
            <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none"/>
            <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor}
                  fill="#333">{`${payload.name} ${value}`}</text>
            <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
                {`(Porcentaje ${(percent * 100).toFixed(2)}%)`}
            </text>
        </g>
    );
};


const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

function model_options(id_model: string) {
    let model = CxStore.models.get(id_model);
    let ret: any[] = [];
    if (model && model.desc) {
        //clone desc
        let desc = JSON.parse(JSON.stringify(model.desc));
        delete  desc.title;
        delete  desc.type;
        let index = 0;
        for (let key of Object.keys(desc)) {
            index = index + 1;
            ret.push({key: index, value: key, text: key})
        }
    }

    return ret
}

@observer
export class Trainings extends React.Component <any, any> {
    state = {
        prediction_page: 1,
        prediction_items_per_page: 3,
        picked_prediction: null,
        activeIndex: 0,
        training_form: {
            id_model: "",
            name: "",
            type: "supervised",
            dataset_url_training: "",
            training_label: "",
            subtype: "classification",
            isOpen: false,
            uploading: false
        },
        prediction_form: {name: "", input: "", isOpen: false, uploading: false},
    };

    handleSortTrainings = (field: ITrainingSortableFields) => {
        return () => {
            const queryParams: any = rootStore.routerStore.routerState.queryParams;
            rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                rootStore.routerStore.routerState.params,
                {
                    ...queryParams,
                    training_sort_direction: queryParams.training_sort_direction == ISortDirection.ascending ? ISortDirection.descending : ISortDirection.ascending,
                    training_sort_field: field
                })
        };

    };


    handleSortPredictions = (field: IPredictionSortableFields) => {
        return () => {
            const queryParams: any = rootStore.routerStore.routerState.queryParams;
            rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                rootStore.routerStore.routerState.params,
                {
                    ...queryParams,
                    prediction_sort_direction: queryParams.prediction_sort_direction == ISortDirection.ascending ? ISortDirection.descending : ISortDirection.ascending,
                    prediction_sort_field: field
                })
        };
    };

    onPieEnter = (data: any, index: any) => {
        this.setState({
            activeIndex: index,
        });
    };

    upload_prediction = (e: any) => {
        console.log("called");//dont remove this line, there is a strange bugs when is removed you cant upload many images at the same time
        let file = e.target.files[0];
        let reader = new FileReader();
        this.setState({
            prediction_form: {
                ...this.state.prediction_form,
                uploading: true
            }
        });
        reader.onloadend = () => {
            this.setState({
                prediction_form: {
                    ...this.state.prediction_form,
                    uploading: false
                }
            });

            CxStore.cx_uploads3_predictinput("predictinput", "csv", reader.result.split(',')[1], (url: string) => {
                this.setState({
                    prediction_form: {
                        ...this.state.prediction_form,
                        input: url
                    }
                });
            });
        };


        reader.readAsDataURL(file);
    };

    upload_training = (e: any) => {
        console.log("called");//dont remove this line, there is a strange bugs when is removed you cant upload many images at the same time
        let file = e.target.files[0];
        let reader = new FileReader();
        this.setState({
            training_form: {
                ...this.state.training_form,
                uploading: true
            }
        });
        reader.onloadend = () => {
            this.setState({
                training_form: {
                    ...this.state.training_form,
                    uploading: false
                }
            });

            CxStore.cx_uploads3_traininput("traininginput", "csv", reader.result.split(',')[1], (url: string) => {
                this.setState({
                    training_form: {
                        ...this.state.training_form,
                        dataset_url_training: url
                    }
                });
            });
        };


        reader.readAsDataURL(file);
    };

    render() {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;

        let prediction_sort = {direction: ISortDirection.descending, field: IPredictionSortableFields.creation_time};
        if (queryParams.prediction_sort_direction) {
            prediction_sort.direction = queryParams.prediction_sort_direction
        }
        if (queryParams.prediction_sort_field) {
            prediction_sort.field = queryParams.prediction_sort_field
        }

        let predictions: ICxPrediction[] = CxStore.get_predictions(queryParams.id_training, prediction_sort);


        let training_sort = {direction: ISortDirection.descending, field: ITrainingSortableFields.creation_time};
        if (queryParams.training_sort_direction) {
            training_sort.direction = queryParams.training_sort_direction
        }
        if (queryParams.training_sort_field) {
            training_sort.field = queryParams.training_sort_field
        }

        let trainings: ICxTraining[] = CxStore.get_trainings(training_sort);


        const items_per_page = 5;
        const total_pages = Math.ceil(trainings.length / items_per_page);
        const page: number = parseInt(queryParams.page || 1);

        let picked_training: any = null;
        if (queryParams.id_training) {
            picked_training = CxStore.trainings.get(queryParams.id_training)
        }
        let index = 0;
        let prediction_index = 0;
        let prediction_page = this.state.prediction_page;
        const prediction_total_pages = Math.ceil(predictions.length / this.state.prediction_items_per_page);
        let pie_chart_data: any = [];
        let current_prediction = CxStore.predictions.get(this.state.picked_prediction!);

        if (current_prediction && current_prediction.statistic) {
            pie_chart_data = [
                {
                    name: current_prediction.statistic.positive_level_name,
                    value: parseInt(current_prediction.statistic.positive_level),
                    positive: true
                },
                {
                    name: current_prediction.statistic.negative_level_name,
                    value: parseInt(current_prediction.statistic.negative_level),
                    positive: false
                }
            ]

        }


        return (
            <Container fluid>
                <Segment stacked padded={"very"}>
                    <div className={"table-header"}>
                        <div className={"table-header-left"}>
                            <Header as="h3"> Entrenamientos <Loader size={"tiny"}
                                                                    active={CxStore.isLoading("cx_list_trainings")}
                                                                    inline/></Header>
                        </div>
                        <div className={"table-header-right"}>
                            <Popup trigger={<Button icon primary onClick={() => {
                                CxStore.cx_list_trainings(true)
                            }}>
                                <Icon name='undo'/>
                            </Button>} content='Actualizar'/>
                            <Popup trigger={<Button icon primary={!this.state.training_form.isOpen}
                                                    negative={this.state.training_form.isOpen} onClick={() => {

                                if (!this.state.training_form.isOpen) {
                                    CxStore.cx_list_mymodels(false);

                                }
                                this.setState({
                                    training_form: {
                                        ...this.state.training_form,
                                        isOpen: !this.state.training_form.isOpen
                                    }
                                });
                            }}>
                                {this.state.training_form.isOpen ? <Icon name='minus'/> : <Icon name='plus'/>}
                            </Button>} content='Agregar Entranamiento'/>
                        </div>
                    </div>
                    {this.state.training_form.isOpen ?
                        <Segment basic
                                 loading={CxStore.isLoading("cx_list_mymodels") || CxStore.isLoading("cx_create_training") || this.state.training_form.uploading || CxStore.isLoading("cx_uploads3_traininput")}>
                            <Form onSubmit={() => {
                                CxStore.cx_create_training(this.state.training_form).then(() => {
                                    toast(<div>Entrenamiento <Label>{this.state.training_form.name}</Label> creado
                                    </div>);
                                    this.setState({
                                            training_form: {
                                                id_model: "",
                                                name: "",
                                                type: "supervised",
                                                dataset_url_training: "",
                                                training_label: "",
                                                subtype: "classification",
                                                isOpen: false,
                                                uploading: false
                                            },
                                        }
                                    )
                                })
                            }}>
                                <Form.Input label='Nombre'
                                            placeholder='Nombre'
                                            onChange={(e: any, data: any) => {
                                                this.setState({
                                                    training_form: {
                                                        ...this.state.training_form,
                                                        name: data.value
                                                    }
                                                })
                                            }}
                                            value={this.state.training_form.name}
                                            required/>
                                <Form.Select label='Modelo'
                                             placeholder='Modelo'
                                             options={CxStore.get_my_model_option()}
                                             onChange={(e: any, data: any) => {
                                                 this.setState({
                                                     training_form: {
                                                         ...this.state.training_form,
                                                         id_model: data.value,
                                                         training_label: "",
                                                     }
                                                 })
                                             }}
                                             value={this.state.training_form.id_model}
                                             required/>
                                <Form.Input label='Datos de entrenamiento'
                                            type={"file"}
                                            placeholder='Datos de entrenamiento'
                                            onChange={this.upload_training}
                                            required/>
                                <Form.Select label='Columna a predecir'
                                             placeholder='Columna a predecir'
                                             options={model_options(this.state.training_form.id_model)}
                                             onChange={(e: any, data: any) => {
                                                 this.setState({
                                                     training_form: {
                                                         ...this.state.training_form,
                                                         training_label: data.value,
                                                     }
                                                 })
                                             }}
                                             value={this.state.training_form.training_label}
                                             required/>

                                <Form.Input label='Tipo'
                                            placeholder='Tipo'
                                            onChange={(e: any, data: any) => {
                                                this.setState({
                                                    training_form: {
                                                        ...this.state.training_form,
                                                        type: data.value
                                                    }
                                                })
                                            }}
                                            value={this.state.training_form.type}
                                            required
                                            disabled/>

                                <Form.Input label='Sub tipo'
                                            placeholder='Sub tipo'
                                            onChange={(e: any, data: any) => {
                                                this.setState({
                                                    training_form: {
                                                        ...this.state.training_form,
                                                        subtype: data.value
                                                    }
                                                })
                                            }}
                                            value={this.state.training_form.subtype}
                                            required
                                            disabled/>


                                <Button secondary>Crear</Button>
                            </Form>
                        </Segment> : null}
                    <Table celled selectable sortable>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell
                                    sorted={training_sort.field == ITrainingSortableFields.name ? training_sort.direction : undefined}
                                    onClick={this.handleSortTrainings(ITrainingSortableFields.name)}>
                                    Nombre
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={training_sort.field == ITrainingSortableFields.creation_time ? training_sort.direction : undefined}
                                    onClick={this.handleSortTrainings(ITrainingSortableFields.creation_time)}>
                                    Creado
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={training_sort.field == ITrainingSortableFields.model ? training_sort.direction : undefined}
                                    onClick={this.handleSortTrainings(ITrainingSortableFields.model)}>
                                    Modelo
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={training_sort.field == ITrainingSortableFields.state ? training_sort.direction : undefined}
                                    onClick={this.handleSortTrainings(ITrainingSortableFields.state)}>
                                    Estado
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={training_sort.field == ITrainingSortableFields.type ? training_sort.direction : undefined}
                                    onClick={this.handleSortTrainings(ITrainingSortableFields.type)}>
                                    Tipo
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={training_sort.field == ITrainingSortableFields.subtype ? training_sort.direction : undefined}
                                    onClick={this.handleSortTrainings(ITrainingSortableFields.subtype)}>
                                    Subtipo
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {trainings.map((training) => {
                                if (!training) {
                                    return
                                }
                                if (index < ((page - 1) * items_per_page)) {
                                    index = index + 1;
                                    return null;
                                } else if (index >= page * items_per_page) {
                                    return null
                                }

                                index = index + 1;
                                return <Table.Row key={training.id_training}
                                                  positive={queryParams.id_training == training.id_training}>
                                    <Table.Cell>
                                        <Button
                                            disabled={training.state != "ready"}
                                            size={"tiny"}
                                            onClick={() => {
                                                this.setState({picked_prediction: null});
                                                rootStore.routerStore.goTo("entrenamientos", {}, {
                                                    page: page,
                                                    id_training: training.id_training
                                                });

                                            }} secondary={queryParams.id_training == training.id_training}>
                                            {training.name}
                                        </Button>
                                        {training.is_free == "true" ?
                                            <Label color={"red"} pointing='left'>gratis</Label> : null}
                                    </Table.Cell>
                                    <Table.Cell>{moment(training.creation_time).fromNow()}</Table.Cell>
                                    <Table.Cell>{training.model.name}</Table.Cell>
                                    <Table.Cell><Label
                                        color={training.state == "ready" ? "green" : "orange"}>{training.state == "ready" ? "finalizado" : "procesando"}</Label></Table.Cell>
                                    <Table.Cell>{training.type}</Table.Cell>
                                    <Table.Cell>{training.subtype}</Table.Cell>
                                </Table.Row>
                            })}

                        </Table.Body>

                    </Table>
                    <Grid columns='equal' style={{paddingTop: 16}}>
                        <Grid.Column floated='left'>
                            Mostrando
                            del {(page - 1) * items_per_page} al {page * items_per_page} de {trainings.length} ítems
                        </Grid.Column>
                        <Grid.Column floated='right' width={8} textAlign="right">
                            {total_pages > 0 ?

                                <Pagination onPageChange={(event, data) => {
                                    this.setState({picked_prediction: null});
                                    rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                                        rootStore.routerStore.routerState.params,
                                        {
                                            ...rootStore.routerStore.routerState.queryParams,
                                            page: data.activePage,
                                            id_training: undefined
                                        });

                                }}
                                            defaultActivePage={page}
                                            ellipsisItem={{content: <Icon name='ellipsis horizontal'/>, icon: true}}
                                            firstItem={{content: <Icon name='angle double left'/>, icon: true}}
                                            lastItem={{content: <Icon name='angle double right'/>, icon: true}}
                                            prevItem={{content: <Icon name='angle left'/>, icon: true}}
                                            nextItem={{content: <Icon name='angle right'/>, icon: true}}
                                            totalPages={total_pages}
                                /> : null
                            }
                        </Grid.Column>
                    </Grid>

                </Segment>
                {picked_training ?
                    <Segment padded={"very"}
                             loading={CxStore.isLoading(`cx_training_details.${queryParams.id_training}`)}>
                        <Segment vertical>
                            <b>Matriz de confusión:</b>
                            <div style={{display: "flex", justifyContent: "center", alignItems: "center"}}>
                                <Table celled padded collapsing>
                                    <Table.Header>
                                        <Table.Row textAlign={"center"}>
                                            <Table.HeaderCell/>
                                            <Table.HeaderCell>
                                                n'
                                                <br/>
                                                (Predicción)
                                            </Table.HeaderCell>
                                            <Table.HeaderCell>
                                                p'
                                                <br/>
                                                (Predicción)
                                            </Table.HeaderCell>
                                        </Table.Row>
                                    </Table.Header>

                                    <Table.Body>
                                        <Table.Row textAlign={"center"}>
                                            <Table.Cell>
                                                <Header as='h5' textAlign='center'>
                                                    n'
                                                    <br/>
                                                    (Actual)
                                                </Header>
                                            </Table.Cell>
                                            <Table.Cell>
                                                {picked_training.metric && picked_training.metric.confusion_matrix.tn}
                                                <br/>
                                                Verdaderos Negativos
                                            </Table.Cell>
                                            <Table.Cell>
                                                {picked_training.metric && picked_training.metric.confusion_matrix.fp}
                                                <br/>
                                                Falsos Positivos

                                            </Table.Cell>
                                        </Table.Row>
                                        <Table.Row textAlign={"center"}>
                                            <Table.Cell>
                                                <Header as='h5' textAlign='center'>
                                                    p'
                                                    <br/>
                                                    (Actual)
                                                </Header>
                                            </Table.Cell>
                                            <Table.Cell>
                                                {picked_training.metric && picked_training.metric.confusion_matrix.fn}
                                                <br/>
                                                Falsos Negativos
                                            </Table.Cell>
                                            <Table.Cell>
                                                {picked_training.metric && picked_training.metric.confusion_matrix.tp}
                                                <br/>
                                                Verdaderos Positivos
                                            </Table.Cell>
                                        </Table.Row>
                                    </Table.Body>
                                </Table>
                            </div>

                            {picked_training.metric && picked_training.metric.roc_curve && picked_training.metric.roc_curve.fpr && picked_training.metric.roc_curve.tpr ?
                                <div><b>Curva roc: </b><RocCurve roc_curve={picked_training.metric.roc_curve}/>
                                </div> : null}

                            {picked_training.metric && picked_training.metric.importances ?
                                <div><b>Importancia de variables: </b><VariableImportance
                                    importances={picked_training.metric.importances}/>
                                </div> : null}


                        </Segment>

                        <Segment vertical>
                            <b>Accuracy:</b> {picked_training.metric.accuracy}
                        </Segment>
                        <Segment vertical>
                            <b>F score:</b> {picked_training.metric.fscore}
                        </Segment>
                        <Segment vertical>
                            <b>Recall:</b> {picked_training.metric.recall}
                        </Segment>
                        <Segment vertical>
                            <b>Precision:</b> {picked_training.metric.precision}
                        </Segment>
                        <Segment vertical>
                            <b>Especificidad:</b> {picked_training.metric.roc}
                        </Segment>
                    </Segment> : null}


                {picked_training ? <Container fluid>
                    <Segment stacked padded={"very"}>
                        <div className={"table-header"}>
                            <div className={"table-header-left"}>
                                <Header as="h3"> Predicciónes <Loader size={"tiny"}
                                                                      active={CxStore.isLoading(`cx_list_predictions.${queryParams.id_training}`)}
                                                                      inline/></Header>
                            </div>
                            <div className={"table-header-right"}>
                                <Popup trigger={<Button icon primary onClick={() => {
                                    CxStore.cx_list_predictions(picked_training.id_training, true)
                                }}>
                                    <Icon name='undo'/>
                                </Button>} content='Actualizar'/>
                                <Popup trigger={<Button icon primary={!this.state.prediction_form.isOpen}
                                                        negative={this.state.prediction_form.isOpen} onClick={() => {
                                    this.setState({
                                        prediction_form: {
                                            ...this.state.prediction_form,
                                            isOpen: !this.state.prediction_form.isOpen
                                        }
                                    });
                                }}>
                                    {this.state.prediction_form.isOpen ? <Icon name='minus'/> : <Icon name='plus'/>}
                                </Button>} content='Agregar Predicción'/>
                            </div>
                        </div>

                        {this.state.prediction_form.isOpen ?
                            <Segment basic
                                     loading={CxStore.isLoading("cx_create_prediction") || this.state.prediction_form.uploading || CxStore.isLoading("cx_uploads3_predictinput")}>
                                <Form onSubmit={() => {
                                    CxStore.cx_create_prediction(queryParams.id_training, this.state.prediction_form.name, this.state.prediction_form.input).then(() => {
                                        toast(<div>Predicción <Label>{this.state.prediction_form.name}</Label> creada
                                        </div>);
                                        this.setState({
                                                prediction_form: {name: "", input: "", isOpen: false, uploading: false},
                                            }
                                        )
                                    })
                                }}>
                                    <Form.Group widths={2}>
                                        <Form.Input label='Nombre'
                                                    placeholder='Nombre'
                                                    onChange={(e: any, data: any) => {
                                                        this.setState({
                                                            prediction_form: {
                                                                ...this.state.prediction_form,
                                                                name: data.value
                                                            }
                                                        })
                                                    }}
                                                    value={this.state.prediction_form.name}
                                                    required/>

                                        <Form.Input label='Datos de entrada'
                                                    type={"file"}
                                                    placeholder='Datos de entrada'
                                                    onChange={this.upload_prediction}
                                                    required/>
                                    </Form.Group>

                                    <Button secondary>Crear</Button>
                                </Form>
                            </Segment> : null}


                        <Table celled selectable sortable>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell
                                        sorted={prediction_sort.field == IPredictionSortableFields.name ? prediction_sort.direction : undefined}
                                        onClick={this.handleSortPredictions(IPredictionSortableFields.name)}>
                                        Nombre
                                    </Table.HeaderCell>
                                    <Table.HeaderCell
                                        sorted={prediction_sort.field == IPredictionSortableFields.creation_time ? prediction_sort.direction : undefined}
                                        onClick={this.handleSortPredictions(IPredictionSortableFields.creation_time)}>
                                        Creado
                                    </Table.HeaderCell>
                                    <Table.HeaderCell
                                        sorted={prediction_sort.field == IPredictionSortableFields.job_state ? prediction_sort.direction : undefined}
                                        onClick={this.handleSortPredictions(IPredictionSortableFields.job_state)}>
                                        Estado
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>

                            <Table.Body>
                                {predictions.map((prediction) => {
                                    if (prediction_index < ((this.state.prediction_page - 1) * this.state.prediction_items_per_page)) {
                                        prediction_index = prediction_index + 1;
                                        return null;
                                    } else if (prediction_index >= this.state.prediction_page * this.state.prediction_items_per_page) {
                                        return null
                                    }

                                    prediction_index = prediction_index + 1;

                                    return <Table.Row key={prediction.id_prediction}
                                                      positive={this.state.picked_prediction == prediction.id_prediction}>
                                        <Table.Cell>
                                            <Button
                                                size={"tiny"}
                                                disabled={prediction.job_state != "finished"}
                                                onClick={() => {
                                                    CxStore.cx_prediction_results(prediction.id_prediction);
                                                    this.setState({picked_prediction: prediction.id_prediction})

                                                }}
                                                secondary={this.state.picked_prediction == prediction.id_prediction}>
                                                {prediction.name}
                                            </Button>
                                        </Table.Cell>
                                        <Table.Cell>{moment(prediction.creation_time).fromNow()}</Table.Cell>
                                        <Table.Cell>
                                            <Label
                                                color={prediction.job_state == "finished" ? "green" : "orange"}>
                                                {prediction.job_state == "finished" ? "finalizado" : "procesando"}
                                            </Label>
                                        </Table.Cell>
                                    </Table.Row>
                                })}

                            </Table.Body>

                        </Table>
                        <Grid columns='equal' style={{paddingTop: 16}}>
                            <Grid.Column floated='left'>
                                Mostrando
                                del {(this.state.prediction_page - 1) * this.state.prediction_items_per_page} al {this.state.prediction_page * this.state.prediction_items_per_page} de {predictions.length} ítems
                            </Grid.Column>
                            <Grid.Column floated='right' width={8} textAlign="right">
                                {total_pages > 0 ?

                                    <Pagination onPageChange={(event, data) => {
                                        this.setState({prediction_page: data.activePage, picked_prediction: null});
                                    }}
                                                defaultActivePage={prediction_page}
                                                ellipsisItem={{
                                                    content: <Icon name='ellipsis horizontal'/>,
                                                    icon: true
                                                }}
                                                firstItem={{content: <Icon name='angle double left'/>, icon: true}}
                                                lastItem={{content: <Icon name='angle double right'/>, icon: true}}
                                                prevItem={{content: <Icon name='angle left'/>, icon: true}}
                                                nextItem={{content: <Icon name='angle right'/>, icon: true}}
                                                totalPages={prediction_total_pages}
                                    /> : null
                                }
                            </Grid.Column>
                        </Grid>

                    </Segment>

                    {this.state.picked_prediction ? <Segment padded={"very"}>
                        <Segment basic>
                            <Header as={"h4"}>
                                Como indicador
                                &nbsp;
                                &nbsp;
                                <Button size={"mini"}
                                        color={as_indicator_color(this.state.picked_prediction!)}
                                        disabled={CxStore.isLoading(`cx_prediction_state.${this.state.picked_prediction!}`)}
                                        onClick={() => {
                                            CxStore.cx_prediction_state(this.state.picked_prediction!);
                                        }}
                                >
                                    {as_indicator_name(this.state.picked_prediction!)}
                                </Button>
                            </Header>
                        </Segment>
                        <Segment basic
                                 loading={CxStore.isLoading(`cx_prediction_results.${this.state.picked_prediction!}`)}>
                            <Header
                                as="h4"> {current_prediction && current_prediction.statistic && current_prediction.statistic.report_name}</Header>
                            <p>
                                {current_prediction && current_prediction.statistic && current_prediction.statistic.report_desc}
                            </p>
                        </Segment>

                        <Segment basic
                                 loading={CxStore.isLoading(`cx_prediction_results.${this.state.picked_prediction!}`)}>
                            <Header as="h4">Evaluación</Header>
                            <div style={{display: "flex"}}>
                                <Table definition collapsing style={{marginTop: 50}}>
                                    <Table.Body>
                                        <Table.Row>
                                            <Table.Cell>{current_prediction && current_prediction.statistic && current_prediction.statistic.positive_level_name}</Table.Cell>
                                            <Table.Cell>{current_prediction && current_prediction.statistic && current_prediction.statistic.positive_level}</Table.Cell>
                                        </Table.Row>
                                        <Table.Row>
                                            <Table.Cell>{current_prediction && current_prediction.statistic && current_prediction.statistic.negative_level_name}</Table.Cell>
                                            <Table.Cell>{current_prediction && current_prediction.statistic && current_prediction.statistic.negative_level}</Table.Cell>
                                        </Table.Row>
                                        <Table.Row>
                                            <Table.Cell>Total</Table.Cell>
                                            <Table.Cell>{current_prediction && current_prediction.statistic && current_prediction.statistic.total_entries}</Table.Cell>
                                        </Table.Row>
                                    </Table.Body>
                                </Table>

                                <PieChart width={800} height={300} style={{flexGrowth: 1}}>
                                    <Pie
                                        activeIndex={this.state.activeIndex}
                                        activeShape={renderActiveShape}
                                        data={pie_chart_data}
                                        cx={400}
                                        cy={150}
                                        innerRadius={60}
                                        outerRadius={80}
                                        fill="#8884d8"
                                        onMouseEnter={this.onPieEnter}
                                    />
                                </PieChart>
                            </div>
                        </Segment>


                    </Segment> : null}

                </Container> : null}
            </Container>
        )
    }

}


