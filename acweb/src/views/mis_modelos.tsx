import * as React from "react";
import {observer} from "mobx-react";
import {Menu, Dropdown, Segment, Header, Table, Popup, Grid, Icon, Pagination, Button, Loader} from 'semantic-ui-react'
import {
    CxStore,
    IClientModelSortableFields,
    ICxClientModel,
    ICxModel,
    IModelSortableFields,
    ISortDirection
} from "../backend/models";
import * as moment from 'moment';
import {rootStore} from "../backend/stores";
import {capatilizedFromSnakeCase} from "../utils"

@observer
export class MyModels extends React.Component <any, any> {
    handleSort = (field: IClientModelSortableFields) => {
        return () => {
            const queryParams: any = rootStore.routerStore.routerState.queryParams;
            rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                rootStore.routerStore.routerState.params,
                {
                    ...queryParams,
                    client_model_sort_direction: queryParams.client_model_sort_direction == ISortDirection.ascending ? ISortDirection.descending : ISortDirection.ascending,
                    client_model_sort_field: field
                })
        };

    };

    render() {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;
        let sort = {direction: ISortDirection.descending, field: IClientModelSortableFields.addition_date};
        if (queryParams.client_model_sort_direction) {
            sort.direction = queryParams.client_model_sort_direction
        }
        if (queryParams.client_model_sort_field) {
            sort.field = queryParams.client_model_sort_field
        }
        let client_models: ICxClientModel[] = CxStore.get_client_models(sort);

        const items_per_page = 7;
        const total_pages = Math.ceil(client_models.length / items_per_page);
        const page: number = parseInt(queryParams.page || 1);


        let index = 0;
        return (

            <Segment stacked padded={"very"}>
                <div className={"table-header"}>
                    <div className={"table-header-left"}>
                        <Header as="h3"> Mis Modelos <Loader size={"tiny"}
                                                             active={CxStore.isLoading("cx_list_mymodels")}
                                                             inline/></Header>
                    </div>
                    <div className={"table-header-right"}>
                        <Popup trigger={<Button icon primary onClick={() => {
                            CxStore.cx_list_mymodels(true)
                        }}>
                            <Icon name='undo'/>
                        </Button>} content='Actualizar'/>
                        <Popup trigger={<Button icon primary onClick={() => {
                            rootStore.routerStore.goTo("tienda", {}, {"category": "default_rate"});
                        }}>
                            <Icon name='plus'/>
                            <Icon name='world'/>
                        </Button>} content='agregar modelo desde la tienda'/>
                    </div>
                </div>
                <Table celled sortable>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell
                                sorted={sort.field == IClientModelSortableFields.name ? sort.direction : undefined}
                                onClick={this.handleSort(IClientModelSortableFields.name)}>
                                Nombre
                            </Table.HeaderCell>
                            <Table.HeaderCell
                                sorted={sort.field == IClientModelSortableFields.addition_date ? sort.direction : undefined}
                                onClick={this.handleSort(IClientModelSortableFields.addition_date)}>
                                Agregado
                            </Table.HeaderCell>
                            <Table.HeaderCell
                                sorted={sort.field == IClientModelSortableFields.category ? sort.direction : undefined}
                                onClick={this.handleSort(IClientModelSortableFields.category)}>
                                Categoría
                            </Table.HeaderCell>
                            <Table.HeaderCell
                                sorted={sort.field == IClientModelSortableFields.industry ? sort.direction : undefined}
                                onClick={this.handleSort(IClientModelSortableFields.industry)}>Servicio</Table.HeaderCell>
                            <Table.HeaderCell
                                sorted={sort.field == IClientModelSortableFields.version ? sort.direction : undefined}
                                onClick={this.handleSort(IClientModelSortableFields.version)}
                                collapsing>Versión</Table.HeaderCell>
                            <Table.HeaderCell collapsing>Acciones</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {client_models.map((cm) => {
                            if (index < ((page - 1) * items_per_page)) {
                                index = index + 1;
                                return null;
                            } else if (index >= page * items_per_page) {
                                return null
                            }

                            index = index + 1;
                            return <Table.Row key={cm.id_client_model}>
                                <Table.Cell>{cm.model.name}</Table.Cell>
                                <Table.Cell>{moment(cm.addition_date).fromNow()}</Table.Cell>
                                <Table.Cell>{capatilizedFromSnakeCase(cm.model.category)}</Table.Cell>
                                <Table.Cell>{cm.model.industry}</Table.Cell>
                                <Table.Cell>{cm.model.version}</Table.Cell>
                                <Table.Cell textAlign={"center"}>
                                    <Dropdown icon='ellipsis horizontal' className={"grey-icon"} simple>
                                        <Dropdown.Menu>
                                            <Dropdown.Item onClick={() => {
                                                rootStore.routerStore.goTo("entrenamientos", {}, {page: 1});
                                            }
                                            }>
                                                <Icon name='list alternate outline'/>
                                                <span className='text'>Ver entrenamientos</span>
                                            </Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>

                                </Table.Cell>
                            </Table.Row>
                        })}

                    </Table.Body>

                </Table>
                <Grid columns='equal' style={{paddingTop: 16}}>
                    <Grid.Column floated='left'>
                        Mostrando
                        del {(page - 1) * items_per_page} al {page * items_per_page} de {client_models.length} ítems
                    </Grid.Column>
                    <Grid.Column floated='right' width={8} textAlign="right">
                        {total_pages > 0 ?

                            <Pagination onPageChange={(event, data) => {
                                rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                                    rootStore.routerStore.routerState.params,
                                    {
                                        ...rootStore.routerStore.routerState.queryParams,
                                        page: data.activePage,
                                    });
                            }}
                                        defaultActivePage={page}
                                        ellipsisItem={{content: <Icon name='ellipsis horizontal'/>, icon: true}}
                                        firstItem={{content: <Icon name='angle double left'/>, icon: true}}
                                        lastItem={{content: <Icon name='angle double right'/>, icon: true}}
                                        prevItem={{content: <Icon name='angle left'/>, icon: true}}
                                        nextItem={{content: <Icon name='angle right'/>, icon: true}}
                                        totalPages={total_pages}
                            /> : null
                        }
                    </Grid.Column>
                </Grid>

            </Segment>
        )
    }

}


