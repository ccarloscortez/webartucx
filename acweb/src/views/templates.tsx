import * as React from "react";
import {observer} from "mobx-react";
import {
    Label,
    Modal,
    Dropdown,
    Form,
    Segment,
    Header,
    Table,
    Container,
    Grid,
    Icon,
    Pagination,
    Button,
    TextArea,
    Loader,
    Popup
} from 'semantic-ui-react'
import {
    CxStore,
    ICxMailTemplate,
    IMailTemplateSortableFields,
    ISortDirection,
    ICxSmsTemplate
} from "../backend/models";
import * as moment from 'moment';
import {rootStore} from "../backend/stores";
import {toast} from "react-toastify";
import {removeModal} from "../utils";


@observer
class TemplateDeleteConfirmModal extends React.Component<any, any> {
    close = () => {
        rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
            rootStore.routerStore.routerState.params,
            removeModal(rootStore.routerStore.routerState.queryParams)
        )
    };

    remove = () => {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;
        let template_id = queryParams.modal_template_id;
        let template = CxStore.templates.get(template_id);
        let sms_template = CxStore.sms_templates.get(template_id);
        if (template) {
            const tn = template.name;
            CxStore.cx_delete_template(template.id_mailtemplate).then(() => {
                toast.error(
                    <div>Template <Label>{tn}</Label> eliminado
                    </div>);
            });
        } else if (sms_template) {
            const tn = sms_template.name;
            CxStore.cx_delete_smstemplate(sms_template.id_smstemplate).then(() => {
                toast.error(
                    <div>Template <Label>{tn}</Label> eliminado
                    </div>);
            });
        } else {
            return null;
        }
        this.close()
    };

    render() {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;
        let template_id = queryParams.modal_template_id;
        let template = CxStore.templates.get(template_id);
        let sms_template = CxStore.sms_templates.get(template_id);
        if (!template && !sms_template) {
            return null
        }
        return <Modal open={queryParams.modal == "template_delete_confirmation"}
                      closeOnEscape
                      size={'tiny'}
                      closeOnDimmerClick
                      onClose={this.close}>
            <Modal.Header>Eliminar template</Modal.Header>
            <Modal.Content>
                <p>Esta seguro que desea eliminar el template <Label>{template.name}</Label></p>
            </Modal.Content>
            <Modal.Actions>
                <Button onClick={this.close} negative>
                    No
                </Button>
                <Button onClick={this.remove}
                        positive
                        labelPosition='right'
                        icon='checkmark'
                        content='Si'/>
            </Modal.Actions>
        </Modal>
    }
}


@observer
export class TemplatesView extends React.Component <any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            list_type: "both",
            template_form: {
                isOpen: false,
                name: "",
                desc: "",
                body: "",
                title: "",
                type: "email",
            }
        };

    }

    handleSort = (field: IMailTemplateSortableFields) => {
        return () => {
            const queryParams: any = rootStore.routerStore.routerState.queryParams;
            rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                rootStore.routerStore.routerState.params,
                {
                    ...queryParams,
                    mail_template_sort_direction: queryParams.mail_template_sort_direction == ISortDirection.ascending ? ISortDirection.descending : ISortDirection.ascending,
                    mail_template_sort_field: field
                })
        };

    };

    render() {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;
        let sort = {direction: ISortDirection.descending, field: IMailTemplateSortableFields.creation_date};
        if (queryParams.mail_template_sort_direction) {
            sort.direction = queryParams.mail_template_sort_direction
        }
        if (queryParams.mail_template_sort_field) {
            sort.field = queryParams.mail_template_sort_field
        }
        let templates: ICxMailTemplate[] = [];
        let sms_templates: ICxSmsTemplate[] = [];
        const items_per_page = 7;
        // Total no. of pages based on the total count of mail + sms templates
        let total_pages = 0;
        switch (this.state.list_type) {
            case "both":
                // FIXME: Combine both list for sorting properly otherwise email and sms templates will be sorted within themselves.
                templates = CxStore.get_templates(sort);
                sms_templates = CxStore.get_smstemplates(sort);
                total_pages = Math.ceil((templates.length + sms_templates.length) / items_per_page);
            break;
            case "email":
                templates = CxStore.get_templates(sort);
                total_pages = Math.ceil(templates.length / items_per_page);
            break;
            case "sms":
                sms_templates = CxStore.get_smstemplates(sort);
                total_pages = Math.ceil(sms_templates.length / items_per_page);
            break;
        }
        const page: number = parseInt(queryParams.page || 1);
        let picked_template: any = null;
        let picked_sms_template: any = null;
        if (queryParams.id_mailtemplate) {
            picked_template = CxStore.templates.get(queryParams.id_mailtemplate)
        } else if (queryParams.id_smstemplate) {
            picked_sms_template = CxStore.sms_templates.get(queryParams.id_smstemplate)
        }
        let index = 0;

        return (
            <Container fluid>
                <TemplateDeleteConfirmModal/>
                <Segment stacked padded={"very"}>
                    <div className={"table-header"}>
                        <div className={"table-header-left"}>
                            <Header as="h3">
                                Templates
                                <Loader size={"tiny"}
                                        active={CxStore.isLoading("cx_list_templates")}
                                        inline/>
                            </Header>
                        </div>
                        <Form.Group widths={2}>
                            <Form.Select
                                options={[
                                {
                                    key: "both",
                                    value: "both",
                                    text: "All"
                                }, {
                                    key: "email",
                                    value: "email",
                                    text: "Email"
                                }, {
                                    key: "sms",
                                    value: "sms",
                                    text: "SMS"
                                }]}
                                placeholder='Tipo'
                                onChange={(e: any, data: any) => {
                                    this.setState({
                                        list_type: data.value,
                                    })
                                }}
                                value={this.state.list_type}
                                required
                            />
                        </Form.Group>
                        <div className={"table-header-right"}>
                            <Popup trigger={<Button icon primary onClick={() => {
                                CxStore.cx_list_templates(true)
                                CxStore.cx_list_smstemplates(true)
                            }}>
                                <Icon name='undo'/>
                            </Button>} content='Actualizar'/>
                            <Popup trigger={<Button icon primary={!this.state.template_form.isOpen}
                                                    negative={this.state.template_form.isOpen} onClick={() => {
                                this.setState({template_form: {isOpen: !this.state.template_form.isOpen}});
                            }}>
                                {this.state.template_form.isOpen ? <Icon name='minus'/> : <Icon name='plus'/>}
                            </Button>} content='Agregar template'/>
                        </div>
                    </div>
                    {/* // FIXME: loading={CxStore.isLoading("cx_create_smstemplate")} */}
                    {this.state.template_form.isOpen ? <Segment basic loading={CxStore.isLoading("cx_create_template")}>
                        <Form onSubmit={() => {
                            let pr;
                            if (this.state.template_form.type == "email") {
                                pr = CxStore.cx_create_template(this.state.template_form.name, this.state.template_form.desc, this.state.template_form.title, this.state.template_form.body);
                            } else {
                                pr = CxStore.cx_create_smstemplate(this.state.template_form.name, this.state.template_form.desc, this.state.template_form.body);
                            }
                            pr.then(() => {
                                toast(<div>Template <Label>{this.state.template_form.name}</Label> creado</div>);

                                this.setState({
                                        template_form: {isOpen: false, name: "", desc: "", body: "", title: "", type: "email"}
                                    }
                                )
                            })
                        }}>
                            <Form.Group widths={2}>
                                <Form.Input label='Nombre'
                                            placeholder='Nombre'
                                            onChange={(e: any, data: any) => {
                                                this.setState({
                                                    template_form: {
                                                        ...this.state.template_form,
                                                        name: data.value
                                                    }
                                                })
                                            }}
                                            value={this.state.template_form.name}
                                            required/>
                                <Form.Select label='Tipo'
                                             options={[
                                                {
                                                    key: "email",
                                                    value: "email",
                                                    text: "Email"
                                                }, {
                                                    key: "sms",
                                                    value: "sms",
                                                    text: "SMS"
                                                }]}
                                             placeholder='Tipo'
                                             onChange={(e: any, data: any) => {
                                                this.setState({
                                                    template_form: {
                                                        ...this.state.template_form,
                                                        type: data.value,
                                                        title: "",
                                                    }
                                                })
                                             }}
                                             value={this.state.template_form.type}
                                             required/>
                                {this.state.template_form.type == "email" ? <Form.Input label='Título del email'
                                            placeholder='Título del email'
                                            onChange={(e: any, data: any) => {
                                                this.setState({
                                                    template_form: {
                                                        ...this.state.template_form,
                                                        title: data.value
                                                    }
                                                })
                                            }}
                                            value={this.state.template_form.title}
                                            required/> : null}
                            </Form.Group>
                            <div className={"field"}>
                                <label>Descripción</label>
                                <TextArea
                                    autoHeight
                                    placeholder='Descripción'
                                    onChange={(e: any, data: any) => {
                                        this.setState({
                                            template_form: {
                                                ...this.state.template_form,
                                                desc: data.value
                                            }
                                        })
                                    }}
                                    value={this.state.template_form.desc}/>
                            </div>

                            <div className={"field"}>
                                <label>Mensaje (soporta html)</label>
                                <TextArea
                                    autoHeight
                                    placeholder='Mensaje'
                                    onChange={(e: any, data: any) => {
                                        this.setState({
                                            template_form: {
                                                ...this.state.template_form,
                                                body: data.value
                                            }
                                        })
                                    }}
                                    value={this.state.template_form.body}
                                    required/>
                            </div>
                            <Button secondary>Crear</Button>
                        </Form>
                    </Segment> : null}
                    <Table celled selectable sortable>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell
                                    sorted={sort.field == IMailTemplateSortableFields.name ? sort.direction : undefined}
                                    onClick={this.handleSort(IMailTemplateSortableFields.name)}>
                                    Nombre
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={sort.field == IMailTemplateSortableFields.creation_date ? sort.direction : undefined}
                                    onClick={this.handleSort(IMailTemplateSortableFields.creation_date)}>
                                    Creado
                                </Table.HeaderCell>
                                <Table.HeaderCell collapsing>Acciones</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {templates.map((template) => {
                                if (index < ((page - 1) * items_per_page)) {
                                    index = index + 1;
                                    return null;
                                } else if (index >= page * items_per_page) {
                                    return null
                                }

                                index = index + 1;

                                return <Table.Row key={template.id_mailtemplate}
                                                  negative={CxStore.isLoading(`cx_delete_template.${template.id_mailtemplate}`)}
                                                  positive={queryParams.id_mailtemplate == template.id_mailtemplate}>
                                    <Table.Cell>
                                        <Button size={"tiny"} onClick={() => {
                                            rootStore.routerStore.goTo("templates", {}, {
                                                page: page,
                                                id_mailtemplate: template.id_mailtemplate
                                            });

                                        }} secondary={queryParams.id_template == template.id_mailtemplate}>
                                            {template.name}
                                        </Button>
                                        <Loader size={"tiny"}
                                                active={CxStore.isLoading(`cx_delete_template.${template.id_mailtemplate}`)}
                                                inline/>
                                    </Table.Cell>
                                    <Table.Cell>{moment(template.creation_date!).fromNow()}</Table.Cell>
                                    <Table.Cell textAlign={"center"}>
                                        <Dropdown icon='ellipsis horizontal'
                                                  className={"grey-icon"}
                                                  direction={"left"}>
                                            <Dropdown.Menu>
                                                <Dropdown.Item
                                                    disabled={CxStore.isLoading(`cx_delete_template.${template.id_mailtemplate}`) || template.campaign_count > 0}
                                                    onClick={() => {
                                                        rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                                                            rootStore.routerStore.routerState.params,
                                                            {
                                                                ...rootStore.routerStore.routerState.queryParams,
                                                                modal: "template_delete_confirmation",
                                                                modal_template_id: template.id_mailtemplate
                                                            }
                                                        )
                                                    }}>
                                                    <Icon name='close' color={"red"}/>
                                                    <span className='text'>Eliminar</span>
                                                    <Label
                                                        style={{marginLeft: 16}}>{template.campaign_count} Campañas</Label>

                                                </Dropdown.Item>
                                            </Dropdown.Menu>
                                        </Dropdown>
                                    </Table.Cell>
                                </Table.Row>
                            })}
                            {sms_templates.map((template) => {
                                if (index < ((page - 1) * items_per_page)) {
                                    index = index + 1;
                                    return null;
                                } else if (index >= page * items_per_page) {
                                    return null
                                }

                                index = index + 1;

                                return <Table.Row key={template.id_smstemplate}
                                                  negative={CxStore.isLoading(`cx_delete_smstemplate.${template.id_smstemplate}`)}
                                                  positive={queryParams.id_smstemplate == template.id_smstemplate}>
                                    <Table.Cell>
                                        <Button size={"tiny"} onClick={() => {
                                            rootStore.routerStore.goTo("templates", {}, {
                                                page: page,
                                                id_smstemplate: template.id_smstemplate
                                            });

                                        }} secondary={queryParams.id_template == template.id_smstemplate}>
                                            {template.name}
                                        </Button>
                                        <Loader size={"tiny"}
                                                active={CxStore.isLoading(`cx_delete_smstemplate.${template.id_smstemplate}`)}
                                                inline/>
                                    </Table.Cell>
                                    <Table.Cell>{moment(template.creation_date!).fromNow()}</Table.Cell>
                                    <Table.Cell textAlign={"center"}>
                                        <Dropdown icon='ellipsis horizontal'
                                                  className={"grey-icon"}
                                                  direction={"left"}>
                                            <Dropdown.Menu>
                                                <Dropdown.Item
                                                    disabled={CxStore.isLoading(`cx_delete_smstemplate.${template.id_smstemplate}`) || template.campaign_count > 0}
                                                    onClick={() => {
                                                        rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                                                            rootStore.routerStore.routerState.params,
                                                            {
                                                                ...rootStore.routerStore.routerState.queryParams,
                                                                modal: "template_delete_confirmation",
                                                                modal_template_id: template.id_smstemplate
                                                            }
                                                        )
                                                    }}>
                                                    <Icon name='close' color={"red"}/>
                                                    <span className='text'>Eliminar</span>
                                                    <Label
                                                        style={{marginLeft: 16}}>{template.campaign_count} Campañas</Label>

                                                </Dropdown.Item>
                                            </Dropdown.Menu>
                                        </Dropdown>
                                    </Table.Cell>
                                </Table.Row>
                            })}
                        </Table.Body>

                    </Table>
                    <Grid columns='equal' style={{paddingTop: 16}}>
                        <Grid.Column floated='left'>
                            Mostrando
                            del {(page - 1) * items_per_page} al {page * items_per_page} de {total_pages} ítems
                        </Grid.Column>
                        <Grid.Column floated='right' width={8} textAlign="right">
                            {total_pages > 0 ?

                                <Pagination onPageChange={(event, data) => {
                                    rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                                        rootStore.routerStore.routerState.params,
                                        {
                                            ...rootStore.routerStore.routerState.queryParams,
                                            page: data.activePage,
                                            id_mailtemplate: undefined,
                                            id_smstemplate: undefined
                                        });
                                }}
                                            defaultActivePage={page}
                                            ellipsisItem={{
                                                content: <Icon name='ellipsis horizontal'/>,
                                                icon: true
                                            }}
                                            firstItem={{content: <Icon name='angle double left'/>, icon: true}}
                                            lastItem={{content: <Icon name='angle double right'/>, icon: true}}
                                            prevItem={{content: <Icon name='angle left'/>, icon: true}}
                                            nextItem={{content: <Icon name='angle right'/>, icon: true}}
                                            totalPages={total_pages}
                                /> : null
                            }
                        </Grid.Column>
                    </Grid>

                </Segment>
                {picked_template ?
                    <Segment padded={"very"}
                             loading={CxStore.isLoading(`cx_edit_template.${queryParams.id_mailtemplate}`)}>
                        <Form onSubmit={() => {
                            CxStore.cx_edit_template(queryParams.id_mailtemplate, {
                                name: picked_template.name,
                                desc: picked_template.desc,
                                title: picked_template.title,
                                body: picked_template.body
                            })
                        }}>
                            <Form.Group widths={2}>
                                <Form.Input label='Nombre'
                                            placeholder='Nombre'
                                            onChange={(e: any, data: any) => {
                                                picked_template.set_name(data.value);
                                            }}
                                            value={picked_template.name}
                                            required/>
                                <Form.Input label='Sujeto'
                                            placeholder='Sujeto'
                                            onChange={(e: any, data: any) => {
                                                picked_template.set_title(data.value);
                                            }}
                                            value={picked_template.title}
                                            required/>
                            </Form.Group>
                            <div className={"field"}>
                                <label>Descripción</label>
                                <TextArea
                                    autoHeight
                                    placeholder='Descripción'
                                    onChange={(e: any, data: any) => {
                                        picked_template.set_desc(data.value);

                                    }}
                                    value={picked_template.desc}/>
                            </div>

                            <div className={"field"}>
                                <label>Mensaje (soporta html)</label>
                                <TextArea
                                    autoHeight
                                    placeholder='Mensaje'
                                    onChange={(e: any, data: any) => {
                                        picked_template.set_body(data.value);

                                    }}
                                    value={picked_template.body}
                                    required/>
                            </div>
                            <Button secondary>Guardar</Button>
                        </Form>

                    </Segment> : picked_sms_template ?
                    <Segment padded={"very"}
                            loading={CxStore.isLoading(`cx_edit_template.${queryParams.id_smstemplate}`)}>
                        <Form onSubmit={() => {
                            CxStore.cx_edit_smstemplate(queryParams.id_smstemplate, {
                                name: picked_sms_template.name,
                                desc: picked_sms_template.desc,
                                body: picked_sms_template.body
                            })
                        }}>
                        <Form.Group widths={2}>
                            <Form.Input label='Nombre'
                                        placeholder='Nombre'
                                        onChange={(e: any, data: any) => {
                                            picked_sms_template.set_name(data.value);
                                        }}
                                        value={picked_sms_template.name}
                                        required/>
                        </Form.Group>
                        <div className={"field"}>
                            <label>Descripción</label>
                            <TextArea
                                autoHeight
                                placeholder='Descripción'
                                onChange={(e: any, data: any) => {
                                    picked_sms_template.set_desc(data.value);

                                }}
                                value={picked_sms_template.desc}/>
                        </div>

                        <div className={"field"}>
                            <label>Mensaje (soporta html)</label>
                            <TextArea
                                autoHeight
                                placeholder='Mensaje'
                                onChange={(e: any, data: any) => {
                                    picked_sms_template.set_body(data.value);

                                }}
                                value={picked_sms_template.body}
                                required/>
                        </div>
                        <Button secondary>Guardar</Button>
                    </Form>

                </Segment> : null}

            </Container>
        )
    }

}


