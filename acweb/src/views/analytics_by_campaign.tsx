import * as React from "react";
import {observer} from "mobx-react";
import {
    Segment,
    Header,
    Table,
    Label,
    Grid,
    Icon,
    Pagination,
    Button,
    Loader,
    Container,
    Image,
    Form, Dropdown
} from 'semantic-ui-react'
import {CxStore, PostmarkEventKind} from "../backend/models";
import * as moment from 'moment';
import {rootStore} from "../backend/stores";
import {
    AreaChart,
    Area,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    BarChart,
    Bar,
    Radar,
    RadarChart,
    PolarGrid,
    Legend,
    PieChart,
    Pie,
    PolarAngleAxis,
    PolarRadiusAxis
} from 'recharts';

import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import {mapTimeToString, TimeModal} from "./eventos";

@observer
export class TextCard extends React.Component <any, any> {


    render() {
        return (
            <Segment>
                <Header as="h5">{this.props.title}</Header>
                <Label color={"black"} size={"mini"}>SEMANA PASADA</Label>
                <Segment textAlign={"center"} basic style={{fontSize: 40}}>
                    {this.props.message}
                </Segment>
            </Segment>
        )
    }

}


@observer
export class TextCard2 extends React.Component <any, any> {


    render() {
        return (
            <Segment>
                <Header as="h5">{this.props.title}</Header>
                <Label style={{visibility: "hidden"}} color={"black"} size={"mini"}>SEMANA PASADA</Label>
                <Segment textAlign={"center"} basic style={{fontSize: 40}}>
                    {this.props.message}
                </Segment>
            </Segment>
        )
    }

}

const SimpleBarChart = (props: any) => {
    let data = [];
    let day_timestamp = moment(new Date());
    for (let i = 0; i < 7; i++) {
        day_timestamp = day_timestamp.add(-1, "day");
        let day: any = {name: day_timestamp.format('DD, MM')};
        const queryParams: any = rootStore.routerStore.routerState.queryParams;
        day[`${props.dataKey}`] = CxStore.get_postmark_event(queryParams.id_campaign,
            "",
            day_timestamp.startOf('day').toDate(),
            day_timestamp.endOf('day').toDate(),
            [props.kind]).length;
        data.push(day)
    }

    return (
        <BarChart width={600} height={300} data={data}
                  margin={{top: 5, right: 30, left: 20, bottom: 5}}>
            <CartesianGrid strokeDasharray="3 3"/>
            <XAxis dataKey="name"/>
            <YAxis/>
            <Tooltip/>
            <Legend/>
            <Bar dataKey={props.dataKey} fill={props.color}/>
        </BarChart>
    );

};


const TwoLevelRadarChart = (props: any) => {
    const data = [
        {subject: 'Math', A: 120, B: 110, fullMark: 150},
        {subject: 'Chinese', A: 98, B: 130, fullMark: 150},
        {subject: 'English', A: 86, B: 130, fullMark: 150},
        {subject: 'Geography', A: 99, B: 100, fullMark: 150},
        {subject: 'Physics', A: 85, B: 90, fullMark: 150},
        {subject: 'History', A: 65, B: 85, fullMark: 150},
    ];
    return (
        <RadarChart cx={300} cy={250} outerRadius={150} width={600} height={500} data={data}>
            <PolarGrid/>
            <PolarAngleAxis dataKey="subject"/>
            <PolarRadiusAxis angle={30} domain={[0, 150]}/>
            <Radar name="Mike" dataKey="A" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6}/>
            <Radar name="Lily" dataKey="B" stroke="#82ca9d" fill="#82ca9d" fillOpacity={0.6}/>
            <Legend/>

        </RadarChart>
    );
};


const TwoLevelPieChart = (props: any) => {
    const data01 = [{name: 'Group A', value: 400}, {name: 'Group B', value: 300},
        {name: 'Group C', value: 300}, {name: 'Group D', value: 200}]

    const data02 = [{name: 'A1', value: 100},
        {name: 'A2', value: 300},
        {name: 'B1', value: 100},
        {name: 'B2', value: 80},
        {name: 'B3', value: 40},
        {name: 'B4', value: 30},
        {name: 'B5', value: 50},
        {name: 'C1', value: 100},
        {name: 'C2', value: 200},
        {name: 'D1', value: 150},
        {name: 'D2', value: 50}]
    return (
        <PieChart width={600} height={300}>
            <Pie data={data01} cx={300} cy={150} outerRadius={60} fill="#8884d8"/>
            <Pie data={data02} cx={300} cy={150} innerRadius={70} outerRadius={90} fill="#82ca9d" label/>
        </PieChart>
    );
};

@observer
export class CampaignAnalytics extends React.Component <any, any> {

    state = {myDate: moment(), popup_open: null};

    render() {
        const queryParams: any = rootStore.routerStore.routerState.queryParams;
        let time = queryParams.time;
        let from = null;
        let to = null;
        if (!time) {
            time = "all_time"
        }
        if (time == "last_30_days") {
            to = new Date();
            from = moment(to).add(-30, "days").toDate();
        }
        if (time == "last_month") {
            to = new Date();
            from = moment(to).startOf("month").toDate();
        }
        if (time == "previous_month") {
            to = new Date();
            from = moment(to).startOf("month").add(-1, "month").toDate();
            to = moment(from).endOf("month").toDate()
        }
        if (time == "last_7_days") {
            to = new Date();
            from = moment(to).add(-7, "days").toDate();
        }
        if (time == "last_week") {
            to = new Date();
            from = moment(to).startOf("week").toDate();
        }
        if (time == "previous_week") {
            to = new Date();
            from = moment(to).startOf("week").add(-1, "week").toDate();
            to = moment(from).endOf("week").toDate()
        }
        if (time == "custom") {
            if (queryParams.from) {
                from = moment(queryParams.from, "YYYYMMDD").toDate()
            }
            if (queryParams.to) {
                to = moment(queryParams.to, "YYYYMMDD").toDate()
            }
        }
        return (
            <Container fluid>
                <TimeModal from={from} to={to}/>

                <Segment>
                    <Form>
                        <Form.Group>
                            <div className="field">
                                <label>Campaña</label>
                                <Dropdown search
                                          placeholder='Campaña'
                                          value={queryParams.id_campaign}
                                          onChange={(e: any, data: any) => {
                                              rootStore.routerStore.goTo(
                                                  rootStore.routerStore.routerState.routeName,
                                                  rootStore.routerStore.routerState.params,
                                                  {
                                                      ...rootStore.routerStore.routerState.queryParams,
                                                      id_campaign: data.value,
                                                  });
                                          }}
                                          selection
                                          options={CxStore.get_campaign_option()}/>
                            </div>
                            <div className="field">
                                <label style={{visibility: "hidden"}}>Hasta</label>

                                <Dropdown
                                    open={this.state.popup_open == "time"}
                                    onClose={() => {
                                        if (this.state.popup_open == "time") this.setState({popup_open: null})
                                    }}
                                    onOpen={() => {
                                        this.setState({popup_open: "time"})
                                    }}
                                    button
                                    className='icon primary'
                                    floating
                                    labeled
                                    icon='calendar'
                                    text={mapTimeToString(time)}>
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={() => {
                                            rootStore.routerStore.goTo(
                                                rootStore.routerStore.routerState.routeName,
                                                rootStore.routerStore.routerState.params,
                                                {
                                                    ...rootStore.routerStore.routerState.queryParams,
                                                    time: "all_time",
                                                    from: undefined,
                                                    to: undefined
                                                });
                                        }}>Todo el tiempo</Dropdown.Item>
                                        <Dropdown.Item onClick={() => {
                                            rootStore.routerStore.goTo(
                                                rootStore.routerStore.routerState.routeName,
                                                rootStore.routerStore.routerState.params,
                                                {
                                                    ...rootStore.routerStore.routerState.queryParams,
                                                    time: "last_30_days",
                                                    from: undefined,
                                                    to: undefined
                                                });
                                        }}>Último 30 dias</Dropdown.Item>
                                        <Dropdown.Item onClick={() => {
                                            rootStore.routerStore.goTo(
                                                rootStore.routerStore.routerState.routeName,
                                                rootStore.routerStore.routerState.params,
                                                {
                                                    ...rootStore.routerStore.routerState.queryParams,
                                                    time: "last_month",
                                                    from: undefined,
                                                    to: undefined
                                                });
                                        }}>Último mes</Dropdown.Item>
                                        <Dropdown.Item onClick={() => {
                                            rootStore.routerStore.goTo(
                                                rootStore.routerStore.routerState.routeName,
                                                rootStore.routerStore.routerState.params,
                                                {
                                                    ...rootStore.routerStore.routerState.queryParams,
                                                    time: "previous_month",
                                                    from: undefined,
                                                    to: undefined
                                                });
                                        }}>Mes anterior</Dropdown.Item>
                                        <Dropdown.Item onClick={() => {
                                            rootStore.routerStore.goTo(
                                                rootStore.routerStore.routerState.routeName,
                                                rootStore.routerStore.routerState.params,
                                                {
                                                    ...rootStore.routerStore.routerState.queryParams,
                                                    time: "last_7_days",
                                                    from: undefined,
                                                    to: undefined
                                                });
                                        }}>Último 7 dias</Dropdown.Item>
                                        <Dropdown.Item onClick={() => {
                                            rootStore.routerStore.goTo(
                                                rootStore.routerStore.routerState.routeName,
                                                rootStore.routerStore.routerState.params,
                                                {
                                                    ...rootStore.routerStore.routerState.queryParams,
                                                    time: "last_week",
                                                    from: undefined,
                                                    to: undefined
                                                });
                                        }}>Última semana</Dropdown.Item>
                                        <Dropdown.Item onClick={() => {
                                            rootStore.routerStore.goTo(
                                                rootStore.routerStore.routerState.routeName,
                                                rootStore.routerStore.routerState.params,
                                                {
                                                    ...rootStore.routerStore.routerState.queryParams,
                                                    time: "previous_week",
                                                    from: undefined,
                                                    to: undefined
                                                });
                                        }}>Semana anterior</Dropdown.Item>
                                        <Dropdown.Item onClick={() => {
                                            rootStore.routerStore.goTo(rootStore.routerStore.routerState.routeName,
                                                rootStore.routerStore.routerState.params,
                                                {...rootStore.routerStore.routerState.queryParams, modal: "custom_time"}
                                            )
                                        }}>Personalizado</Dropdown.Item>

                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>

                        </Form.Group>
                    </Form>


                </Segment>
                <Grid columns={5} stackable>
                    <Grid.Row>
                        <Grid.Column>

                            <TextCard2 title={"Enviados"}
                                       message={CxStore.get_postmark_event(queryParams.id_campaign,
                                           "",
                                           from,
                                           to,
                                           [PostmarkEventKind.delivery, PostmarkEventKind.bounce]).length}/>
                        </Grid.Column>
                        <Grid.Column>
                            <TextCard2 title={"Entregados"} message={CxStore.get_postmark_event(queryParams.id_campaign,
                                "",
                                from,
                                to,
                                [PostmarkEventKind.delivery]).length}/>
                        </Grid.Column>
                        <Grid.Column>
                            <TextCard2 title={"Abiertos"} message={CxStore.get_postmark_event(queryParams.id_campaign,
                                "",
                                from,
                                to,
                                [PostmarkEventKind.open]).length}/>
                        </Grid.Column>
                        <Grid.Column>
                            <TextCard2 title={"Clicks"} message={CxStore.get_postmark_event(queryParams.id_campaign,
                                "",
                                from,
                                to,
                                [PostmarkEventKind.click]).length}/>
                        </Grid.Column>
                        <Grid.Column>
                            <TextCard2 title={"Rebotados"} message={CxStore.get_postmark_event(queryParams.id_campaign,
                                "",
                                from,
                                to,
                                [PostmarkEventKind.bounce]).length}/>
                        </Grid.Column>
                    </Grid.Row>

                </Grid>
                <Grid columns={2} stackable>
                    <Grid.Row>
                        <Grid.Column>
                            <Segment>
                                <Header>Entregados por día</Header>
                                <SimpleBarChart dataKey={"Entregados"} color={"#b5cc18"}
                                                kind={PostmarkEventKind.delivery}/>
                            </Segment>
                        </Grid.Column>
                        <Grid.Column>
                            <Segment>
                                <Header>Abiertos por día</Header>
                                <SimpleBarChart dataKey={"Abiertos"} color={"#00b5ad"} kind={PostmarkEventKind.open}/>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Segment>
                                <Header>Rebotados por día</Header>
                                <SimpleBarChart dataKey={"Rebotados"} color={"#db2828"}
                                                kind={PostmarkEventKind.bounce}/>
                            </Segment>
                        </Grid.Column>
                        <Grid.Column>
                            <Segment>
                                <Header>Clicks por día</Header>
                                <SimpleBarChart dataKey={"Clicks"} color={"#a333c8"} kind={PostmarkEventKind.click}/>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>

        )
    }

}


