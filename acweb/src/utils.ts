export function capatilizedFromSnakeCase(input: string) {
    return input.replace(/_{1,}/g, ' ').replace(/(\s{1,}|\b)(\w)/g, function (m, space, letter) {
        return space + letter.toUpperCase();
    })
}


export function mapResourceKind(kind: string): string {
    switch (kind) {
        case "CxMailtemplate":
            return "Template";
        case  "CxProject":
            return "Proyecto";
        case "CxClientModel":
            return "Modelo";
        case  "CxSegment":
            return "Segmento";
        case  "CxTraining":
            return "Entrenamiento";
        case "CxCampaign":
            return "Campaña";
        case "CxPrediction":
            return "Predicción";

        default:
            return "---"
    }

}

export function removeModal(obj: any): any {
    let ret: any = {};
    for (let key of Object.keys(obj)) {
        if (!key.startsWith("modal")) {
            ret[key] = obj[key]
        }
    }
    return ret;
}
