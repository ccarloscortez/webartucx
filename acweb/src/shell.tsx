import * as React from "react";
import {inject} from 'mobx-react';
import {RouterView} from 'mobx-state-router';
import {LoginView} from "./views/login";
import {MyModels} from "./views/mis_modelos";
import {MainLayout} from "./views/app_layout";
import {Tienda} from "./views/tienda";
import {Trainings} from "./views/entrenamientos";
import {BusinessAnalytics} from "./views/business_analytics";
import {Projects} from "./views/proyectos";
import {TemplatesView} from "./views/templates";
import {Campaigns} from "./views/campañas";
import {Events} from "./views/eventos";
import {Indicators} from "./views/indicators";
import {CampaignAnalytics} from "./views/analytics_by_campaign";

const viewMap = {
    login: <LoginView/>,
    mis_modelos: <MainLayout><MyModels/></MainLayout>,
    tienda: <MainLayout><Tienda/></MainLayout>,
    entrenamientos: <MainLayout><Trainings/></MainLayout>,
    analitica_por_segmento: <MainLayout fluid={true}><BusinessAnalytics/></MainLayout>,
    analitica_por_campagna: <MainLayout fluid={true}><CampaignAnalytics/></MainLayout>,
    proyectos: <MainLayout><Projects/></MainLayout>,
    templates: <MainLayout><TemplatesView/></MainLayout>,
    campaigns: <MainLayout><Campaigns/></MainLayout>,
    events: <MainLayout><Events/></MainLayout>,
    indicators: <MainLayout fluid={true}><Indicators/></MainLayout>,


};

class ShellBase extends React.Component<any, any> {
    render() {
        const routerStore = this.props.rootStore.routerStore;

        return (
            <div>
                <RouterView routerStore={routerStore} viewMap={viewMap}/>
            </div>
        );
    }
}

export const Shell = inject('rootStore')(ShellBase);